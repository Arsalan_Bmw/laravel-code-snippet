<?php

return [
    [
        "status" => ["Appealing", "Closed", "CS Issue", "Dismantle", " ID Flagged", "Replaced", "User Flagged"],
        "label" => "No Action",
    ],
    [
        "status" => ["Active", "Building", "WH Warming"],
        "fields" => [
            "ad_account_id" => "",
            "business_manager_id" => "",
            "fb_user_full_name" => ""
        ],
        "label" => "FB Account Check"
    ],
    [
        "status" => ["Active", "Building", "WH Warming"],
        "fields" => [
            "domain" => ""
        ],
        "label" => "Get Domain",
    ],
    [
        "status" => ["Active", "Building", "WH Warming"],
        "fields" => [
            "website_cleaned" => "0"
        ],
        "label" => "Clean website",
    ],
    [
        "status" => ["Active", "Building", "WH Warming"],
        "fields" => [
            "page_id" => ""
        ],
        "label" => "Create Page",
    ],
    [
        "status" => ["Active", "Building", "WH Warming"],
        "fields" => [
            "post_scheduled" => "0"
        ],
        "label" => "Schedule Posts",
    ],
    [
        "status" => ["Active", "Building", "WH Warming"],
        "fields" => [
            "ad_account_id" => ""
        ],
        "label" => "Create Ad Account",
    ],
    [
        "status" => ["Active", "Building", "WH Warming"],
        "fields" => [
            "cc_added" => "0"
        ],
        "label" => "Add CC",
    ],
    [
        "status" => ["Active", "Building", "WH Warming"],
        "fields" => [
            "added_to_adcost" => "0"
        ],
        "label" => "Add to AdCost",
    ],
    [
        "status" => ["Active", "Building", "WH Warming"],
        "fields" => [
            "daily_likes_camp" => "0"
        ],
        "label" => "Build Daily Likes Campaign",
    ],
    [
        "status" => ["Active", "Building", "WH Warming"],
        "fields" => [
            "pixel_id" => ""
        ],
        "label" => "Create Pixel",
    ],
    [
        "status" => ["Active", "Building", "WH Warming"],
        "fields" => [
            "domain_verified_in_fb" => "0"
        ],
        "label" => "Verify Domain",
    ],
    [
        "status" => ["Active", "Building", "WH Warming"],
        "fields" => [
            "offer" => "0"
        ],
        "label" => "Assign Offer",
    ],
    [
        "status" => ["Active", "Building", "WH Warming"],
        "fields" => [
            "media_buyer" => "0"
        ],
        "label" => "Assign Media Buyer",
    ],
    [
        "status" => ["Active", "Building", "WH Warming"],
        "fields" => [
            "added_to_kk" => "0"
        ],
        "label" => "Add to KK",
    ],
    [
        "status" => ["Active", "Building", "WH Warming"],
        "fields" => [
            "passed_to_media_buyer" => "0"
        ],
        "label" => "Pass to Media Buyer",
    ],
    [
        "status" => ["Active", "Building", "WH Warming"],
        "fields" => [
            "wh_camp" => "0"
        ],
        "label" => "check WH Camp",
    ],
    [
        "status" => ["Active", "Building", "WH Warming"],
        "fields" => [
            "bh_camp" => "0"
        ],
        "label" => "check BH Camp",
    ],
    [
        "status" => ["Active", "Building", "WH Warming"],
        "fields" => [
            "bh_camp" => "1"
        ],
        "label" => "Daily Manage",
    ],
    
];
