<?php
return [
    'key' => env('CROSSROADS_KEY', ''),
    'url' => env('CROSSROADS_URL', '')
];
