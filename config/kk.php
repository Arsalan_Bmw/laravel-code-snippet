<?php

use Illuminate\Support\Str;
return [
    'group_id'=>env('KK_GROUP_ID',''),
    'api_key'=>env('KK_API_KEY',''),
    'url'=>env('KK_API_URL','')
];
