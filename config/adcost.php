<?php

use Illuminate\Support\Str;
return [
    'token'=>env('AD_COST_TOKEN',''),
    'url'=>env('AD_COST_URL','')
];
