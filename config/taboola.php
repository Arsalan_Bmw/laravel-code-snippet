<?php

return [
    'login_url'=>env('TABOOLA_LOGIN_URL',''),
    'base_url'=>env('TABOOLA_URL',''),
    'client_id'=>env('TABOOLA_CLIENT_ID',''),
    'client_secret'=>env('TABOOLA_CLIENT_SECRET',''),
    'grant_type'=>env('TABOOLA_GRANT_TYPE','')
];
