<?php

namespace App\Providers;

use App\Models\FBAccount;
use App\Models\FBAccountLog;
use App\Observers\FbAccountLogObserver;
use App\Observers\FbAccountObserver;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\UrlGenerator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        DB::listen(function($query) {
            Log::info(
                $query->sql,
                $query->bindings,
                $query->time
            );
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(UrlGenerator $url)
    {
        //force https scheme for traefik
        if (env('APP_ENV') !== 'local') {
            $url->forceScheme('https');
        }
        FBAccount::observe(FbAccountObserver::class);
        FBAccountLog::observe(FbAccountLogObserver::class);
    }
}
