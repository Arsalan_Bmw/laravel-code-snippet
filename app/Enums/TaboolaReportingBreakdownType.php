<?php
namespace App\Enums;
abstract class TaboolaReportingBreakdownType {
    const Site="site";
    const Campaign="campaign";
    const Advertiser="advertiser";
    const Day="day";
    const Hour="hour";
}