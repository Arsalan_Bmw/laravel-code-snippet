<?php
namespace App\Enums;
abstract class TaboolaReportingMetricsType {
    const Conversions="conversions";
    const CampaignNumber="campaign_number";
    const Clicks="clicks";
    const Impressions="impressions";
    const VisibleImpressions="visible_impressions";
    const Spent="spent";
    const CTR="ctr";
    const VCTR="vctr";
    const CPM="cpm";
    const VCPM="vcpm";
    const CPC="cpc";
    const CPA="cpa";
    const CPAConversionRate="cpa_conversion_rate";
    const Timestamp="timestamp";
    const LanderSearches="lander_searches";
    const LanderVisitors="lander_visitors";
    const PublisherRevenueAmount="publisher_revenue_amount";
    const RevenueClicks="revenue_clicks";
    const TotalVisitors="total_visitors";
    const TrackedVisitors="tracked_visitors";
    const OBCTR="ob_ctr";
    const RPC="rpc";
    const Profit="profit";
    const RPM="rpm";
    const RPV="rpv";
    const ROAS="roas";
    const ROI="roi";
    const ProfitMargin="profit_margin";
}