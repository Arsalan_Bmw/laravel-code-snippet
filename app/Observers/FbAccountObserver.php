<?php

namespace App\Observers;

use App\Models\FBAccount;

class FbAccountObserver
{
    /**
     * Handle the FBAccount "saving" event.
     *
     * @param  \App\Models\FBAccount  $fBAccount
     * @return void
     */
    public function saving(FBAccount $FBAccount)
    {
        $accountName = $FBAccount->ad_account_name == '' ? 'NA' : $FBAccount->ad_account_name;
        $accountId = $FBAccount->ad_account_id == '' ? 'NA' : $FBAccount->ad_account_id;
        $offer = $FBAccount->vertical()->first() ? $FBAccount->vertical()->first()['short_name']:'NA';
        $pixelId = $FBAccount->pixel_id == '' ? 'NA' : $FBAccount->pixel_id;
        $managerId = $FBAccount->business_manager_id == '' ? 'NA' : $FBAccount->business_manager_id;



        $FBAccount->kk_account_name = $accountName.'_'.$accountId.'_'.$managerId.'_'.$pixelId;
        $FBAccount->kk_campaign_title = $this->getInitials($FBAccount).'_'.$accountName.'_'.$accountId.'_'.$managerId.'_'.$pixelId;
        $FBAccount->kk_campaign_url = $FBAccount->domain.'/lp1';
        $FBAccount->fb_campaign_name = $this->getInitials($FBAccount).'_'.$offer.'_'.$accountId.'_PUR2_'.date("Ymd").'_Angle_Rulz';
        $FBAccount->url_tracking_string = 's1={{campaign.id}}&s3={{ad.id}}&s4='.$FBAccount->ad_account_id;
        
        $FBAccount->setNextStep();

    }

    private function getInitials($FBAccount){
        
        if($FBAccount->mediaBuyer){
           $firstName   =  $FBAccount->mediaBuyer->first_name;
           $lastName   =  $FBAccount->mediaBuyer->last_name;
           if(strlen($firstName)==0 && strlen($lastName)==0){
               return "NA";
           }
            return ucwords($firstName ? $firstName[0] : '').ucwords($lastName ? $lastName[0] : '');
        }

        return "NA";
        
    }
}
