<?php

namespace App\Observers;

use App\Models\FBAccountLog;

class FbAccountLogObserver
{
    /**
     * Handle the FBAccount "created" event.
     *
     * @param  \App\Models\FBAccountLog  $fBAccountLog
     * @return void
     */
    public function created(FBAccountLog $fBAccountLog)
    {
        $fBAccountLog->fbAccount->last_log_time = $fBAccountLog->date;
        $fBAccountLog->fbAccount->save();
    }
}
