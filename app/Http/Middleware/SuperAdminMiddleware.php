<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;

class SuperAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Sentinel::check())
        {
            $user = Sentinel::getUser();

            if($user->inRole('superadmin'))
            {
                return $next($request);
            }
            else if($user->inRole('admin'))
            {
                return redirect()->route('admin-list-account');
            }
            else if($user->inRole('manager'))
            {
                return redirect()->route('manager-list-account');
            }
            // else if($user->inRole('accountant'))
            // {
            //     return redirect()->route('accountant-list-account');
            // }
        }

        return redirect()->route('login');
    }
}
