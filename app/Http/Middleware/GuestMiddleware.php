<?php

namespace App\Http\Middleware;

use Closure;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class GuestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Sentinel::check())
        {
            if (Sentinel::inRole('superadmin'))
            {
                return redirect()->route('list-account');
            }
            else if(Sentinel::inRole('admin'))
            {
                return redirect()->route('admin-list-account');
            }
            else if (Sentinel::inRole('manager'))
            {
                return redirect()->route('manager-list-account');;
            }
            //  else if (Sentinel::inRole('accountant'))
            // {
            //     return redirect()->route('accountant-list-account');
            // }
            else
            {
               
                return redirect()->route('login');
            }
        }

         // return redirect()->route('login')
        return $next($request);
    }
}
