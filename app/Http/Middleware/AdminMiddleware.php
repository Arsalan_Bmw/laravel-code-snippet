<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Sentinel::check())
        {
            $user = Sentinel::getUser();

            if($user->inRole('admin'))
            {
                return $next($request);
            }
            else if($user->inRole('superadmin'))
            {
                return redirect()->route('list-account');
            }
            else if($user->inRole('manager'))
            {
                return redirect()->route('manager-list-account');
            }
            // else if($user->inRole('accountant'))
            // {
            //     return redirect()->route('accountant-list-account');
            // }
        }

        return redirect()->route('login');
    }
}
