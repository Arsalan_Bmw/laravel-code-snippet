<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class HasPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $permission)
    {
        $role = Role::where(
            'name',
            Auth::user()
                ->roles
                ->pluck('name')
                ->toarray()
        )
            ->first();

        if (!$role->hasPermissionTo($permission)) {
            return response()->json(['error' => true, 'message' => 'Access denied'], 400);
        }
        return $next($request);
    }
}
