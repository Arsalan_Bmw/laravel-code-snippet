<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Auth;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        return User::with('roles')->filter($request->all())->paginateFilter($request->get("per_page"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $post = $request->all();
        $validatedData =  Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ]);
        if ($validatedData->fails()) {
            return response(['message' => 'Please enter proper details']);
        }

        $credentials = [
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'is_media_buyer' => $request->is_media_buyer,
            'status' => 1,
        ];

        if ($request->get('password')) {
            $credentials['password'] = Hash::make($request->password);
        }

        if ($user = User::create($credentials)) {
            if ($request->get('role')) {
                $role = Role::findOrCreate($request->get('role'));
                $user->assignRole($role);
                return response(['message' => 'User Created Successfully']);
            } else {
                User::find($user->id)->forceDelete();
                return response(['message' => 'Cannot create user without a valid role'], 403);
            }
        } else {
            return response(['message' => 'Something Went Wrong, Please try again'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        if (!empty($id)) {
            if ($user = User::with('roles')->find($id)) {
                $user->roles->pluck('name');
                $user = $user->toArray();
                return response(['user' => $user, 'message' => 'Retrieved successfully']);
            } else {
                return response(['message' => 'User id not valid']);
            }
        } else {
            return response(['message' => "Please Provide user id"]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $post = $request->all();
        if (isset($post)) {
            $user = User::find($id);
            if ($user->email != $post['email']) {
                $validatedData =  Validator::make($request->all(), [
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'email' => 'required|email|unique:users',
                ]);
            } else {
                $validatedData =  Validator::make($request->all(), [
                    'first_name' => 'required',
                    'last_name' => 'required',
                ]);
            }
            if ($validatedData->fails()) {
                return response(['message' => 'Please enter all the required fields']);
            }
            $credentials = [
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'is_media_buyer' => $request->is_media_buyer,
                'archive'  =>  $request->archive 
            ];
            if(isset($request->password)){
            $credentials['password']=bcrypt($request->password);
            }
            Log::info(json_encode($credentials));
            if ($user->update($credentials)) {
                if ($request->get('role')) {
                    $role = Role::findOrCreate($request->get('role'));
                    $user->syncRoles([$role]);
                }
                return response(['message' => 'User Info Updated']);
            } else {
                return response(['message' => 'Something went wrong. Please try again.']);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = User::findOrFail($id);
            $user->delete();
        } catch (Exception $e) {
            return response(['error' => 'User not found'], 404);
        }
    }


    /**
     * all the media buyers
     *
     * @return \Illuminate\Http\Response
     */
    public function mediaBuyers()
    {
        return User::Active()->get();
    }


    public function updateUserStatus($id,Request $request){
      $user = User::find($id);
      if($user){
          $user= $user->update($request->all());
      }
    }
}
