<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class AuthController extends Controller
{

    public function login(Request $request)
    {
        $loginData = $request->validate([
            'email' => 'email|required',
            'password' => 'required'

        ]);

        if (!auth()->attempt(
            [
                'email' => $request['email'],
                'password' => $request['password'],
                'archive' => 0
            ]
        )) {
            return response(["errors" => ['Invalid Credentials']], 422);
        }

        $accessToken = auth()->user()->createToken('authToken')->accessToken;
        auth()->user()->load('roles.permissions');
        return response(['user' => auth()->user(), 'token' => $accessToken]);
    }

    public function verify()
    {
        if (auth()->check()) {
            $accessToken = auth()->user()->createToken('authToken')->accessToken;
            auth()->user()->load('roles.permissions');
            return response(['user' => auth()->user(), 'token' => $accessToken]);
        } else {
            return response(["errors" => ['Invalid token']], 400);
        }
    }

    public function logout()
    {
        if (auth()->check()) {
            $user = auth()->user()->token();
            $user->delete();
            return response(['message' => 'Success']);
        } else {
            return response(['message' => 'Failed']);
        }
    }
}
