<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MetaConfigration;
use Carbon\Carbon;

class MetaConfigrationController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  string  $type
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return MetaConfigration::all();
    }
}
