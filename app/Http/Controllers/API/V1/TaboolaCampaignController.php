<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\TaboolaCampaignRequest;
use App\Services\TaboolaCampaignDataQueryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TaboolaCampaignController extends Controller
{
    public $taboolaCampaignDataQueryService;

    public function __construct(TaboolaCampaignDataQueryService $taboolaCampaignDataQueryService)
    {
        $this->taboolaCampaignDataQueryService = $taboolaCampaignDataQueryService;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function index(TaboolaCampaignRequest $request)
    {

        $data   =   $this->taboolaCampaignDataQueryService
            ->getData(
                $request['metrics'],
                $request['breakdown'],
                $request['startdate'],
                $request['enddate'],
                $request['page'],
                $request['per_page'],
                $request['sort_column'],
                $request['sort_dir']
            );
        $count = count($this->taboolaCampaignDataQueryService->getCount(
            $request['metrics'],
            $request['breakdown'],
            $request['startdate'],
            $request['enddate'],
        ));

        Log::info(json_encode($count));


        return [
            'data' => $data,
            'columns' => array_merge($request['metrics'], $request['breakdown']),
            'current_page' => $request['page'],
            'last_page' => ceil($count / $request['per_page']),
            'per_page' => $request['per_page'],
            'from' => ($request['page'] - 1) * $request['per_page'] + 1,
            'to' => ($request['page']) * $request['per_page'] < $count ? ($request['page']) * $request['per_page'] : $count,
            'total' => $count
        ];
    }


    public function getRevenueReport(Request $request)
    {
        return DB::table("taboola_campaign_performances")
            ->leftJoin('crossroads_revenue_reports', 'crossroads_revenue_reports.campaign_number', '=', 'taboola_campaign_performances.campaign_id')
            // ->leftJoin('crossroads_revenue_reports','crossroads_revenue_reports.tt','=','taboola_campaign_performances.tt')
            ->paginate(10);
    }
}
