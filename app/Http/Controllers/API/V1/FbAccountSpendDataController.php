<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Services\FBSpendDataQueryService;
use App\Http\Requests\FbAccountSpendDataRequest;

use Illuminate\Http\Request;

class FbAccountSpendDataController extends Controller
{
    public $fbReportQueryService;

    public function __construct(FBSpendDataQueryService $fbReportQueryService)
    {
        $this->fbReportQueryService = $fbReportQueryService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function show($fbAccountId,FbAccountSpendDataRequest $request)
    {
        return $this->fbReportQueryService->getData($fbAccountId,$request['metrics'],$request['start_date'],$request['end_date']);
    }

}
