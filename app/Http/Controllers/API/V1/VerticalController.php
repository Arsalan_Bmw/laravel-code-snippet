<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\OfferRequest;
use App\Models\Vertical;
use Illuminate\Http\Request;

class VerticalController extends Controller
{
    /**
     * Display a listing of the vertical.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Vertical::filter($request->all())->paginateFilter($request->get("per_page"));
    }

    /**
     * Store a newly created vertical in storage.
     *
     * @param  \Illuminate\Http\Vertical  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vertical = Vertical::create($request->all());
        return ['vertical' => $vertical];
    }

    /**
     * Display the specified vertical.
     *
     * @param  \App\Models\Verticals  $verticals
     * @return \Illuminate\Http\Response
     */
    public function show(Vertical $vertical)
    {
        return ['vertical' => $vertical];
    }

    /**
     * Update the specified vertical in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vertical  $vertical
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vertical $vertical)
    {
        $vertical->update($request->all());
        return ['vertical' => $vertical];
    }

    /**
     * Remove the specified vertical from storage.
     *
     * @param  \App\Models\vertical  $vertical
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vertical $vertical)
    {
        $vertical->delete();
        return [];
    }

    /**
     * all the verticals
     *
     * @param  \App\Models\vertical  $vertical
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        return Vertical::select(['long_name', 'short_name', 'id','archive'])
        ->Active()
        ->get();
    }
}
