<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\fbAccountLogRequest;
use App\Models\FBAccountLog;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FBAccountLogController extends Controller
{

    /**
     * Show the form for store a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(fbAccountLogRequest $request)
    {
       $fbLog = FBAccountLog::create(array_merge(['user_id'=>Auth::user()->id],$request->all()));
       if($fbLog){
           return response(['fbLog'=>$fbLog]);
       }else {
           return response(['errors' => 'There was some error please try again']);
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,Request $request)
    {
         return $fbLogs =   FBAccountLog::filter($request->all())
          ->with('getUser')
          ->where('account_id',$id)
          ->paginateFilter($request->get("per_page"));
    }




}
