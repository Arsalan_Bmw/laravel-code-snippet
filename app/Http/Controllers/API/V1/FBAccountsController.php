<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\FBAccount;
use App\Models\FBAccountLog;
use App\Models\FBAccountReport;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\FBAccountResource;
use App\Services\UserRoleService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;


class FBAccountsController extends Controller
{
    public $userRole;
    public function __construct(UserRoleService $userRole)
    {
        $this->userRole = $userRole;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // return Auth::user()->roles->pluck('name');
        return FBAccount::filter($request->all())
            ->select('*')
            ->spendToday()
            ->when(!$this->userRole->isAdmin(), function ($q) {
                $q->where('media_buyer_id', Auth::user()->id);
            })
            ->spendYesterday()
            ->with('source')
            ->revenueToday()
            ->revenueYesterday()
            ->paginateFilter($request->get("per_page"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = $request->all();

        if (isset($post)) {

            $fbAccount = new FBAccount($post);
            // $fbAccount->setNextStep(false);

            if ($fbAccount->save()) {
                // $update['account_id'] = $fbAccount->id;
                // $fbAccount->update($update);
                return response(['fbAccount' => $fbAccount]);
            } else {
                return response(['errors' => 'There was some error please try again']);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  FBAccount  $FBAccount
     * @return \Illuminate\Http\Response
     */
    public function show(FBAccount $fb_account)
    {
        $accountLogs = $fb_account->viewFbAccount();
        return response(['account' => $fb_account, 'accountLogs' => $accountLogs]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = $request->all();
        $fbAccount = FBAccount::find($id);
        if ($fbAccount->update($post)) {
            return response(['message' => 'Account Updated']);
        } else {
            return response(['error' => 'There was some error please try again'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            $fbAccount = FBAccount::findOrFail($id);
            $fbAccount->archive = 1;
            $fbAccount->save();
        } catch (Exception $e) {
            return response(['error' => 'FB Ad Account not found'], 404);
        }
    }

    public function getDropdowns(Request $request)
    {
        $getName = $request->get('list');
        $dropDown = config('dropdowns');
        if ($getName == '') {
            $list = $dropDown;
        } else {
            $list = $dropDown[$getName];
        }

        return response(['list' => $list]);
    }
}
