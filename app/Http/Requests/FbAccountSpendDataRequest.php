<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FbAccountSpendDataRequest extends FormRequest
{
    public $metrics=['cpc','conversion','cpr','clicks','spend', 'revenue'];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_date'=>['required'],
            'end_date'=>['required'],
            'metrics'=>['required','array',Rule::in($this->metrics)]
        ];
    }
}
