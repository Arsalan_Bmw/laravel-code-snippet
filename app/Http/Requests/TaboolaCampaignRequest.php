<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TaboolaCampaignRequest extends FormRequest
{
    private $breakdown=[
        'advertiser',
        'campaign',
        'site',
        'day',
        'hour'
    ];
    private $metrics=[
        "clicks",
        "impressions",
        "conversions",
        "visible_impressions",
        "spent",
        "ctr",
        "vctr",
        "cpm",
        "vcpm",
        "cpc",
        "cpa",
        "cpa_conversion_rate",
        "campaign_number",
        "timestamp",
        "lander_searches",
        "lander_visitors",
        "publisher_revenue_amount",
        "revenue_clicks",
        "total_visitors",
        "tracked_visitors",
        "Ob_ctr",
        "rpc",
        "profit",
        "rpm",
        "rpv",
        "roas",
        "roi",
        "profit_margin"

    ];
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'metrics'=>['required','array','in:'.implode(',',$this->metrics)],
            'breakdown'=>['required','array','in:' . implode(',', $this->breakdown)],
            'startdate'=>['required'],
            'enddate'=>'nullable|after_or_equal:startdate'
        ];
    }
}
