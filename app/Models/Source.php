<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
    use HasFactory,Filterable;

    protected $fillable=['short_name','long_name','archive'];
    
    const ACTIVE = false;

    public function scopeActive(){
        return $this->where('archive',self::ACTIVE);
    }
}
