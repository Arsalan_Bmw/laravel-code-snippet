<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;

class FBAccountLog extends Model
{
    use Filterable;
    /**
     * The table associated with the model.
     *
     * @var string
     */
	protected $table = 'fbaccounts_log';

    /**
     * set primary key of table
     *
     * @var integer
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * fillable column name goes here
     *
     * @var array
     */
    protected $fillable = [
		'user_id',
        'account_id',
        'date',
        'log_messages'
	];

    public function fbAccount() {
        return $this->belongsTo(FBAccount::class, 'account_id');
    }

    public function getUser() {
        return $this->belongsTo(User::class, 'user_id');
    }

}
