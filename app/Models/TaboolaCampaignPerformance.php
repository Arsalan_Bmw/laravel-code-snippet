<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaboolaCampaignPerformance extends Model
{
    use HasFactory;

    protected $fillable    = [
        'campaign_id',
        'clicks',
        'impressions',
        'visible_impressions',
        'spent',
        'cpa_actions_num',
        'currency',
        'timestamp',
        'conversions_value',
        'cpa_conversion_rate',
        'day',
        'hour',
        'report_at'
    ];

    
}
