<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FbAdSet extends Model
{
    use HasFactory;
    protected $fillable=['ad_set_id','ad_set_name'];
    public function AdSetReports(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(FBAccountReport::class,'ad_set_id','ad_set_id');
    }
}
