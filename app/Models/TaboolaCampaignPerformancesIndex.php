<?php

namespace App\Models;
use Bavix\LaravelClickHouse\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;


class TaboolaCampaignPerformancesIndex extends Model
{
    use HasFactory;
}
