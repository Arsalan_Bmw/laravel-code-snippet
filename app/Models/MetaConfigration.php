<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MetaConfigration extends Model
{
    use HasFactory;

    protected $fillable = ['type', 'updated_at'];

    const TYPE_ADCOST =  'ADCOST_DOWNLOAD';
    const TYPE_KK_REVENUE = 'KK_REVENUE_DOWNLOAD';
    const TYPE_TABOOLA = 'TABOOLA_DOWNLOAD';
    const TYPE_CROSSROADS = 'TYPE_CROSSROADS_DOWNLOAD';
}
