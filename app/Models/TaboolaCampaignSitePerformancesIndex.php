<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Bavix\LaravelClickHouse\Database\Eloquent\Model;

class TaboolaCampaignSitePerformancesIndex extends Model
{
    use HasFactory;
    protected $connetion='clickhouse';
}
