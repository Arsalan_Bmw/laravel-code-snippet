<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KKRevenueReport extends Model
{
    use HasFactory;
    protected $table = "kk_revenue_reports";

    protected $fillable = ['campaign_id', 'click_id', 'price', 'time', 'event'];
}
