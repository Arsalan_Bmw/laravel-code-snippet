<?php

namespace App\Models;

use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use App\Models\FBAccountLog;
use App\Models\User;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;


class FBAccount extends Model
{
    use Filterable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'fbaccounts';

    /**
     * set primary key of table
     *
     * @var integer
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;


    /**
     * fillable column name goes here
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'build_name',
        'next_step',
        'status',
        'priority',
        'quick_note',
        'accesspoint_notes',
        'accesspoint_password',
        'source',
        'comment_management_date',
        'date_of_next_action',
        'geo',
        'timezone',
        'fb_user_full_name',
        'fb_user_login_info',
        'twofa_secret',
        'fb_campaign_name',
        'type_of_ad_account',
        'ad_account_name',
        'ad_account_id',
        'active_ad_url',
        'business_manager_name',
        'business_manager_id',
        'business_verified',
        'starting_max_spend_of_account',
        'domain',
        'kk_master_server_name',
        'website_cleaned',
        'cost_of_ad_account',
        'page_name',
        'page_url',
        'page_id',
        'post_scheduled',
        'last_day_post_scheduled',
        'cc_added',
        'added_to_buyer_tool',
        'added_to_adcost',
        'engagement_lifetime_campaign',
        'engagement_lifetime_campaign_end_date',
        'like_lifetime_campaign',
        'like_lifetime_campaign_end_date',
        'daily_likes_camp',
        'domain_verified_in_fb',
        'pixel_id',
        'vertical',
        'media_buyer_id',
        'added_to_kk',
        'kk_campaign_status',
        'kk_account_name',
        'kk_campaign_title',
        'kk_campaign_url',
        'kk_campaign_id',
        'url_tracking_string',
        'passed_to_media_buyer',
        'lpv_camp',
        'wh_camp',
        'bh_camp',
        'media_buyer_passing_template',
        'long_notes',
        'terminated_card',
        'remove_from_kk',
        'archive',
        'add_s_tracking_fields',
        'added_pixel'
    ];

    protected $with = ["mediaBuyer"];

    public function fbAccountsLogs()
    {
        return $this->hasMany(FBAccountLog::class, 'account_id');
    }

    public function mediaBuyer()
    {
        return $this->belongsTo(User::class, 'media_buyer_id', 'id');
    }

    public function setNextStep()
    {
        $conditions = config('nextstep');
        $label  = "";

        foreach ($conditions as $condition) {
            $hasLabel = false;
            if (array_key_exists('status', $condition)) {
                if (in_array($this->status, $condition['status'])) {
                    $hasLabel = true;
                    if (array_key_exists('fields', $condition)) {
                        foreach ($condition['fields'] as $name => $value) {
                            if ($this->{$name} != $value) {
                                $hasLabel = false;
                            }
                        }
                    }
                }
            }
            if ($hasLabel) {
                $label = $condition['label'];
                break;
            }
        }
        $this->next_step = $label;
    }

    public function viewFbaccount()
    {
        if ($this) {
            $post = [];
            $user = new FBAccountLog();
            $accountLogs = $user::with('getUser')
                ->where('account_id', $this->id)
                ->latest()
                ->get();

            if ($this->fb_campaign_name == '') {
                $post['fb_campaign_name'] = $this->media_buyer_id . '_' . $this->offer . '_' . $this->ad_account_id . '_PUR2_' . date("Ymd") . '_Angle_Rulz';
                $this->fb_campaign_name = $post['fb_campaign_name'];
            }
            if ($this->url_tracking_string == '') {
                $post['url_tracking_string'] = 's1={{campaign.id}}&s3={{ad.id}}&s4=' . $this->ad_account_id;
                $this->url_tracking_string = $post['url_tracking_string'];
            }
            $this->update($post);

            return $accountLogs;
        }
    }
    public function scopeSpendToday($query)
    {
        return $query->addSelect([
            'spend_today' => FBAccountReport::selectRaw('IFNULL(sum(spend), 0) as today')
                ->whereColumn('fb_ad_account_id', 'fbaccounts.ad_account_id')
                ->where('start_date', '>', Carbon::now()->subDay()->format('Y-m-d'))
                ->where('end_date', '<=', Carbon::now()->format('Y-m-d'))
        ]);
    }
    public function scopeSpendYesterday($query)
    {
        return $query->addSelect([
            'spend_yesterday' => FBAccountReport::selectRaw('IFNULL(sum(spend), 0) as yesterday')
                ->whereColumn('fb_ad_account_id', 'fbaccounts.ad_account_id')
                ->whereDate('start_date', '>', Carbon::now()->subDay(2)->format('Y-m-d'))
                ->whereDate('end_date', '<=', Carbon::now()->subDay(1)->format('Y-m-d'))
        ]);
    }

    public function scopeRevenueToday($query)
    {
        return $query->addSelect([
            'revenue_today' => KKRevenueReport::selectRaw('IFNULL(sum(price), 0) as revenueToday')
                ->whereColumn('campaign_id', 'fbaccounts.kk_campaign_id')
                ->whereRaw('cast(time as signed) > ' . Carbon::now()->subDay()->timestamp)
                ->whereRaw('cast(time as signed) < ' . Carbon::now()->timestamp)
        ]);
    }
    public function scopeRevenueYesterday($query)
    {
        return $query->addSelect([
            'revenue_yesterday' => KKRevenueReport::selectRaw('IFNULL(sum(price), 0) as revenueYesterday')
                ->whereColumn('campaign_id', 'fbaccounts.kk_campaign_id')
                ->whereRaw('cast(time as signed) > ' .  Carbon::now()->subDay(2)->timestamp)
                ->whereRaw('cast(time as signed) < ' .  Carbon::now()->subDay(1)->timestamp)
        ]);
    }


    public function vertical(){
        return $this->hasOne(Vertical::class,'id','vertical');
    }

    public function source(){
        return $this->hasOne(Source::class,'id','source');
    }
}
