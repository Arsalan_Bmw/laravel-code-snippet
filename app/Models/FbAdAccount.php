<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FbAdAccount extends Model
{
    use HasFactory;
    protected $fillable=['ad_account_id','ad_account_name'];
    public function AdAccountReports(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\FbAccountsReport::class,'fb_ad_account_id','ad_account_id');
    }
}
