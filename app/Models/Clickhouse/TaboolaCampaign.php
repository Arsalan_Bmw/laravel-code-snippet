<?php
namespace App\Models\Clickhouse;

use PhpClickHouseLaravel\BaseModel;

class TaboolaCampaign extends BaseModel
{
    // Not necessary. Can be obtained from class name MyTable => my_table
    protected $table = 'taboola_campaigns';
}