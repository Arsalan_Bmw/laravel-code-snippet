<?php
namespace App\Models\Clickhouse;

use PhpClickHouseLaravel\BaseModel;

class CrossroadsAggregatedCampaignSitePerformance extends BaseModel
{
    // Not necessary. Can be obtained from class name MyTable => my_table
    protected $table = 'crossroads_aggregated_campaign_site_performances';
}