<?php

namespace App\Models\Clickhouse;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use PhpClickHouseLaravel\BaseModel;


class TaboolaAdvertiser extends BaseModel
{
    use HasFactory;

    protected $fillable = [
        'taboola_advertiser_id',
        'advertiser_name',
        'is_active',
        'default_platform',
        'type',
        'currency',
        'time_zone_name'
    ];
}
