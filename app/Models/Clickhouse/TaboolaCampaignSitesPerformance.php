<?php
namespace App\Models\Clickhouse;

use PhpClickHouseLaravel\BaseModel;

class TaboolaCampaignSitesPerformance extends BaseModel
{
    // Not necessary. Can be obtained from class name MyTable => my_table
    protected $table = 'taboola_campaign_sites_performances';
}