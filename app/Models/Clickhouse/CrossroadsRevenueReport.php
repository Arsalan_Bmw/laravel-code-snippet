<?php
namespace App\Models\Clickhouse;

use PhpClickHouseLaravel\BaseModel;

class CrossroadsRevenueReport extends BaseModel
{
    // Not necessary. Can be obtained from class name MyTable => my_table
    protected $table = 'crossroads_revenue_reports';
}