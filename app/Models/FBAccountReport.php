<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class FBAccountReport extends Model
{
    use Filterable;
    protected $fillable = [
        'fb_ad_account_id', 'clicks', 'cpm', 'ctr', 'frequency', 'spend',
        'start_date', 'end_date', 'campaign_id', 'ad_set_id', 'fb_id',
        'unique_clicks', 'outbound_clicks', 'inline_link_clicks',
        'conversions', 'impressions', 'reach', 'media_buyer_id',
        'media_buyer_id', 'offer_id'
    ];
    protected $table = 'fb_account_reports';



    public function adSet(): BelongsTo
    {
        return $this->belongsTo(FbAdSet::class, 'ad_set_id', 'ad_set_id');
    }

    public function fbCampaign(): BelongsTo
    {
        return $this->belongsTo(FbCampaign::class, 'campaign_id', 'campaign_id');
    }

    public function fbAdAccount(): BelongsTo
    {
        return $this->belongsTo(FbAdAccount::class, 'fb_ad_account_id', 'ad_account_id');
    }
    public function fbAd(): BelongsTo
    {
        return $this->belongsTo(FbAd::class, 'fb_id', 'fb_id');
    }
    public function fbAccount(): BelongsTo
    {
        return $this->belongsTo(FBAccount::class, 'fb_ad_account_id', 'ad_account_id');
    }
}
