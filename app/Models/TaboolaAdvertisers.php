<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;


use Bavix\LaravelClickHouse\Database\Eloquent\Model;
class TaboolaAdvertisers extends Model
{
    use HasFactory;

    protected $fillable =[
        'taboola_advertiser_id',
        'advertiser_name',
        'is_active',
        'default_platform',
        'type',
        'currency',
        'time_zone_name'
    ];
    
}
