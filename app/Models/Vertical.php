<?php

namespace App\Models;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vertical extends Model
{
    use HasFactory, Filterable, SoftDeletes;
    
    const ACTIVE = false;
    
    protected $fillable = ["long_name", "short_name",'archive'];

   public function scopeActive($query){
    $query->where('archive',Vertical::ACTIVE);
   }
}
