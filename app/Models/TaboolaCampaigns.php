<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Bavix\LaravelClickHouse\Database\Eloquent\Model;

class TaboolaCampaigns extends Model
{
    use HasFactory;

    protected $table='taboola_campaigns';
  
    protected $connection= 'clickhouse';

    protected $fillable = ['taboola_campaign_id','campaign_name','advertiser_id'];

    

}
