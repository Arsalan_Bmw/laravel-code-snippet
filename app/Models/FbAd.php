<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FbAd extends Model
{
    use HasFactory;
    protected $fillable=['fb_id','fb_name'];
    public function fbAdReports(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(\FbAccountsReport::class,'fb_id','fb_id');
    }
}
