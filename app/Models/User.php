<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Cartalyst\Sentinel\Users\EloquentUser as SentinelUser;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use App\Models\FBAccountLog;
use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends SentinelUser implements Authenticatable
{
    use AuthenticableTrait, Notifiable, HasApiTokens, HasRoles, Filterable, SoftDeletes;

    protected $guard_name = 'api';
    
    const ACTIVE = false;

    const IS_MEDIA_BUYER  = true;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * set primary key of table
     *
     * @var integer
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'permissions',
        'last_login',
        'first_name',
        'last_name',
        'archive',
        'is_media_buyer'
    ];

    /**
     * set logins field
     *
     * @var string
     */
    protected $loginNames = ['email'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('archive', self::ACTIVE);
    }

    public function oAthAccessToken()
    {
        return $this->hasMany('\App\OauthAccessToken');
    }

    public function getAccountLogs()
    {
        return $this->hasMany(FBAccountLog::class, 'user_id');
    }
    

}
