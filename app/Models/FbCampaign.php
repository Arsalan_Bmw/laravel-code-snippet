<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FbCampaign extends Model
{
    use HasFactory;
    protected $fillable=['campaign_id','campaign_name'];
    public function campaignReports(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(FBAccountReport::class,'campaign_id','campaign_id');
    }
}
