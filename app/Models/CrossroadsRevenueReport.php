<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrossroadsRevenueReport extends Model
{
    use HasFactory;
    protected $table = 'crossroads_revenue_reports';
    protected $fillable = [
        'publisher',
        'day',
        'gclid',
        'platform',
        'hour',
        'revenue_clicks',
        'total_visitors',
        'tracked_visitors',
        'publisher_revenue_amount',
        'referrer',
        'user_agent',
        'category',
        'city',
        'country_code',
        'device_type',
        'keyword',
        'lander_keyword',
        'revenue_keyword',
        'search_term',
        'state',
        'tg1',
        'tg4',
        'tg5',
        'lander_searches',
        'lander_visitors',
        'tg10',
        'tg7',
        'tg8',
        'tg9',
        'traffic_source',
        'campaign_name',
        'campaign_type',

        'browser',
        'tg6',
        'account_id',
        'tg2',
        'ad_id',
        'campaign_number',
        'tg3',
        'query_date',
        'match_type'
    ];
}
