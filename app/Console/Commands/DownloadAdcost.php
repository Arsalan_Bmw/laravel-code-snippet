<?php

namespace App\Console\Commands;

use App\Models\FBAccount;
use App\Models\FBAccountReport;
use App\Models\FbAd;
use App\Models\FbAdAccount;
use App\Models\FbAdSet;
use App\Models\FbCampaign;
use App\Models\MetaConfigration;
use App\Services\MetaConfigrationService;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DownloadAdcost extends Command
{
    public $secondToExecute = 2;

    public $attempt = 1;

    public $client;
    public $metaConfigrationService;
    public function __construct(MetaConfigrationService $metaConfigrationService) {
        parent::__construct();
        $this->metaConfigrationService = $metaConfigrationService;
    }
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'adcost:download {start_date?} {end_date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'download adCost';

    /**
     * Create a new command instance.
     *
     * @return void
     */


    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        
        try {
            $url = config('adcost.url') . "reports";
            $this->client = new Client();
            $startDate = $this->argument('start_date') !== null ? $this->argument('start_date') : Carbon::now()->subDays(1)->format('Y-m-d');
            $endDate = $this->argument('end_date') !== null ? $this->argument('end_date') : Carbon::now()->format('Y-m-d');
            $this->line('Creating Report');
            $response = $this->client->request(
                'POST',
                $url,
                ['form_params' => [
                    'date' => ["start" => $startDate, "end" => $endDate],
                    'advertiser' => "facebook",
                    'breakdown' => "day",
                    "level" => "ad",
                    "convert_to_usd" => true,
                    "selected_metrics" => ['clicks', 'cpm', 'ctr', 'frequency', 'spend', 'unique_clicks', 'outbound_clicks', 'inline_link_clicks', 'conversions', 'impressions', 'reach'],
                    "accounts" => [],
                    "api_token" => config('adcost.token')
                ]]
            );
            Log::info('report generated:' . $response->getBody()->getContents());

            $data = json_decode((string)$response->getBody(), true);
            $this->line('Report created with id ' . $data['data']['id']);
            //as Adcost returns 404 if we try to load reports too quickly after its created
            //wait for 10 seconds before proceeding
//            sleep(2);
            //wait for the report to be processed
            while (true) {
                sleep(pow($this->secondToExecute, $this->attempt));
                $this->line($this->isReportProcessed($data['data']['id']));
                if ($this->isReportProcessed($data['data']['id']) == 404) {
                    $this->attempt++;
                    $this->line('inc' . $this->attempt);
                } else {
                    $this->line($this->isReportProcessed($data['data']['id']));
                    break;

                }
            }
            $this->importReport($data['data']['id']);
            $this->line("Report processed");
        } catch (Exception $exception) {
            Log::error("Message:" . $exception->getMessage());
        }
    }

    public function isReportProcessed($reportId)
    {
        $url = config('adcost.url') . "reports";
        try {
            $response = $this->client->request('GET', $url . '/' . $reportId . '?api_token=' .config('adcost.token'), []);
            $this->line('response status code is ' . $response->getStatusCode());
            return $response->getStatusCode();
        } catch (ClientException $e) {
            return $e->getResponse()->getStatusCode();
        }
    }

    public function importReport($reportId)
    {
        $url = config('adcost.url') . "reports";
        $response = $this->client->request('GET', $url . '/' . $reportId . '?api_token=' . config('adcost.token'), []);
        $report = json_decode((string)$response->getBody(), true);
        Log::info((string)$response->getBody());
        $this->line("updating the DB");
        DB::beginTransaction();
        try {
            foreach ($report['data']['report'] as $key => $fbAd) {
                if (FbAd::where('fb_id', $fbAd['id'])->count() == 0) {
                    FbAd::create(['fb_id' => $fbAd['id'], 'fb_name' => $fbAd['name']]);
                }
                foreach ($fbAd['ad_accounts'] as $key => $adAccount) {
                    if (FbAdAccount::where('ad_account_id', $adAccount['ad_account_id'])->count() == 0) {
                        FbAdAccount::create(['ad_account_id' => $adAccount['ad_account_id'], 'ad_account_name' => $adAccount['ad_account_name']]);
                    }
                    $fbAdAccount = FBAccount::where('ad_account_id', $adAccount['ad_account_id'])->first();
                    
                    foreach ($adAccount['insights']['results']['value'] as $key => $value) {
                        if (FbCampaign::where('campaign_id', $value['campaign_id'])->count() == 0) {
                            FbCampaign::create(['campaign_id' => $value['campaign_id'], 'campaign_name' => $value['campaign_name']]);
                        }
                        if (FbAdSet::where('ad_set_id', $value['adset_id'])->count() == 0) {
                            FbAdSet::create(['ad_set_id' => $value['adset_id'], 'ad_set_name' => $value['adset_name']]);
                        }
                        if (FbAd::where('fb_id', $value['id'])->count() == 0) {
                            FbAd::create(['fb_id' => $value['id'], 'fb_name' => $value['name']]);
                        }

                        FBAccountReport::updateOrCreate([
                            'fb_ad_account_id' => $adAccount['ad_account_id'],
                            'start_date' => $value['date_start'],
                            'end_date' => $value['date_stop'],
                            'campaign_id' => $value['campaign_id'],
                            'ad_set_id' => $value['adset_id'],
                            'fb_id' => $value['id']
                        ], [
                            'clicks' => $value['clicks'],
                            'cpm' => $value['cpm'],
                            'ctr' => $value['ctr'],
                            'frequency' => $value['frequency'],
                            'spend' => $value['spend'],
                            'unique_clicks' => $value['unique_clicks'] == null ? 0 : $value['unique_clicks'],
                            'outbound_clicks' => $value['outbound_clicks'] == null ? 0 : $value['outbound_clicks'],
                            'inline_link_clicks' => $value['inline_link_clicks'] == null ? 0 : $value['inline_link_clicks'],
                            'conversions' => $value['conversions'] == null ? 0 : $value['conversions'],
                            'impressions' => $value['impressions'] == null ? 0 : $value['impressions'],
                            'reach' => $value['reach'] == null ? 0 : $value['reach'],
                            'media_buyer_id' => $fbAdAccount ? $fbAdAccount->media_buyer_id : null,
                            'offer_id' => $fbAdAccount ? $fbAdAccount->offer : null,
                        ]);
                        $this->metaConfigrationService->updateOrCreateMetadata(MetaConfigration::TYPE_ADCOST);
                    }
                }
            }
            DB::commit();
            $this->line("Done updating the DB");
        } catch (Exception $e) {
            DB::rollBack();
            Log::info("exception" . $e);
        }
    }
}
