<?php

namespace App\Console\Commands;

use App\Models\KKRevenueReport;
use App\Models\MetaConfigration;
use App\Services\MetaConfigrationService;
use Carbon\Carbon;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class DownloadKK extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kk:download {start_date?} {end_date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download KK Transactions';

    public $metaConfigrationService;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(MetaConfigrationService $metaConfigrationService)
    {
        parent::__construct();
        $this->metaConfigrationService = $metaConfigrationService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
      
        
        // try {
        $url = config('kk.url') . 'conversion/' . config('kk.group_id');
        $this->client = new Client();
        $startDate = $this->argument('start_date') !== null ? $this->argument('start_date') : Carbon::now()->subDays(1)->format('Y/m/d');
        $endDate = $this->argument('end_date') !== null ? $this->argument('end_date') : Carbon::now()->format('Y/m/d');
        $response = $this->client->request(
            'GET',
            $url,
            [
                'query' => [
                    "sdate" => $startDate, "edate" => $endDate,
                ],
                'headers' => ["X-Api-Key" => config('kk.api_key')]
            ]
        );
        $data = json_decode((string)$response->getBody(), true);
        foreach ($data as $record) {
            KKRevenueReport::updateOrCreate([
                'click_id' => $record['click_id'],
                'time' => $record['time'],
                'campaign_id' => $record['campaign_id']
            ], $record);
        }
        $this->metaConfigrationService->updateOrCreateMetadata(MetaConfigration::TYPE_KK_REVENUE);
        $this->line('Processed Successfully');
    }
}
