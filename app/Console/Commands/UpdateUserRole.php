<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Role;

class UpdateUserRole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:userRole';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update user role';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
       $users = User::query()->whereHas("roles", function($q){ $q->whereNotIn("name", ["admin","superadmin"]); })->get();
       foreach($users as $user){
        $user->syncRoles(['user']);
      }
    }
}
