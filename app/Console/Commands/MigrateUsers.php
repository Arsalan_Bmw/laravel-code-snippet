<?php

namespace App\Console\Commands;

use App\Models\User;
use Cartalyst\Sentinel\Roles\EloquentRole;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class MigrateUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate sentinel user roles to spatie';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $roles = DB::table('roles')->get();
        foreach ($roles as $role) {
            Role::create(['name' => $role->name, 'guard_name' => 'api']);
        }
        $userRoles = DB::table('role_users')->get();

        foreach ($userRoles as $userRole) {
            User::find($userRole->user_id)
                ->assignRole(Role::where(["name" => EloquentRole::find($userRole->role_id)->name])->get());
        }

        //drop the sentinent tables
        DB::table('role_users')->truncate();
        DB::table('roles')->truncate();
    }
}
