<?php

namespace App\Console\Commands;

use App\Models\Clickhouse\CrossroadsAggregatedCampaignPerformance;
use App\Models\Clickhouse\CrossroadsAggregatedCampaignSitePerformance;
use App\Models\Clickhouse\CrossroadsRevenueReport;
use App\Models\CrossroadsFacebookRevenueReport;
use App\Models\CrossroadsTaboolaRevenueReport;
use App\Models\MetaConfigration;
use App\Services\MetaConfigrationService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use GuzzleHttp\Client;
use Exception;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DownloadCrossroadsMediaRevenue extends Command
{
    public $client;
    public $secondToExecute = 2;
    public $url, $key, $date;
    public $attempt = 1;
    public $metaConfigrationService;
    public $selectedColumns = [
        "ad_id", "browser", "campaign_number", "category", "city",
        "country_code", "day", "device_type", "gclid", "hour",
        "keyword", "lander_keyword", "platform", "publisher",
        "referrer", "revenue_keyword", "search_term", "state",
        "tg1", "tg10", "tg2", "tg3", "tg4", "tg5", "tg6",
        "tg7", "tg8", "tg9", "traffic_source", "user_agent", "match_type"
    ];
    private $changedTimestamps = [];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crossroads:download {start_date?} {end_date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(MetaConfigrationService $metaConfigrationService)
    {
        parent::__construct();
        $this->metaConfigrationService = $metaConfigrationService;
        $this->client = new Client();
        $this->url = config('crossroads.url');
        $this->key = config('crossroads.key');
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $startDate = $this->argument('start_date') !== null ? $this->argument('start_date') : Carbon::now()->subDays(1)->format('Y-m-d');
        $endDate = $this->argument('end_date') !== null ? $this->argument('end_date') : Carbon::now()->format('Y-m-d');
        try {
            // $dataUrl = $this->requestData($startDate, $endDate);
            // $this->importReport($dataUrl, $startDate, $endDate);
            $this->importReport('https://s3-us-west-2.amazonaws.com/cr-api-v2-bulk-data/c186bd8f-36a7-46d4-89cd-2339e86441dd.json', $startDate, $endDate);
            $this->metaConfigrationService->updateOrCreateMetadata(MetaConfigration::TYPE_CROSSROADS);
        } catch (Exception $e) {
            $this->line("Error :" . $e->getMessage());
            return;
        }
    }

    public function requestData($startDate, $endDate)
    {
        // $totalDays = Carbon::parse($startDate)->diffInDays(Carbon::parse($endDate));
        $params = [
            "query" => [
                "key" => $this->key,
                "start_date" => $startDate,
                "end_date" => $endDate,
                "extra-fields" => implode(",", $this->selectedColumns),
                "format" => "json"
            ]
        ];
        $response = $this->client->request(
            'GET',
            $this->url . 'prepare-bulk-data',
            $params
        );
        $generatecrossRoadRevenueReportId = json_decode((string)$response->getBody(), true);
        while (true) {
            try {
                sleep(pow($this->secondToExecute, $this->attempt));
                if (!$this->isReportProcessed($generatecrossRoadRevenueReportId['request_id'])) {
                    $this->attempt++;
                    $this->line('inc' . $this->attempt);
                } else {
                    $this->line($this->isReportProcessed($generatecrossRoadRevenueReportId['request_id']));
                    break;
                }
            } catch (Exception $e) {
            }
        }

        $this->line("Processing reports");
        $response = $this->client->request('GET', $this->url . 'get-request-state?key=' . $this->key . '&request-id=' . $generatecrossRoadRevenueReportId['request_id']);
        $this->line((string)$response->getBody());
        $revenueReports = json_decode((string)$response->getBody(), true);
        Log::info(json_encode($revenueReports));
        $this->line($revenueReports);

        return $revenueReports['file_url'];
    }
    public function isReportProcessed($reportId)
    {
        try {
            $response = $this->client->request('GET', $this->url . 'get-request-state?key=' . $this->key . '&request-id=' . $reportId);
            $this->line('response status code is ' . $response->getStatusCode());
            $this->line('response is ' . $response->getBody());
            $responseData = json_decode((string)$response->getBody(), true);
            if ($responseData['status'] === "SUCCESS") {
                return true;
            } else {
                return false;
            }
            return $response->getStatusCode();
        } catch (ClientException $e) {
            throw $e;
        }
    }
    public function importReport($reportUrl, $startDate, $endDate)
    {
        $this->line("Loading report from URL " . $reportUrl);
        $revenueReportsEncodedRecords = file_get_contents($reportUrl);
        if ($revenueReportsEncodedRecords) {
            CrossroadsRevenueReport::where('api_start_date', $startDate)
                ->where('api_end_date', $endDate)->delete();
        }

        $this->line("Importing revenue to tables");
        $reportRows = [];
        foreach (json_decode($revenueReportsEncodedRecords, true) as $report) {
            array_push($this->changedTimestamps, [
                'day' => $report['day'],
                'hour' => Carbon::createFromFormat('H', $report['hour'])->format('H:i:s')
            ]);

            array_push($reportRows, [
                'publisher' => str_replace("'", " ", $report['publisher']),
                'day' => $report['day'],
                'gclid' => $report['gclid'],
                'platform' => $report['platform'],
                'hour' => Carbon::createFromFormat('H', $report['hour'])->format('H:i:s'),
                'revenue_clicks' => $report['revenue_clicks'],
                'total_visitors' => $report['total_visitors'],
                'tracked_visitors' => $report['tracked_visitors'],
                'publisher_revenue_amount' => $report['publisher_revenue_amount'],
                'referrer' => $report['referrer'],
                'user_agent' => $report['user_agent'],
                'category' => $report['category'],
                'city' => str_replace("'", " ", $report['city']),
                'country_code' => $report['country_code'],
                'device_type' => $report['device_type'],
                'keyword' => $report['keyword'],
                'lander_keyword' => $report['lander_keyword'],
                'revenue_keyword' => $report['revenue_keyword'],
                'search_term' => $report['search_term'],
                'state' => $report['state'],
                'tg1' => $report['tg1'],
                'tg4' => $report['tg4'],
                'tg5' => $report['tg5'],
                'lander_searches' => $report['lander_searches'],
                'lander_visitors' => $report['lander_visitors'],
                'tg10' => $report['tg10'],
                'tg7' => $report['tg7'],
                'tg8' => $report['tg8'],
                'tg9' => $report['tg9'],
                'traffic_source' => $report['traffic_source'],
                'campaign_name' => str_replace("'", " ", $report['campaign__name']),
                'campaign_type' => $report['campaign__type'],
                'browser' => $report['browser'],
                'tg6' => $report['tg6'],
                'account_id' => $report['campaign_id'],
                'tg2' => $report['tg2'],
                'ad_id' => $report['ad_id'],
                'campaign_number' => $report['campaign_number'],
                'tg3' => $report['tg3'],
                'query_date' => $this->date,
                'match_type' => intval($report['match_type']),
                'api_start_date' => $startDate,
                'api_end_date' => $endDate,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')

            ]);

            if (count($reportRows) > 500) {
                Log::info(json_encode($reportRows));
                CrossroadsRevenueReport::insertBulk($reportRows);
                $reportRows = [];
            }
        }
        if (count($reportRows) > 0) {
            CrossroadsRevenueReport::insertBulk($reportRows);
        }
        $this->line("Updating aggregate tables");
        $this->updateAgreegates();
    }

    function updateAgreegates()
    {
        $aggregatedCampaignPerformanceRows = [];
        $aggregatedCampaignSitePerformanceRows = [];
        foreach ($this->changedTimestamps as $changedTimestamp) {
            CrossroadsAggregatedCampaignPerformance::where('day', $changedTimestamp['day'])
                ->where('day', $changedTimestamp['day'])
                ->delete();
            CrossroadsAggregatedCampaignSitePerformance::where('day', $changedTimestamp['day'])
                ->where('day', $changedTimestamp['day'])
                ->delete();

            //get the sum of metrics
            $row = DB::connection('clickhouse')->table('crossroads_revenue_reports')
                ->where('day', $changedTimestamp['day'])
                ->where('hour', $changedTimestamp['hour'])
                ->first([
                    DB::raw('SUM(lander_searches) as lander_searches'),
                    DB::raw('SUM(lander_visitors) as lander_visitors'),
                    DB::raw('SUM(publisher_revenue_amount) as publisher_revenue_amount'),
                    DB::raw('SUM(revenue_clicks) as revenue_clicks'),
                    DB::raw('SUM(total_visitors) as total_visitors'),
                    DB::raw('SUM(tracked_visitors) as tracked_visitors')
                ]);
            array_push(
                $aggregatedCampaignPerformanceRows,
                [
                    "lander_searches" => $row['lander_searches'],
                    "publisher_revenue_amount" => $row['publisher_revenue_amount'],
                    "revenue_clicks" => $row['revenue_clicks'],
                    "total_visitors" => $row['total_visitors'],
                    "tracked_visitors" => $row['tracked_visitors'],
                    "day" => $changedTimestamp["day"],
                    "hour" => $changedTimestamp["hour"]
                ]
            );

            if (count($aggregatedCampaignPerformanceRows) > 500) {
                CrossroadsAggregatedCampaignPerformance::insertAssoc($aggregatedCampaignPerformanceRows);
                $aggregatedCampaignPerformanceRows = [];
            }

            //get the sum of metrics
            $rows = DB::connection('clickhouse')->table('crossroads_revenue_reports')
                ->where('day', $changedTimestamp['day'])
                ->where('hour', $changedTimestamp['hour'])
                ->groupBy('match_type')
                ->get([
                    DB::raw('SUM(lander_searches) as lander_searches'),
                    DB::raw('SUM(lander_visitors) as lander_visitors'),
                    DB::raw('SUM(publisher_revenue_amount) as publisher_revenue_amount'),
                    DB::raw('SUM(revenue_clicks) as revenue_clicks'),
                    DB::raw('SUM(total_visitors) as total_visitors'),
                    DB::raw('SUM(tracked_visitors) as tracked_visitors'),
                    DB::raw('match_type')
                ]);
            foreach ($rows as $row) {
                array_push(
                    $aggregatedCampaignSitePerformanceRows,
                    [
                        "site_id" => $row['match_type'],
                        "lander_searches" => $row['lander_searches'],
                        "lander_visitors" => $row['lander_visitors'],
                        "publisher_revenue_amount" => $row['publisher_revenue_amount'],
                        "revenue_clicks" => $row['revenue_clicks'],
                        "total_visitors" => $row['total_visitors'],
                        "tracked_visitors" => $row['tracked_visitors'],
                        "day" => $changedTimestamp["day"],
                        "hour" => $changedTimestamp["hour"]
                    ]
                );
                if (count($aggregatedCampaignSitePerformanceRows) > 500) {
                    CrossroadsAggregatedCampaignSitePerformance::insertAssoc($aggregatedCampaignSitePerformanceRows);
                    $aggregatedCampaignSitePerformanceRows = [];
                }
            }
        }
        if (count($aggregatedCampaignPerformanceRows) > 0) {
            CrossroadsAggregatedCampaignPerformance::insertAssoc($aggregatedCampaignPerformanceRows);
        }
        if (count($aggregatedCampaignSitePerformanceRows) > 0) {
            CrossroadsAggregatedCampaignSitePerformance::insertAssoc($aggregatedCampaignSitePerformanceRows);
        }
    }
}
