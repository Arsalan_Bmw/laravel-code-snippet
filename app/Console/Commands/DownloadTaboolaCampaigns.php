<?php

namespace App\Console\Commands;

use App\Models\Clickhouse\TaboolaAdvertiser;
use App\Models\Clickhouse\TaboolaCampaign;
use App\Models\Clickhouse\TaboolaCampaignPerformance;
use App\Models\Clickhouse\TaboolaCampaignSitesPerformance;
use App\Models\Clickhouse\TaboolaSite;
use App\Services\ConvertToUTC;
use App\Services\MetaConfigrationService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use TaboolaSites;

class DownloadTaboolaCampaigns extends Command
{
    public $client;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'taboola:download {start_date?} {end_date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download tabaloo campaign data';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public $loginUrl, $clientId, $clientSecret, $grantType, $baseUrl, $siteName;
    public $metaConfigrationService;
    public $convertToUTC;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(MetaConfigrationService $metaConfigrationService, ConvertToUTC $convertToUTC)
    {
        parent::__construct();
        $this->convertToUTC = $convertToUTC;
        $this->loginUrl =   config('taboola.login_url');
        $this->clientId =   config('taboola.client_id');
        $this->clientSecret  =   config('taboola.client_secret');
        $this->grantType =   config('taboola.grant_type');
        $this->baseUrl   =   config('taboola.base_url');
        $this->metaConfigrationService = $metaConfigrationService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $startDate = $this->argument('start_date') !== null ? $this->argument('start_date') : Carbon::now()->subDays(1)->format('Y-m-d');
        $endDate   = $this->argument('end_date') !== null ? $this->argument('end_date') : Carbon::now()->format('Y-m-d');
        $this->line("Starting");
        $token     = $this->getAccessToken();
        $this->line("Access Token Loaded");
        $this->getAdvertiser($token);
        $this->getTaboolaCampaigns($token);
        $this->getTaboolaCampaignsPerformance($token, $startDate, $endDate);
        $this->getTaboolaSites($token, $startDate, $endDate);
    }


    private function getAccessToken()
    {
        $this->client = new Client();
        $getAccessToken = $this->client->request(
            'POST',
            $this->loginUrl,
            ['form_params' => [
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
                "grant_type" => $this->grantType,

            ]]
        );

        return  json_decode((string)$getAccessToken->getBody())->access_token;
    }

    private function getAdvertiser($token)
    {
        $this->line("Loading Advertisers");
        $advertiserResponse =   $this->client->request(
            'GET',
            $this->baseUrl . 'advertisers',
            [
                'headers' =>
                [
                    'Authorization' => "Bearer " . $token
                ]
            ]
        );

        $advertisers = json_decode((string)$advertiserResponse->getBody());
        foreach ($advertisers->results as $key => $advertiser) {
            echo ".";
            TaboolaAdvertiser::where('taboola_advertiser_id', $advertiser->id)->delete();
            TaboolaAdvertiser::make([
                'taboola_advertiser_id' => $advertiser->id,
                'advertiser_name' => $advertiser->name,
                'is_active' => $advertiser->is_active,
                'default_platform' => $advertiser->default_platform,
                'type' => $advertiser->type,
                'currency' => $advertiser->currency,
                'time_zone_name' => $advertiser->time_zone_name
            ])->save();
        }
        echo "\n";
    }

    private function getTaboolaCampaigns($token)
    {
        $this->line("Loading Taboola Campaigns");
        $taboolaCampaign = $this->client->request(
            'GET',
            $this->baseUrl . 'campaigns/base',
            [
                'headers' =>
                [
                    'Authorization' => "Bearer " . $token
                ]
            ]
        );

        $campaignsData = json_decode((string)$taboolaCampaign->getBody());

        foreach ($campaignsData->results as $campaign) {
            echo ".";
            TaboolaCampaign::where('taboola_campaign_id', $campaign->id)->delete();
            TaboolaCampaign::make([
                'taboola_campaign_id' => $campaign->id,
                'campaign_name' => $campaign->name,
                'media_buyer_id' => null,
                'advertiser_id' => $campaign->advertiser_id
            ])->save();
        }

        echo "\n";
    }

    private function getTaboolaCampaignsPerformance($token, $startDate, $endDate)
    {
        $this->line("Loading Taboola Campaigns Performance");
        $taboolaCampaignByHour = $this->client->request(
            'GET',
            $this->baseUrl . 'reports/campaign-summary/dimensions/campaign_hour_breakdown?start_date=' . $startDate . '&end_date=' . $endDate,
            [
                'headers' =>
                [
                    'Authorization' => "Bearer " . $token
                ]
            ]
        );

        $taboolaCampaignByHourData = json_decode((string)$taboolaCampaignByHour->getBody());

        foreach ($taboolaCampaignByHourData->results as $key => $campaign) {
            echo ".";
            $date = $this->convertToUTC->getUTCDateTime(Carbon::parse($campaign->date)->format('Y-m-d H:i:s'), $taboolaCampaignByHourData->timezone);
            TaboolaCampaignPerformance::where('campaign_id', (int)$campaign->campaign)
                ->where('timestamp', Carbon::parse($date)->format('Y-m-d H:i:s'))
                ->where('day',  Carbon::parse($date)->format('Y-m-d'))
                ->where('hour', Carbon::parse($date)->format('H:i:s'))->delete();
            TaboolaCampaignPerformance::make([
                'campaign_id' => (int)$campaign->campaign,
                'clicks' => $campaign->clicks,
                'impressions' => $campaign->impressions,
                'visible_impressions' => $campaign->visible_impressions,
                'spent' => $campaign->spent,
                'cpa_actions_num' => $campaign->cpa_actions_num,
                'currency' =>  $campaign->currency,
                'timestamp' => Carbon::parse($date)->format('Y-m-d H:i:s'),
                'conversions_value' => $campaign->conversions_value,
                'cpa_conversion_rate' => $campaign->cpa_conversion_rate,
                'day' => Carbon::parse($date)->format('Y-m-d'),
                'hour' => Carbon::parse($date)->format('H:i:s'),
                'api_start_date' => $startDate,
                'api_end_date' => $endDate
            ])->save();
        }
        echo "\n";
    }


    private function getTaboolaSites($token, $startDate, $endDate)
    {
        $this->line("Getting Taboola Sites");
        $campaignSiteByDay   =   $this->client->request(
            'GET',
            $this->baseUrl . 'reports/campaign-summary/dimensions/campaign_site_day_breakdown?start_date=' . $startDate . '&end_date=' . $endDate,
            [
                'headers' =>
                [
                    'Authorization' => "Bearer " . $token
                ]
            ]
        );

        $campaignSiteByDayData = json_decode((string)$campaignSiteByDay->getBody());

        $this->line(json_encode($campaignSiteByDayData));

        $this->line("Total Campaigns to be downloaded" . count($campaignSiteByDayData->results));
        $siteRows = [];
        $siteIds = [];
        foreach ($campaignSiteByDayData->results as $key => $site) {
            array_push($siteIds, $site->site_id);
            $this->siteName = $site->site_name;
            $siteName = str_replace("'", " ", $site->site_name);
            $siteRows[$site->site_id] = [
                "taboola_site_id" => $site->site_id,
                "site_name" => $siteName
            ];
        }
        TaboolaSite::whereRaw('taboola_site_id IN (' . implode(',', $siteIds) . ')')->delete();
        TaboolaSite::insertAssoc($siteRows);

        $sites = TaboolaSite::select()->getRows();
        $this->line("Total Sites - " . count($sites));

        foreach ($sites as $key => $site) {
            $rows = [];
            $campaignIds = [];
            echo ".";
            try {

                $campaignBySiteName   =   $this->client->request(
                    'GET',
                    $this->baseUrl . 'reports/campaign-summary/dimensions/campaign_hour_breakdown?start_date=' . $startDate . '&end_date=' . $endDate . '&site=' . $this->siteName,
                    [
                        'headers' =>
                        [
                            'Authorization' => "Bearer " . $token
                        ]
                    ]
                );
                $campaignsBySiteNameData = json_decode((string)$campaignBySiteName->getBody())->results;


                foreach ($campaignsBySiteNameData as $campaign) {
                    $date = $this->convertToUTC->getUTCDateTime(Carbon::parse($campaign->date)->format('Y-m-d H:i:s'), json_decode((string)$campaignBySiteName->getBody())->timezone);
                    array_push($campaignIds, $campaign->campaign);
                    array_push($rows, [
                        'campaign_id' => (int)$campaign->campaign,
                        'clicks' => $campaign->clicks,
                        'site_id' => (int)$site['taboola_site_id'],
                        'impressions' => $campaign->impressions,
                        'visible_impressions' => $campaign->visible_impressions,
                        'spent' => $campaign->spent,
                        'cpa_actions_num' => $campaign->cpa_actions_num,
                        'currency' =>  $campaign->currency,
                        'timestamp' => Carbon::parse($date)->format('Y-m-d H:i:s'),
                        'conversions_value' => $campaign->conversions_value,
                        'cpa_conversion_rate' => $campaign->cpa_conversion_rate,
                        'day' => Carbon::parse($date)->format('Y-m-d'),
                        'hour' => Carbon::parse($date)->format('H:i:s'),
                        'api_start_date' => $startDate,
                        'api_end_date' => $endDate
                    ]);
                }
                TaboolaCampaignSitesPerformance::whereRaw('campaign_id IN (' . implode(',', $campaignIds) . ')')
                    ->where('site_id', (int)$site['taboola_site_id'])
                    ->where('day', '>=', Carbon::parse($startDate)->format('Y-m-d'))
                    ->where('day', '<=', Carbon::parse($endDate)->format('Y-m-d'))->delete();
                TaboolaCampaignSitesPerformance::insertAssoc($rows);
            } catch (ClientException $e) {

                if ($e->getResponse()->getStatusCode() == 404 || $e->getResponse()->getStatusCode() == 400) {
                    continue;
                }
            }
        }
        echo "\n";
    }
}
