<?php

namespace App\Console;

use App\Console\Commands\DownloadCrossroadsMediaRevenue;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\DownloadAdcost::class,
        commands\DownloadTaboolaCampaigns::class,
        commands\DownloadCrossroadsMediaRevenue::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $startDate = Carbon::now()->subDays(1)->format('Y-m-d');
        $endDate = Carbon::now()->format('Y-m-d');
        $schedule->command(
            'adcost:download' . ' ' .
                $startDate . ' ' .
                $endDate
        )
            ->hourly();
        $schedule->command(
            'crossroads:download' . ' ' .
                $endDate . ' ' .
                $endDate
        )
            ->hourly();

        $schedule->command(
            'taboola:download' . ' ' .
                $startDate . ' ' .
                $endDate
        )
            ->hourly()->withoutOverlapping();

        //add download KK
        $schedule->command(
            'kk:download ' .
                Carbon::now()->subDays(1)->format('Y/m/d') . ' ' .
                Carbon::now()->format('Y/m/d')
        )->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
