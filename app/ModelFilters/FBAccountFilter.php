<?php

namespace App\ModelFilters;

use EloquentFilter\ModelFilter;
use Illuminate\Support\Facades\Log;

class FBAccountFilter extends ModelFilter
{
    /**
     * Related Models that have ModelFilters as well as the method on the ModelFilter
     * As [relationMethod => [input_key1, input_key2]].
     *
     * @var array
     */
    public $relations = [];

    public function searchTerm($name)
    {
        return $this->where(function ($q) use ($name) {
            return $q->where('build_name', 'LIKE', "%$name%")
                ->orWhere('next_step', 'LIKE', "%$name%")
                ->orWhere('quick_note', 'LIKE', "%$name%")
                ->orWhere('accesspoint_notes', 'LIKE', "%$name%")
                ->orWhere('fb_campaign_name', 'LIKE', "%$name%")
                ->orWhere('ad_account_name', 'LIKE', "%$name%")
                ->orWhere('id', 'LIKE', "%$name%");
        });
    }

    public function mediaBuyer($mediaBuyer)
    {
        return $this->where('media_buyer_id', $mediaBuyer);
    }

    public function source($source)
    {
        return $this->where('source', $source);
    }

    public function status($status)
    {
        return $this->where('status', $status);
    }

    public function archive($include)
    {
        return $this->where(function ($q) use ($include) {
            if($include) {
                return $q;
            } else {
                return $q->where('archive', '<>', "1");
            }
        });
    }


    public function sortColumn($orderBy)
    {
        $this->orderBy($orderBy, $this->input('sort_dir', 'DESC'));
    }
}
