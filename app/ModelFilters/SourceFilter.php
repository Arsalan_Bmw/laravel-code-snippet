<?php 

namespace App\ModelFilters;

use EloquentFilter\ModelFilter;

class SourceFilter extends ModelFilter
{
    /**
    * Related Models that have ModelFilters as well as the method on the ModelFilter
    * As [relationMethod => [input_key1, input_key2]].
    *
    * @var array
    */
    public $relations = [];

    public function searchTerm($name)
    {
        return $this->where(function ($q) use ($name) {
            return $q->where('long_name', 'LIKE', "%$name%")
                ->orWhere('short_name', 'LIKE', "%$name%");
        });
    }

    public function sortColumn($orderBy)
    {
        $this->orderBy($orderBy, $this->input('sort_dir', 'DESC'));
    }
}
