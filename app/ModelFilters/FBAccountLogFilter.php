<?php

namespace App\ModelFilters;

use EloquentFilter\ModelFilter;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FBAccountLogFilter extends ModelFilter
{
    /**
    * Related Models that have ModelFilters as well as the method on the ModelFilter
    * As [relationMethod => [input_key1, input_key2]].
    *
    * @var array
    */
    public $relations = ['getUser'];
    public function searchTerm($name)
    {

        return $this->where(function ($q) use ($name) {
            return $q->where('log_messages', 'LIKE', "%$name%")
                ->orWhereHas('getUser',function($q) use($name){
                    $q->where(DB::raw('CONCAT_WS(" ", first_name, last_name)'), 'like', "%$name%");

                });

        });
    }
    public function sortColumn($orderBy)
    {
        if($orderBy=='first_name'){
        return $this->whereHas('getUser',function ($q) use ($orderBy) {
            $q->orderBy($orderBy, $this->input('sort_dir', 'DESC'));

        });
        }else{
            $this->orderBy($orderBy, $this->input('sort_dir', 'DESC'));
        }
    }
}
