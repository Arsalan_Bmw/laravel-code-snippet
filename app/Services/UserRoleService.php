<?php


namespace App\Services;

use Illuminate\Support\Facades\Auth;

class UserRoleService{
   public function isAdmin(){
      $roles = Auth::user()->roles->pluck('name')->toArray();
      return count(array_intersect(['admin','superadmin'], $roles))>0 ? true : false ;
      
   }
}