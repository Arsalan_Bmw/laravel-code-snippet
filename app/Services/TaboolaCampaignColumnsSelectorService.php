<?php


namespace App\Services;

use Illuminate\Support\Facades\DB;

class TaboolaCampaignColumnsSelectorService
{
    public $metrics, $breakdowns, $tableName,$crTableName;
    public function __construct($metrics, $breakdowns)
    {
        $this->metrics = $metrics;
        $this->breakdowns = $breakdowns;
        if (
            (in_array('taboola_campaign_id', $this->breakdowns) && !in_array('taboola_site_id', $this->breakdowns))
            ||
            (!in_array('taboola_campaign_id', $this->breakdowns) && !in_array('taboola_site_id', $this->breakdowns))
        ) {
            $this->tableName   =   'taboola_campaign_performances';
            $this->crTableName  =    'crossroads_revenue_report_groupeds';
        }

        if (
            (in_array('taboola_campaign_id', $this->breakdowns) && in_array('taboola_site_id', $this->breakdowns))
            ||
            (!in_array('taboola_campaign_id', $this->breakdowns) && in_array('taboola_site_id', $this->breakdowns))
        ) {
            $this->tableName    =   'taboola_campaign_sites_performances';
            $this->crTableName  =    'crossroads_revenue_report_site_groupeds';
        }
    }



    public function getSelects()
    {
        $selectedColumns = [];
        foreach ($this->metrics as $metric) {
            switch ($metric) {
                case 'conversions':
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(sum(" . $this->tableName . ".cpa_actions_num), 0) as conversions")
                    );
                    break;
                case 'campaign_number':
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(count(DISTINCT(" . $this->tableName . ".campaign_id)), 0) as campaign_number")
                    );
                    break;
                case 'clicks':
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(sum(" . $this->tableName . ".clicks), 0) as clicks")
                    );
                    break;
                case 'impressions':
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(sum(" . $this->tableName . ".impressions), 0) as impressions")
                    );
                    break;
                case 'visible_impressions':
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(sum(" . $this->tableName . ".visible_impressions), 0) as visible_impressions")
                    );
                    break;
                case 'spent':
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(sum(" . $this->tableName . ".spent), 0) as spent")
                    );
                    break;
                case 'ctr':
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(sum(" . $this->tableName . ".clicks/" . $this->tableName . ".impressions), 0) as ctr")
                    );
                    break;
                case 'vctr':
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(sum(" . $this->tableName . ".clicks/" . $this->tableName . ".visible_impressions), 0) as vctr")
                    );
                    break;
                case 'cpm':
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(sum((" . $this->tableName . ".spent / " . $this->tableName . ".impressions )/ 1000), 0) as cpm")
                    );
                    break;
                case 'vcpm':
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(sum((" . $this->tableName . ".spent / " . $this->tableName . ".visible_impressions )/ 1000), 0) as vcpm")
                    );
                    break;
                case 'cpc':
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(sum(" . $this->tableName . ".clicks / " . $this->tableName . ".spent) , 0) as cpc")
                    );
                    break;
                case 'cpa':
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(sum(" . $this->tableName . ".spent/" . $this->tableName . ".cpa_actions_num), 0) as cpa")
                    );
                    break;
                case 'cpa_conversion_rate':
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(sum(" . $this->tableName . ".cpa_actions_num/" . $this->tableName . ".clicks) , 0) as cpa_conversion_rate")
                    );
                    break;
                case "timestamp":
                    array_push(
                        $selectedColumns,
                        DB::raw($this->tableName . ".timestamp as timestamp")
                    );
                    break;
                case "lander_searches":
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(sum(".$this->crTableName.".cr_lander_searches) , 0) as lander_searches")
                    );
                    break;
                case "lander_visitors":
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(sum(".$this->crTableName.".cr_lander_visitors) , 0) as lander_visitors")
                    );
                    break;
                case "publisher_revenue_amount":
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(sum(".$this->crTableName.".cr_publisher_revenue_amount) , 0) as publisher_revenue_amount")
                    );
                    break;
                case "revenue_clicks":
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(sum(".$this->crTableName.".cr_revenue_clicks) , 0) as revenue_clicks")
                    );
                    break;

                case "total_visitors":
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(sum(".$this->crTableName.".cr_total_visitors) , 0) as total_visitors")
                    );
                    break;
                case "tracked_visitors":
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(sum(".$this->crTableName.".cr_tracked_visitors) , 0) as tracked_visitors")
                    );
                    break;
                case "Ob_ctr":
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(sum(".$this->crTableName.".cr_revenue_clicks/".$this->crTableName.".cr_tracked_visitors) , 0) as Ob_ctr")
                    );
                    break;
                case "rpc":
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(sum(".$this->crTableName.".cr_publisher_revenue_amount/".$this->crTableName.".cr_revenue_clicks) , 0) as rpc")
                    );
                    break;

                case "profit":
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(sum(".$this->crTableName.".cr_publisher_revenue_amount-" . $this->tableName . ".spent) , 0) as profit")
                    );
                    break;
                case "rpm":
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(sum((100/".$this->crTableName.".cr_tracked_visitors)*".$this->crTableName.".cr_publisher_revenue_amount) , 0) as rpm")
                    );
                    break;
                case "rpv":
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(sum(".$this->crTableName.".cr_publisher_revenue_amount/".$this->crTableName.".cr_tracked_visitors) , 0) as rpv")
                    );
                    break;
                case "roas":
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(sum(".$this->crTableName.".cr_publisher_revenue_amount/" . $this->tableName . ".spent) , 0) as roas")
                    );
                    break;
                case "roi":
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(sum( (".$this->crTableName.".cr_publisher_revenue_amount - " . $this->tableName . ".spent)/" . $this->tableName . ".spent)*100 , 0) as roi")
                    );
                    break;
                case "profit_margin":
                    array_push(
                        $selectedColumns,
                        DB::raw("IFNULL(sum((".$this->crTableName.".cr_publisher_revenue_amount -" . $this->tableName . ".spent)/".$this->crTableName.".cr_publisher_revenue_amount) , 0) as profit_margin")
                    );
                    break;
            }
        }
        return $selectedColumns;
    }

    public function AddingDefaultbreakdown()
    {

        foreach ($this->breakdowns as $breakdown) {
            if ($breakdown == 'advertiser_id') {
                array_push($this->breakdowns, 'advertiser_name');
            }
            if ($breakdown == 'day') {
                array_push($this->breakdowns, 'report_at');
            }

            if ($breakdown == 'taboola_site_id') {
                array_push($this->breakdowns, 'site_name');
            }
            if ($breakdown == 'taboola_campaign_id') {
                array_push($this->breakdowns, 'campaign_name');
            }
        }
        return $this->breakdowns;
    }
}
