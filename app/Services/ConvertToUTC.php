<?php


namespace App\Services;


use Carbon\Carbon;

class ConvertToUTC
{

    public function getUTCDateTime($dateTime, $currentTimeZone)
    {
       return Carbon::createFromFormat('Y-m-d H:i:s', Carbon::parse($dateTime)->format('Y-m-d H:i:s'), $currentTimeZone)
            ->setTimezone('UTC');
    }
}
