<?php

namespace App\Services;

class ClickhouseMetricService
{
    public function getMetricSQL($formula)
    {
        //parse current formula to array format
        $parsedFormula = $this->parseMetricFormula($formula);
        //convert the parsed formula back to SQL
        $sql = $this->generateSQL($parsedFormula);
        return $sql;
    }

    public function generateSQL($formulaArray)
    {
        $sql = "";
        for ($i = 0; $i < count($formulaArray); $i++) {
            if (is_array($formulaArray[$i])) {
                $sql .= "(" . $this->generateSQL($formulaArray[$i]) . ")";
            } else if ($i <= count($formulaArray) - 2 && is_array($formulaArray[$i + 1])) {
                if (trim($formulaArray[$i]) == "sum" || trim($formulaArray[$i]) == "distinct") {
                    $sql .= "NULLIF(toFloat64(";
                }
                $sql .= $formulaArray[$i] . "(" . $this->generateSQL($formulaArray[$i + 1]) . ")";
                if (trim($formulaArray[$i]) == "sum" || trim($formulaArray[$i]) == "distinct") {
                    $sql .= "), 0)";
                }
                $i++;
            } else {
                $sql .= $formulaArray[$i];
            }
        }
        return $sql;
    }

    public function parseMetricFormula($formula)
    {
        $nestedLevel = 0;
        $levelIndex = 0;
        $lastCapturedIndex = 0;
        $formulaParts = [];
        $hasSubParts = false;
        for ($i = 0; $i < strlen($formula); $i++) {
            if ($formula[$i] == "(") {
                if ($nestedLevel == 0) {
                    $levelIndex = $i;
                }
                $hasSubParts = true;
                $nestedLevel++;
            }

            if ($formula[$i] == ")") {
                $nestedLevel--;
                if ($nestedLevel == 0) {
                    if ($lastCapturedIndex < $levelIndex) {
                        $formulaParts = array_merge($formulaParts, $this->splitFormulaParts(substr($formula, $lastCapturedIndex, $levelIndex - $lastCapturedIndex)));
                        $lastCapturedIndex = $i + 1;
                    }
                    array_push($formulaParts, $this->parseMetricFormula(substr($formula, $levelIndex + 1, $i - $levelIndex - 1)));
                    $lastCapturedIndex = $i + 1;
                }
            }
        }
        if ($lastCapturedIndex <  strlen($formula) - 1) {
            $formulaParts = array_merge($formulaParts, $this->splitFormulaParts(substr($formula, $lastCapturedIndex, strlen($formula) - $lastCapturedIndex)));
            $lastCapturedIndex = strlen($formula) - 1;
        }
        if (!$hasSubParts) {
            return [$formula];
        }
        return $formulaParts;
    }

    public function splitFormulaParts($formulaParts)
    {
        return preg_split('/([\+\-\*\/])/i', trim($formulaParts), -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
    }
}
