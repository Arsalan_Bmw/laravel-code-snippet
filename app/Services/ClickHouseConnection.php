<?php


namespace App\Services;

class ClickHouseConnection
{
    public function connection()
    {
        $server = new Tinderbox\Clickhouse\Server('127.0.0.1', '8123', 'default', 'user', 'pass');
        $serverProvider = (new Tinderbox\Clickhouse\ServerProvider())->addServer($server);

        $client = new Tinderbox\Clickhouse\Client($serverProvider);
        $builder = new Builder($client);
    }
}
