<?php


namespace App\Services;

use App\Enums\TaboolaReportingBreakdownType;
use App\Models\TaboolaCampaignPerformancesIndex;
use App\Services\TaboolaCampaignColumnsSelectorService;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TaboolaCampaignDataQueryService
{
    private $selectColumns = [];
    private $breakdownColumns = [];
    private $spendTable = '';
    private $revenueTable = '';
    private $clickhouseMetricService;
    function __construct()
    {
        $this->clickhouseMetricService = new ClickhouseMetricService();
    }
    public function getData($columns = [], $breakdowns = [], $startDate, $endDate, $page, $perPage, $sortColumn, $sortDir)
    {
        $query = $this->getQueryObject($columns, $breakdowns, $startDate, $endDate);
        $query->select(array_merge($this->breakdownColumns, $this->selectColumns));
        $query->offset(($page - 1) * $perPage)->limit($perPage);
        $query->orderBy($sortColumn, $sortDir);
        Log::info($query->toSql());
        return $query->get();
    }

    public function getCount($columns = [], $breakdowns = [], $startDate, $endDate)
    {
        $query = $this->getQueryObject($columns, $breakdowns, $startDate, $endDate);
        $selectColumns = array_merge($this->breakdownColumns, $this->selectColumns);
        array_push($selectColumns, DB::raw('count(*) as count'));
        $query->select($selectColumns);
        Log::info($query->toSql());
        return $query->get();
    }


    private function getQueryObject($columns, $breakdowns, $startDate, $endDate)
    {
        $this->setReportingTables($breakdowns);
        $this->setSelects($breakdowns, $columns);

        if (in_array(TaboolaReportingBreakdownType::Site, $breakdowns)) {
            $query = $this->getTaboolaCampaignPerSiteQuery();
            $query->leftJoin('taboola_sites', function ($join) {
                $join->on(
                    DB::raw('"taboola_sites"."taboola_site_id"'),
                    '=',
                    DB::raw('COALESCE(NULLIF("' . $this->revenueTable . '"."site_id", 0), NULLIF("' . $this->spendTable . '"."site_id", 0))')
                );
            });
        } else {
            $query = $this->getTaboolaCampaignQuery();
        };

        $query->leftJoin('taboola_campaigns', function ($join) {
            $join->on(
                DB::raw('"taboola_campaigns"."taboola_campaign_id"'),
                '=',
                DB::raw('COALESCE(NULLIF("' . $this->revenueTable . '"."campaign_id", 0), NULLIF("' . $this->spendTable . '"."campaign_id", 0))')
            );
        });
        $query->leftJoin('taboola_advertisers', 'taboola_campaigns.advertiser_id', '=', 'taboola_advertisers.taboola_advertiser_id');

        $query->groupBy(array_map(function ($breakdown) {
            return 'breakdown_' . $breakdown;
        }, $breakdowns));
        $query->whereRaw("(" . $this->spendTable . ".timestamp > parseDateTimeBestEffortOrNull('" . $startDate . "') AND " .
            $this->spendTable . ".timestamp < parseDateTimeBestEffortOrNull('" . $endDate . "')) OR (" .
            $this->revenueTable . ".timestamp > parseDateTimeBestEffortOrNull('" . $startDate . "') AND " .
            $this->revenueTable . ".timestamp < parseDateTimeBestEffortOrNull('" . $endDate . "'))");
        return $query;
    }
    public function getTaboolaCampaignQuery()
    {
        return DB::connection('clickhouse')
            ->table($this->spendTable)
            ->join($this->revenueTable, function ($join) {
                $join->on($this->revenueTable . '.campaign_id', '=', $this->spendTable . '.campaign_id');
                $join->on($this->revenueTable . '.day', '=', $this->spendTable . '.day');
                $join->on($this->revenueTable . '.hour', '=', $this->spendTable . '.hour');
            }, null, null, 'full outer');
    }

    public function getTaboolaCampaignPerSiteQuery()
    {
        return DB::connection('clickhouse')
            ->table($this->spendTable)->join($this->revenueTable, function ($join) {
                $join->on($this->revenueTable . '.campaign_id', '=', $this->spendTable . '.campaign_id');
                $join->on($this->revenueTable . '.day', '=', $this->spendTable . '.day');
                $join->on($this->revenueTable . '.hour', '=', $this->spendTable . '.hour');
                $join->on($this->revenueTable . '.site_id', '=', $this->spendTable . '.site_id');
            }, null, null, 'full outer');
    }

    private function setReportingTables($breakdowns)
    {
        if (in_array(TaboolaReportingBreakdownType::Site, $breakdowns)) {
            $this->spendTable = config('reporting.taboola_campaign_site_spent_table');
            $this->revenueTable = config('reporting.crossroads_campaign_site_revenue_table');
        } else {
            $this->spendTable = config('reporting.taboola_campaign_spent_table');
            $this->revenueTable = config('reporting.crossroads_campaign_revenue_table');
        }
    }

    private function setSelects($breakdowns, $metrics)
    {
        $dataColumnMap = config('reporting.datacolumns');
        $breakdownColumnMap = config('reporting.breakdowncolumns');

        foreach ($breakdowns as $breakdown) {
            array_push($this->selectColumns, DB::raw($this->processColumnField($dataColumnMap[$breakdown]) . ' as ' . $breakdown));
            array_push($this->breakdownColumns, DB::raw($this->processColumnField($breakdownColumnMap[$breakdown]) . ' as breakdown_' . $breakdown));
        }

        foreach ($metrics as $metric) {
            array_push($this->selectColumns, DB::raw($this->processColumnField($dataColumnMap[$metric]) . ' as ' . $metric));
        }
    }

    private function processColumnField($field)
    {
        $field = $this->clickhouseMetricService->getMetricSQL($field);
        $field = str_replace('[revenue]', $this->revenueTable, $field);
        $field = str_replace('[spend]', $this->spendTable, $field);
        return $field;
    }
}
