<?php


namespace App\Services;


use App\Models\FBAccountReport;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FBSpendDataQueryService
{
    public function getData($fbAccountId, $columns = [], $startDate, $endDate)
    {
        $query = DB::table('fbaccounts');
        $metrics = $this->getSelects($columns);

        foreach ($metrics as $metric) {
            switch ($metric['table']) {
                case 'kk_revenue_reports':
                    $query->addSelect([
                        $metric["as"] => DB::table($metric["table"])
                            ->select($metric['column'])
                            ->whereColumn('campaign_id', 'fbaccounts.kk_campaign_id')
                            ->whereRaw('cast(time as signed) > ' .  strtotime($startDate))
                            ->whereRaw('cast(time as signed) < ' .  strtotime($endDate))
                    ]);
                    break;
                case 'fb_account_reports':
                    $query->addSelect([
                        $metric["as"] => DB::table($metric["table"])
                            ->select($metric['column'])
                            ->whereColumn('fb_ad_account_id', 'fbaccounts.ad_account_id')
                            ->whereDate('start_date', '>', $startDate)
                            ->whereDate('end_date', '<=', $endDate)
                    ]);
                    break;
            }
        }
        $query->where('id', $fbAccountId);

        return json_encode($query->first());
    }
    private function getSelects($columns)
    {
        $selectedColumns = [];
        foreach ($columns as $column) {
            switch ($column) {
                case 'cpc':
                    array_push($selectedColumns, [
                        'column' => DB::raw('IFNULL(sum(spend)/sum(inline_link_clicks), 0) as cpc'),
                        'table' => 'fb_account_reports',
                        'as' => 'cpc'
                    ]);
                    break;
                case 'spend':
                    array_push(
                        $selectedColumns,
                        [
                            'column' => DB::raw('IFNULL(sum(spend), 0)'),
                            'table' => 'fb_account_reports',
                            'as' => 'spend'
                        ]
                    );
                    break;
                case 'conversion':
                    array_push($selectedColumns, [
                        'column' =>  DB::raw('IFNULL(sum(conversions), 0)'),
                        'table' => 'fb_account_reports',
                        'as' => 'conversion'
                    ]);
                    break;
                case 'cpr':
                    array_push($selectedColumns, [
                        'column' =>  DB::raw('IFNULL(sum(spend)/sum(conversions), 0)'),
                        'table' => 'fb_account_reports',
                        'as' => 'cpr'
                    ]);
                    break;
                case 'clicks':
                    array_push($selectedColumns, [
                        'column' =>  DB::raw('IFNULL(sum(inline_link_clicks), 0)'),
                        'table' => 'fb_account_reports',
                        'as' => 'clicks'
                    ]);
                    break;
                case 'revenue':
                    array_push($selectedColumns, [
                        'column' =>  DB::raw('IFNULL(sum(price), 0)'),
                        'table' => 'kk_revenue_reports',
                        'as' => 'revenue'
                    ]);
                    break;
            }
        }
        return $selectedColumns;
    }
}
