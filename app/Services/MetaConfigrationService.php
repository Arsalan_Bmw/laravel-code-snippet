<?php


namespace App\Services;


use App\Models\MetaConfigration;
use Carbon\Carbon;

class MetaConfigrationService
{
  public function updateOrCreateMetadata($type){
   $metaStatus = MetaConfigration::updateOrCreate(
        ['type'=>  $type],
        ['updated_at'=>  Carbon::parse()->format('Y-m-d H:i:s')]
      );
    return $metaStatus ? true : false ;  
  }
 
}