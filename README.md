## About MMA

This is backend stack for MMA frontend, its built on Laravel framework.

## Installation

MMA Backend ships with docker infrastructure, it can be setup on any environment with following commands,

- Install Docker and Docker compose
- Copy .env.example to .env file and update db credentails
- run `docker-compose up -d`
- run `docker-compose run --rm composer install` to install composer dependencies
- run `docker-compose run --rm artisan key:generate` to generate a new application key
- run `docker-compose run --rm artisan migrate` to run migrations
- run `docker-compose run --rm artisan db:seed` to seed initial data
- run `docker-compose run --rm artisan passport:install` to setup passport

open a browser and point to `http://localhost/` to access the application

## Contribution
- Create a branch based on JIRA id, for example, `MMA-1-some-feature`
- Create a pull request once the feature is ready to be merged to main branch
- Squash and merge using --ff-only flag
