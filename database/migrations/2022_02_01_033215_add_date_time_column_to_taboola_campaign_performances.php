<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDateTimeColumnToTaboolaCampaignPerformances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('taboola_campaign_performances', function (Blueprint $table) {
            $table->dateTime('report_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('taboola_campaign_performances', function (Blueprint $table) {
            $table->dropColumn('report_at');
        });
    }
}
