<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexesToFbaccuntsTableColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fbaccounts', function($table) {
            $table->bigInteger('kk_campaign_id')->index()->change();
            $table->string('ad_account_id')->index()->change();
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fbaccounts', function (Blueprint $table) {
            $table->dropIndex('fbaccounts_kk_campaign_id_index');
            $table->dropIndex('fbaccounts_ad_account_id_index');
        });
    }
}
