<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterFbAccountsAddKKstatusAdUrl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fbaccounts', function (Blueprint $table) {
            $table->string('active_ad_url')->nullable()->after('ad_account_id');
            $table->string('kk_campaign_status')->default("Block All")->after('added_to_kk');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fbaccounts', function (Blueprint $table) {
            $table->dropColumn('active_ad_url');
            $table->dropColumn('kk_campaign_status');
        });
    }
}
