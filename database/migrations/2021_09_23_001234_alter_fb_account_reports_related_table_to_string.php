<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterFbAccountReportsRelatedTableToString extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fb_ad_sets', function (Blueprint $table) {
            $table->string('ad_set_name')->change();
        });
        Schema::table('fb_campaigns', function (Blueprint $table) {
            $table->string('campaign_name')->change();
        });
        Schema::table('fb_ads', function (Blueprint $table) {
            $table->string('fb_name')->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fb_ad_sets', function (Blueprint $table) {
            $table->bigInteger('ad_set_name')->change();
        });
        Schema::table('fb_campaigns', function (Blueprint $table) {
            $table->bigInteger('campaign_name')->change();
        });
        Schema::table('fb_ads', function (Blueprint $table) {
            $table->bigInteger('fb_name')->change();
        });
    }
}
