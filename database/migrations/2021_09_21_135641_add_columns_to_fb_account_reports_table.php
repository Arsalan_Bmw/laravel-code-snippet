<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToFbAccountReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fb_account_reports', function (Blueprint $table) {
            $table->bigInteger('unique_clicks')->default(0);
            $table->bigInteger('outbound_clicks')->default(0);
            $table->bigInteger('inline_link_clicks')->default(0);
            $table->decimal('conversions',64, 6)->default(0);
            $table->integer('impressions')->default(0);
            $table->integer('reach')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fb_account_reports', function (Blueprint $table) {
            $table->dropColumn(['unique_clicks','outbound_clicks','inline_link_clicks','conversions','impressions','reach']);
        });
    }
}
