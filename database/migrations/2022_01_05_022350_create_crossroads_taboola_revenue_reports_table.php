<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrossroadsTaboolaRevenueReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crossroads_taboola_revenue_reports', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('campaign_id');
            $table->bigInteger('site_id');
            $table->string('publisher');
            $table->string('ad_id');
            $table->bigInteger('click_id');
            $table->bigInteger('campaign_item_id');
            $table->date('day');
            $table->string('gclid');
            $table->string('platform');
            $table->time('hour');
            $table->bigInteger('revenue_clicks');
            $table->bigInteger('total_visitors');
            $table->bigInteger('tracked_visitors');
            $table->decimal('publisher_revenue_amount',65,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crossroads_taboola_revenue_reports');
    }
}
