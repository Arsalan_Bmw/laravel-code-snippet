<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameAccessPointPasswordInFbAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fbaccounts', function (Blueprint $table) {
            //
            $table->renameColumn('access_point_password', 'accesspoint_password');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fbaccounts', function (Blueprint $table) {
            //
            $table->renameColumn('accesspoint_password', 'access_point_password');
        });
    }
}
