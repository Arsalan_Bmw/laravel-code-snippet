<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaboolaCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taboola_campaigns', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('taboola_campaign_id');
            $table->string('campaign_name');
            $table->bigInteger('media_buyer_id')->nullable();
            $table->bigInteger('advertiser_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taboola_campaigns');
    }
}
