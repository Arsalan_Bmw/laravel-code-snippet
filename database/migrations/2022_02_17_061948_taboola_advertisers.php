<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TaboolaAdvertisers extends PhpClickHouseLaravel\Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        static::write("
        CREATE TABLE `taboola_advertisers`  (
            `taboola_advertiser_id` UInt64,
            `advertiser_name` String,
            `is_active` UInt64,
            `default_platform` String,
            `type` String,
            `currency` String,
            `time_zone_name` String,
            `created_at` Nullable(DateTime),
            `updated_at` Nullable(DateTime)
        )
        ENGINE = MergeTree()
        ORDER BY (taboola_advertiser_id)");
    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        static::write("DROP TABLE taboola_advertisers");
    }
}
