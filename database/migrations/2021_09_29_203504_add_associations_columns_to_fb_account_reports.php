<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAssociationsColumnsToFbAccountReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fb_account_reports', function (Blueprint $table) {
            $table->bigInteger('media_buyer_id')->after('fb_id')->nullable();
            $table->bigInteger('offer_id')->after('fb_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fb_account_reports', function (Blueprint $table) {
            $table->dropColumn('media_buyer_id');
            $table->dropColumn('offer_id');
        });
    }
}
