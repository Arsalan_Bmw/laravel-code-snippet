<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterFbaccountColumnOffer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fbaccounts', function (Blueprint $table) {
            $table->renameColumn('offer', 'vertical');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fbaccounts', function (Blueprint $table) {
            $table->renameColumn('vertical', 'offer');
        });
    }
}
