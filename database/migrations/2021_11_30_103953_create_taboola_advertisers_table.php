<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaboolaAdvertisersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taboola_advertisers', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('taboola_advertiser_id');
            $table->string('advertiser_name');
            $table->tinyInteger('is_active');
            $table->string('default_platform');
            $table->string('type');
            $table->string('currency');
            $table->string('time_zone_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taboola_advertisers');
    }
}
