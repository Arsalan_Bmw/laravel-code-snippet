<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TaboolaCampaignPerformances extends PhpClickHouseLaravel\Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        static::write("
        CREATE TABLE `taboola_campaign_performances`  (
            `campaign_id` UInt64,
            `clicks` UInt64,
            `impressions` UInt64,
            `visible_impressions` UInt64,
            `spent` decimal(65, 2),
            `cpa_actions_num` UInt64,
            `currency` String,
            `timestamp` Nullable(DateTime),
            `conversions_value` decimal(65, 2),
            `cpa_conversion_rate` decimal(65, 2),
            `day` Nullable(Date),
            `hour` Nullable(String),
            `api_start_date` Nullable(Date),
            `api_end_date` Nullable(Date),
            `created_at` Nullable(DateTime),
            `updated_at` Nullable(DateTime)
        )
        ENGINE = MergeTree()
        ORDER BY (campaign_id)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        static::write("DROP TABLE taboola_campaign_performances");
    }
}
