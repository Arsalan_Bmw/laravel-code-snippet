<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterFbAccountReportsColumnToBigInt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fb_account_reports', function (Blueprint $table) {
            $table->bigInteger('ad_set_id')->change();
            $table->bigInteger('campaign_id')->change();
            $table->bigInteger('fb_id')->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fb_account_reports', function (Blueprint $table) {
            $table->unsignedBigInteger('ad_set_id')->change();
            $table->unsignedBigInteger('campaign_id')->change();
            $table->unsignedBigInteger('fb_id')->change();

        });
    }
}
