<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropForeignKeyConstraintCrossroadsFacebookRevenueReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crossroads_facebook_revenue_reports', function (Blueprint $table) {
            $table->dropForeign(['campaign_id']);
            $table->dropForeign(['account_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crossroads_facebook_revenue_reports', function (Blueprint $table) {
            $table->foreign('campaign_id')->references('kk_campaign_id')->on('fbaccounts');
            $table->foreign('account_id')->references('ad_account_id')->on('fbaccounts');
        });
    }
}
