<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FbAccountsReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fb_account_reports', function (Blueprint $table) {
            $table->id();
            $table->foreignId('fb_ad_account_id')->constrained('fbaccounts');
            $table->bigInteger('clicks');
            $table->decimal('cpm', 64, 6);
            $table->decimal('ctr', 64, 6);
            $table->decimal('frequency', 64, 6);
            $table->decimal('spend', 64, 2);
            $table->timestamp('start_date');
            $table->timestamp('end_date');
            $table->bigInteger('campaign_id');
            $table->bigInteger('ad_set_id');
            $table->bigInteger('fb_id');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('fb_account_reports');
    }
}
