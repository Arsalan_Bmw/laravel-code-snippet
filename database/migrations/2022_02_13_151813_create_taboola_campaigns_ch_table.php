<?php
class CreateTaboolaCampaignsChTable extends PhpClickHouseLaravel\Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        static::write("
        CREATE TABLE `taboola_campaigns`  (
           
            `taboola_campaign_id` UInt64,
            `campaign_name` String,
            `media_buyer_id` Nullable(UInt64),
            `advertiser_id` UInt64,
            `created_at` Nullable(DateTime),
            `updated_at` Nullable(DateTime)
        )
        ENGINE = MergeTree()
        ORDER BY (taboola_campaign_id)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        static::write("DROP TABLE taboola_campaigns");
    }
}
