<?php

class CreateCrossroadsAggregatedCampaignPerformancesTable extends PhpClickHouseLaravel\Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        static::write("
        CREATE TABLE `crossroads_aggregated_campaign_performances`  (
            `campaign_id` UInt64,
            `day` Nullable(Date),
            `hour` Nullable(String),
            `lander_searches` UInt64,
            `lander_visitors` UInt64,
            `publisher_revenue_amount` UInt64,
            `revenue_clicks`  UInt64,
            `total_visitors` UInt64,
            `tracked_visitors` UInt64,
            `timestamp` Nullable(DateTime),
            `created_at` Nullable(DateTime),
            `updated_at` Nullable(DateTime)
        )
        ENGINE = MergeTree()
        ORDER BY (campaign_id)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        static::write("DROP TABLE crossroads_aggregated_campaign_performances");
    }
}
