<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrossroadsRevenueReports extends PhpClickHouseLaravel\Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        static::write("
        CREATE TABLE `crossroads_revenue_reports`  (
            `publisher` String,
            `day` Date,
            `gclid` String,
            `platform` String,
            `hour` String,
            `revenue_clicks` UInt64,
            `total_visitors` UInt64,
            `tracked_visitors` UInt64,
            `publisher_revenue_amount` decimal(65, 2),
            `referrer` String,
            `user_agent` String,
            `category` String,
            `city` String,
            `country_code` String,
            `device_type` String,
            `keyword` String,
            `lander_keyword` String,
            `revenue_keyword` String,
            `search_term` String,
            `state` String,
            `tg1` String,
            `tg5` String,
            `tg4` String,
            `lander_searches` UInt64,
            `lander_visitors` UInt64,
            `tg10` String,
            `tg7` String,
            `tg8` String,
            `tg9` String,
            `traffic_source` String,
            `campaign_name` String,
            `campaign_type` String,
            `browser` String,
            `tg6` String,
            `account_id` String,
            `tg2` String,
            `ad_id` String,
            `campaign_number` String,
            `tg3` String,
            `query_date` Date,
            `match_type` String,
            `api_start_date` Nullable(Date),
            `api_end_date` Nullable(Date),
            `created_at` Nullable(DateTime),
            `updated_at` Nullable(DateTime)
        )
        ENGINE = MergeTree()
        ORDER BY (campaign_number)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        static::write("DROP TABLE crossroads_revenue_reports");
    }
}
