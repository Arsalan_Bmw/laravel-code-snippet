<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCampaginIdOnKkRevenueReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kk_revenue_reports', function (Blueprint $table) {
            $table->string('campaign_id')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kk_revenue_reports', function (Blueprint $table) {
            $table->bigInteger('campaign_id')->change();
        });
    }
}
