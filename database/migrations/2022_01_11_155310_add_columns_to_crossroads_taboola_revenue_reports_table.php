<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToCrossroadsTaboolaRevenueReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crossroads_taboola_revenue_reports', function (Blueprint $table) {
            $table->text('referrer')->nullable();
            $table->text('user_agent')->nullable();
            $table->string('category')->nullable();
            $table->string('city')->nullable();
            $table->string('country_code')->nullable();
            $table->string('device_type')->nullable();
            $table->string('keyword')->nullable();
            $table->string('lander_keyword')->nullable();
            $table->string('revenue_keyword')->nullable();
            $table->string('search_term')->nullable();
            $table->string('state')->nullable();
            $table->string('tg1')->nullable();
            $table->string('ts_account_id')->nullable();
            $table->string('ts_click_id')->nullable();
            $table->integer('lander_searches')->nullable();
            $table->integer('lander_visitors')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crossroads_taboola_revenue_reports', function (Blueprint $table) {
            $table->dropColumn('referrer');
            $table->dropColumn('user_agent');
            $table->dropColumn('category');
            $table->dropColumn('city');
            $table->dropColumn('country_code');
            $table->dropColumn('device_type');
            $table->dropColumn('keyword');
            $table->dropColumn('lander_keyword');
            $table->dropColumn('revenue_keyword');
            $table->dropColumn('search_term');
            $table->dropColumn('state');
            $table->dropColumn('tg1');
            $table->dropColumn('ts_account_id');
            $table->dropColumn('ts_click_id');
            $table->dropColumn('lander_searches');
            $table->dropColumn('lander_visitors');
        });
    }
}
