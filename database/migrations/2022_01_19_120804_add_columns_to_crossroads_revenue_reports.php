<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToCrossroadsRevenueReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crossroads_revenue_reports', function (Blueprint $table) {
            $table->string('tg2')->nullable();
            $table->text('ad_id')->nullable();
            $table->text('campaign_number')->nullable();
            $table->renameColumn('ts_click_id', 'tg4');
            $table->renameColumn('ts_account_id', 'tg5');
            $table->string('tg3')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crossroads_revenue_reports', function (Blueprint $table) {
            $table->dropColumn('tg2');
            $table->dropColumn('tg3');
            $table->dropColumn('ad_id');
            $table->renameColumn('tg4','ts_click_id');
            $table->renameColumn('tg5','ts_account_id');
            $table->dropColumn('campaign_number');
        });
    }
}
