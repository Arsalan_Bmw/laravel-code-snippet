<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropMappedColumnsCrossroadsRevenueReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crossroads_revenue_reports', function (Blueprint $table) {
            $table->string('tg6')->nullable();
            $table->string('account_id')->nullable();
          
            $table->dropColumn('site_id');
            $table->dropColumn('ad_id');
            $table->dropColumn('click_id');
            $table->dropColumn('campaign_item_id');
            $table->dropColumn('campaign_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crossroads_revenue_reports', function (Blueprint $table) {
            $table->dropColumn('tg6');
            $table->dropColumn('account_id');
          
            $table->bigInteger('site_id');
            $table->bigInteger('ad_id');
            $table->bigInteger('click_id');
            $table->bigInteger('campaign_item_id');
            $table->bigInteger('campaign_id');
        });
    }
}
