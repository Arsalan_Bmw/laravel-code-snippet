<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaboolaCampaignPerformancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('taboola_campaign_performances', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('campaign_id');
            $table->bigInteger('clicks');
            $table->bigInteger('impressions');
            $table->bigInteger('visible_impressions');
            $table->decimal('spent',65,2);
            $table->bigInteger('cpa_actions_num');
            $table->string('currency');
            $table->dateTime('timestamp');
            $table->decimal('conversions_value',65,2);
            $table->decimal('cpa_conversion_rate',65,18);
            $table->date('day');
            $table->time('hour');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taboola_campaign_performances');
    }
}
