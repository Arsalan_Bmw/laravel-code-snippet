<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameDateOfLastAction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fbaccounts', function (Blueprint $table) {
            $table->renameColumn('date_of_last_action', 'comment_management_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fbaccounts', function (Blueprint $table) {
            $table->renameColumn('comment_management_date', 'date_of_last_action');
        });
    }
}
