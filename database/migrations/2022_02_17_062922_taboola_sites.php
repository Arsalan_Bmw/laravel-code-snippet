<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TaboolaSites extends PhpClickHouseLaravel\Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        static::write("
        CREATE TABLE `taboola_sites`  (
            `taboola_site_id` UInt64,
            `site_name` String,
            `created_at` Nullable(DateTime),
            `updated_at` Nullable(DateTime)
        )
        ENGINE = MergeTree()
        ORDER BY (taboola_site_id)");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      
        static::write("DROP TABLE taboola_sites");
    }
}
