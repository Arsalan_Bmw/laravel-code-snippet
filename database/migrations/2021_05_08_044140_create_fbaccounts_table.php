<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFbaccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fbaccounts', function (Blueprint $table) {
            $table->id();
            $table->integer('account_id')->unsigned();
            $table->string('build_name')->nullable();
            $table->longText('next_step')->nullable();
            $table->string('status')->nullable();
            $table->string('priority')->nullable();
            $table->string('quick_note')->nullable();
            $table->longText('accesspoint_notes')->nullable();
            $table->string('source')->nullable();
            $table->date('date_of_last_action')->nullable();;
            $table->date('date_of_next_action')->nullable();;
            $table->string('geo')->nullable();;
            $table->string('timezone')->nullable();;
            $table->string('fb_user_full_name')->nullable();;
            $table->string('fb_user_login_info')->nullable();;
            $table->string('twofa_secret')->nullable();;
            $table->string('fb_campaign_name')->nullable();;
            $table->string('type_of_ad_account')->nullable();
            $table->string('ad_account_name')->nullable();;
            $table->string('ad_account_id')->nullable();;
            $table->string('business_manager_name')->nullable();;
            $table->string('business_manager_id')->nullable();;
            $table->string('business_verified')->nullable();
            $table->unsignedInteger('starting_max_spend_of_account')->nullable();;
            $table->string('domain')->nullable();;
            $table->string('kk_master_server_name')->nullable();;
            $table->boolean('website_cleaned')->default(0);
            $table->unsignedInteger('cost_of_ad_account')->nullable();;
            $table->string('page_name')->nullable();;
            $table->string('page_url')->nullable();;
            $table->string('page_id')->nullable();;
            $table->boolean('post_scheduled')->default(0);
            $table->date('last_day_post_scheduled')->nullable();
            $table->boolean('cc_added')->default(0);
            $table->boolean('added_to_buyer_tool')->default(0);
            $table->boolean('added_to_adcost')->default(0);
            $table->boolean('engagement_lifetime_campaign')->default(0);
            $table->date('engagement_lifetime_campaign_end_date')->nullable();
            $table->boolean('like_lifetime_campaign')->default(0);
            $table->date('like_lifetime_campaign_end_date')->nullable();
            $table->boolean('daily_likes_camp')->default(0);
            $table->boolean('domain_verified_in_fb')->default(0);
            $table->string('pixel_id')->nullable();;
            $table->string('offer')->nullable();
            $table->string('media_buyer')->nullable();
            $table->boolean('added_to_kk')->default(0);
            $table->string('kk_account_name')->nullable();
            $table->string('kk_campaign_title')->nullable();
            $table->string('kk_campaign_url')->nullable();
            $table->string('url_tracking_string')->nullable();
            $table->boolean('passed_to_media_buyer')->default(0);
            $table->boolean('lpv_camp')->default(0);
            $table->boolean('wh_camp')->default(0);
            $table->boolean('bh_camp')->default(0);
            $table->longText('media_buyer_passing_template')->nullable();
            $table->longText('long_notes')->nullable();
            $table->boolean('terminated_card')->default(0);
            $table->boolean('remove_from_kk')->default(0);
            $table->boolean('archive')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('fbaccounts');
    }
}
