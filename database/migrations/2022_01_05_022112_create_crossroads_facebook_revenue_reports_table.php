<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrossroadsFacebookRevenueReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::create('crossroads_facebook_revenue_reports', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('campaign_id');
            $table->bigInteger('ad_id')->nullable();
            $table->string('account_id')->nullable();
            $table->bigInteger('click_id')->nullable();
            $table->bigInteger('revenue_clicks')->nullable();
            $table->bigInteger('total_visitors')->nullable();
            $table->bigInteger('tracked_visitors')->nullable();
            $table->date('day');
            $table->string('gclid')->nullable();
            $table->string('platform')->nullable();
            $table->time('hour');
            $table->decimal('publisher_revenue_amount',65,2)->nullable();
            $table->timestamps();
        });
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crossroads_facebook_revenue_reports');
    }
}
