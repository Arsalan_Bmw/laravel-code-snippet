<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsTableCrossroadsRevenueReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crossroads_revenue_reports', function (Blueprint $table) {
            $table->string('tg10')->nullable();
            $table->string('tg7')->nullable();
            $table->string('tg8')->nullable();
            $table->string('tg9')->nullable();
            $table->string('traffic_source')->nullable();
            $table->string('campaign_name')->nullable();
            $table->string('campaign_type')->nullable();
            $table->string('browser')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crossroads_revenue_reports', function (Blueprint $table) {
            $table->dropColumn('tg10');
            $table->dropColumn('tg7');
            $table->dropColumn('tg8');
            $table->dropColumn('tg9');
            $table->dropColumn('traffic_source');
            $table->dropColumn('campaign_name');
            $table->dropColumn('campaign_type');
            $table->dropColumn('browser');
        });
        
    }
}
