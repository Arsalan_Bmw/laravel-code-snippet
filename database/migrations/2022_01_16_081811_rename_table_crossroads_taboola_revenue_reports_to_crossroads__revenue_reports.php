<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameTableCrossroadsTaboolaRevenueReportsToCrossroadsRevenueReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('crossroads_taboola_revenue_reports', 'crossroads_revenue_reports');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('crossroads_revenue_reports','crossroads_taboola_revenue_reports');
    }
}
