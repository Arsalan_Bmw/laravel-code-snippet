<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use \Illuminate\Support\Facades\DB;
class DropFbAdAccountIdForeignkeyConstraint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::table('fb_ad_sets', function (Blueprint $table) {
            $table->dropForeign('fb_ad_sets_ad_set_id_foreign');
            $table->dropIndex('fb_ad_sets_ad_set_id_foreign');
        });
        Schema::table('fb_ads', function (Blueprint $table) {
            $table->dropForeign('fb_ads_fb_id_foreign');
            $table->dropIndex('fb_ads_fb_id_foreign');
        });
        Schema::table('fb_campaigns', function (Blueprint $table) {
            $table->dropForeign('fb_campaigns_campaign_id_foreign');
            $table->dropIndex('fb_campaigns_campaign_id_foreign');
        });
        Schema::table('fb_ad_accounts', function (Blueprint $table) {
            $table->dropForeign('fb_ad_accounts_ad_account_id_foreign');
            $table->dropIndex('fb_ad_accounts_ad_account_id_foreign');
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Schema::table('fb_ad_sets', function (Blueprint $table) {
            $table->foreign('ad_set_id')->references('id')->on('fb_ad_sets');
        });
        Schema::table('fb_ads', function (Blueprint $table) {
            $table->foreign('fb_id')->references('id')->on('fb_ads');
        });
        Schema::table('fb_campaigns', function (Blueprint $table) {
            $table->foreign('campaign_id')->references('id')->on('fb_campaigns');
        });
        Schema::table('fb_ad_accounts', function (Blueprint $table) {
            $table->foreign('ad_account_id')->references('id')->on('fb_ad_accounts');
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
