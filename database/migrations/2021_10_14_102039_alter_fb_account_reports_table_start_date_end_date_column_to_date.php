<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AlterFbAccountReportsTableStartDateEndDateColumnToDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fb_account_reports', function (Blueprint $table) {
            $table->date('start_date')->default('0000-00-00')->change();
            $table->date('end_date')->default('0000-00-00')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       DB::statement('Alter  table fb_account_reports MODIFY COLUMN start_date TIMESTAMP NOT NULL');
       DB::statement('Alter  table fb_account_reports MODIFY COLUMN end_date TIMESTAMP NOT NULL');
    }
}
