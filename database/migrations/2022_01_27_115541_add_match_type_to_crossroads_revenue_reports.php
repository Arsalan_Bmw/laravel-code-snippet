<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMatchTypeToCrossroadsRevenueReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crossroads_revenue_reports', function (Blueprint $table) {
            $table->string('match_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crossroads_revenue_reports', function (Blueprint $table) {
            $table->dropColumn('match_type');
        });
    }
}
