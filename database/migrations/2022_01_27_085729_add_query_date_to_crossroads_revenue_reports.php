<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddQueryDateToCrossroadsRevenueReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('crossroads_revenue_reports', function (Blueprint $table) {
            $table->date('query_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('crossroads_revenue_reports', function (Blueprint $table) {
            $table->dropColumn('query_date');
        });
    }
}
