<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UpdateLastLogTimeOnFbaccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fbaccounts', function (Blueprint $table) {
            $table->dateTime('last_log_time')->nullable()->change();
        });
        DB::statement("update fbaccounts set `last_log_time` = " .
            "(select created_at from fbaccounts_log where " .
            "fbaccounts_log.`account_id` = fbaccounts.`account_id`" .
            " order by `fbaccounts_log`.`created_at` desc limit 1);");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fbaccounts', function (Blueprint $table) {
            $table->dateTime('last_log_time')->nullable(false)->change();
        });
    }
}
