<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

use Ranium\SeedOnce\Traits\SeedOnce;

class UpdateUserStatusToZero extends Seeder
{
    use SeedOnce;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::where('archive', null)->update(
            [
                'archive' => 0
            ]
        );
    }
}
