<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

use Ranium\SeedOnce\Traits\SeedOnce;
class AdminUserSeeder extends Seeder
{
    use SeedOnce;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(DB::table('users')->get()->count() == 0)
        {
            /**
        	 * Set the user credential
        	 *
        	 */
            $credentials = [
                'email'    => 'superadmin@admin.com',
                'password' => Hash::make('admin'),
                'first_name' => 'admin',
                'last_name' => 'admin',
                'archive' => 0
            ];

            /**
             * Register and Activate user and inserting all data into database
             *
             */
            $user = User::create($credentials);


            $role = Role::where(["name" => "superadmin"])->get();
            $user->assignRole($role);
        }
    }
}
