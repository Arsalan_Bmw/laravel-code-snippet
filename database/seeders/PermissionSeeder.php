<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Ranium\SeedOnce\Traits\SeedOnce;
class PermissionSeeder extends Seeder
{
    use SeedOnce;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Permission::truncate();
        $permission = Permission::insert(
            [
                ['name' => 'view fbAccount', 'guard_name' => 'api'],
                ['name' => 'view user', 'guard_name' => 'api'],
                ['name' => 'view vertical', 'guard_name' => 'api'],
                ['name' => 'view source', 'guard_name' => 'api'],
                ['name' => 'view taboola', 'guard_name' => 'api']
            ]

        );
        Schema::enableForeignKeyConstraints();
    }
}
