<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class AssignPermissionsToRoles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Schema::disableForeignKeyConstraints();
      DB::table('role_has_permissions')->truncate();
      $supperAdminPermissions  =  [ 'view user','view vertical','view source','view fbAccount','view taboola'];
      $superAdminrole = Role::where('name', 'superadmin')->first();
      $superAdminrole->givePermissionTo($supperAdminPermissions);
      $otherPermissions = ['view fbAccount','view taboola'];
      $adminRole  =  Role::where('name','admin')->first();
      $adminRole->givePermissionTo($otherPermissions);
      $userRole = Role::where('name','user')->first();
      $userRole->givePermissionTo($otherPermissions);
      Schema::enableForeignKeyConstraints();


    }
}
