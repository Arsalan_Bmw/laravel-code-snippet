<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Ranium\SeedOnce\Traits\SeedOnce;
class DatabaseSeeder extends Seeder
{
    use SeedOnce;
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(UsersRolesSeeder::class);
    	$this->call(AdminUserSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(AssignPermissionsToRoles::class);
    }
}
