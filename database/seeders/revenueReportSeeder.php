<?php

namespace Database\Seeders;

use App\Models\TaboolaCampaigns;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class revenueReportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::connection('clickhouse')->statement("TRUNCATE TABLE taboola_sites ");
        DB::connection('clickhouse')->statement("TRUNCATE TABLE taboola_campaigns ");
        DB::connection('clickhouse')->statement("TRUNCATE TABLE crossroads_revenue_reports ");
        DB::connection('clickhouse')->statement("TRUNCATE TABLE taboola_advertisers ");
        DB::connection('clickhouse')->statement("TRUNCATE TABLE taboola_campaign_sites_performances ");
        DB::connection('clickhouse')->statement("TRUNCATE TABLE taboola_campaign_performances ");
        DB::connection('clickhouse')->statement("TRUNCATE TABLE crossroads_revenue_report_groupeds ");
        DB::connection('clickhouse')->statement("TRUNCATE TABLE crossroads_revenue_report_site_groupeds ");
        DB::connection('clickhouse')->table('taboola_sites')->insert([
            [
                'taboola_site_id' => 1,
                'site_name' => 'site a'
            ]
        ]);
        TaboolaCampaigns::make([
            [
                'taboola_campaign_id' => 1,
                'campaign_name' => 'campaign a',
                'advertiser_id' => 1
            ]
        ]);
        DB::connection('clickhouse')->table('taboola_advertisers')->insert([
            [
                'taboola_advertiser_id' => 1,
                'advertiser_name' => 'advertiserA',
                'is_active' => 1,
                'default_platform' => 'TABOOLA_ADS',
                'type' => 'pattern',
                'currency' => 'usd',
                'time_zone_name' => 'US/Pacific'
            ]
        ]);
        DB::connection('clickhouse')->table('taboola_campaign_performances')->insert([
            [
                'campaign_id' => 1,
                'clicks' => 1,
                'impressions' => 1,
                'visible_impressions' => 1,
                'spent' => 5,
                'cpa_actions_num' => 2,
                'currency' => 'USD',
                'timestamp' => '2022-02-20 11:00:00',
                'conversions_value' => 1,
                'cpa_conversion_rate' => 1,
                'day' => '2022-02-20',
                'hour' => '11:00:00',
                'report_at' => '2020-02-20'


            ],
            [
                'campaign_id' => 1,
                'clicks' => 1,
                'impressions' => 1,
                'visible_impressions' => 1,
                'spent' => 5,
                'cpa_actions_num' => 2,
                'currency' => 'USD',
                'timestamp' => '2022-02-20 11:00:00',
                'conversions_value' => 1,
                'cpa_conversion_rate' => 1,
                'day' => '2022-02-20',
                'hour' => '12:00:00',
                'report_at' => '2020-02-20'


            ],
            [
                'campaign_id' => 1,
                'clicks' => 1,
                'impressions' => 1,
                'visible_impressions' => 1,
                'spent' => 5,
                'cpa_actions_num' => 2,
                'currency' => 'USD',
                'timestamp' => '2022-02-20 11:00:00',
                'conversions_value' => 1,
                'cpa_conversion_rate' => 1,
                'day' => '2022-02-20',
                'hour' => '13:00:00',
                'report_at' => '2020-02-20'


            ]
        ]);
        DB::connection('clickhouse')->table('taboola_campaign_sites_performances')->insert([
            [
                'campaign_id' => 1,
                'site_id' => 1,
                'clicks' => 1,
                'impressions' => 1,
                'visible_impressions' => 2,
                'spent' => 5,
                'cpa_actions_num' => 2,
                'currency' => 'USD',
                'timestamp' => '2022-02-20 11:00:00',
                'conversions_value' => 1,
                'cpa_conversion_rate' => 1,
                'day' => '2022-02-20',
                'hour' => '11:00:00',
                'report_at' => '2020-02-20'
            ],
            [
                'campaign_id' => 1,
                'site_id' => 1,
                'clicks' => 1,
                'impressions' => 1,
                'visible_impressions' => 2,
                'spent' => 5,
                'cpa_actions_num' => 2,
                'currency' => 'USD',
                'timestamp' => '2022-02-20 11:00:00',
                'conversions_value' => 1,
                'cpa_conversion_rate' => 1,
                'day' => '2022-02-20',
                'hour' => '12:00:00',
                'report_at' => '2020-02-20'
            ],
            [
                'campaign_id' => 1,
                'site_id' => 1,
                'clicks' => 1,
                'impressions' => 1,
                'visible_impressions' => 2,
                'spent' => 5,
                'cpa_actions_num' => 2,
                'currency' => 'USD',
                'timestamp' => '2022-02-20 11:00:00',
                'conversions_value' => 1,
                'cpa_conversion_rate' => 1,
                'day' => '2022-02-20',
                'hour' => '13:00:00',
                'report_at' => '2020-02-20'
            ]
        ]);
        DB::connection('clickhouse')->table('crossroads_revenue_reports')->insert([
            [
                'publisher' => 'cnbc',
                'day' => '2020-02-20',
                'gclid' => '',
                'platform' => 'ios',
                'hour' => '11:00:00',
                'revenue_clicks' => 1,
                'total_visitors' => 2,
                'tracked_visitors' => 1,
                'publisher_revenue_amount' => 5,
                'referrer' => 'https://trc.taboola.com/',
                'user_agent' => 'Mozilla/5.0 (iPhone; CPU iPhone OS 15_1_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko',
                'category' => '',
                'city' => '',
                'country_code' => '',
                'device_type' => '',
                'keyword' => '',
                'lander_keyword' => '',
                'revenue_keyword' => '',
                'search_term' => '',
                'state' => '',
                'tg1' => '',
                'tg4' => 1425292,
                'tg5' => 'GiC38AfpN6YSxW-c4sZRd94MJ9zwCS6HfRbBLJrW-YZ6sSCM_1Yo4fWMqNSMxNGIAQ',
                'lander_searches' => 1,
                'lander_visitors' => 2,
                'tg10' => '',
                'tg7' => 1385079,
                'tg8' => '004874e386e9a658efbf32506b82466817',
                'tg9' => '0.066',
                'traffic_source' => 'Taboola',
                'campaign_name' => 'campaign a',
                'campaign_type' => 'Native',

                'browser' => '',
                'tg6' => '16183236_1199109_endtimeheadlines-endtimeheadlines_3138426628',
                'account_id' => 64154,
                'tg2' => 428353217,
                'ad_id' => 'Walk-in Bathtubs May Be the Newest Trend in Modern Bathrooms',
                'campaign_number' => 1,
                'tg3' => 3138426628,
                'query_date' => '2021-01-01',
                'match_type' => 1,

            ],
            [
                'publisher' => 'cnbc',
                'day' => '2020-02-20',
                'gclid' => '',
                'platform' => 'ios',
                'hour' => '12:00:00',
                'revenue_clicks' => 1,
                'total_visitors' => 2,
                'tracked_visitors' => 1,
                'publisher_revenue_amount' => 5,
                'referrer' => 'https://trc.taboola.com/',
                'user_agent' => 'Mozilla/5.0 (iPhone; CPU iPhone OS 15_1_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko',
                'category' => '',
                'city' => '',
                'country_code' => '',
                'device_type' => '',
                'keyword' => '',
                'lander_keyword' => '',
                'revenue_keyword' => '',
                'search_term' => '',
                'state' => '',
                'tg1' => '',
                'tg4' => 1425292,
                'tg5' => 'GiC38AfpN6YSxW-c4sZRd94MJ9zwCS6HfRbBLJrW-YZ6sSCM_1Yo4fWMqNSMxNGIAQ',
                'lander_searches' => 1,
                'lander_visitors' => 2,
                'tg10' => '',
                'tg7' => 1385079,
                'tg8' => '004874e386e9a658efbf32506b82466817',
                'tg9' => '0.066',
                'traffic_source' => 'Taboola',
                'campaign_name' => 'campaign a',
                'campaign_type' => 'Native',

                'browser' => '',
                'tg6' => '16183236_1199109_endtimeheadlines-endtimeheadlines_3138426628',
                'account_id' => 64154,
                'tg2' => 428353217,
                'ad_id' => 'Walk-in Bathtubs May Be the Newest Trend in Modern Bathrooms',
                'campaign_number' => 1,
                'tg3' => 3138426628,
                'query_date' => '2021-01-01',
                'match_type' => 1,

            ],
            [
                'publisher' => 'cnbc',
                'day' => '2020-02-20',
                'gclid' => '',
                'platform' => 'ios',
                'hour' => '13:00:00',
                'revenue_clicks' => 1,
                'total_visitors' => 2,
                'tracked_visitors' => 1,
                'publisher_revenue_amount' => 5,
                'referrer' => 'https://trc.taboola.com/',
                'user_agent' => 'Mozilla/5.0 (iPhone; CPU iPhone OS 15_1_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko',
                'category' => '',
                'city' => '',
                'country_code' => '',
                'device_type' => '',
                'keyword' => '',
                'lander_keyword' => '',
                'revenue_keyword' => '',
                'search_term' => '',
                'state' => '',
                'tg1' => '',
                'tg4' => 1425292,
                'tg5' => 'GiC38AfpN6YSxW-c4sZRd94MJ9zwCS6HfRbBLJrW-YZ6sSCM_1Yo4fWMqNSMxNGIAQ',
                'lander_searches' => 1,
                'lander_visitors' => 2,
                'tg10' => '',
                'tg7' => 1385079,
                'tg8' => '004874e386e9a658efbf32506b82466817',
                'tg9' => '0.066',
                'traffic_source' => 'Taboola',
                'campaign_name' => 'campaign a',
                'campaign_type' => 'Native',

                'browser' => '',
                'tg6' => '16183236_1199109_endtimeheadlines-endtimeheadlines_3138426628',
                'account_id' => 64154,
                'tg2' => 428353217,
                'ad_id' => 'Walk-in Bathtubs May Be the Newest Trend in Modern Bathrooms',
                'campaign_number' => 1,
                'tg3' => 3138426628,
                'query_date' => '2021-01-01',
                'match_type' => 1,

            ],
         
            
           


        ]);
        $this->seedRevenueData();
        $this->seedRevenueSiteData();
    }
    public function seedRevenueSiteData()
    {
        $siteData = DB::connection('clickhouse')->select("
        select
        sum(revenue_clicks) as cr_revenue_clicks,
        sum(lander_searches) as cr_lander_searches,
        sum(publisher_revenue_amount) as cr_publisher_revenue_amount,
        sum(lander_visitors) as cr_lander_visitors,
        sum(total_visitors) as cr_total_visitors,
        sum(tracked_visitors) as cr_tracked_visitors,
        day as cr_day,
        hour as cr_hour,
        campaign_number as cr_campaign_id,
        match_type as cr_site_id
        from crossroads_revenue_reports 
        WHERE query_date = '2021-01-01'
        group by cr_day,cr_hour,cr_campaign_id,cr_site_id
        ");
        foreach ($siteData as $revenue) {
            DB::connection('clickhouse')->statement("ALTER TABLE crossroads_revenue_report_site_groupeds DELETE WHERE cr_sit_id='" . $revenue['cr_site_id'] . "' AND cr_campaign_id = '" . $revenue['cr_campaign_id'] . "' AND cr_day = '" . $revenue['cr_day'] . "' AND cr_hour LIKE '" . $revenue['cr_hour'] . "'");
            DB::connection('clickhouse')->statement(
                "INSERT INTO crossroads_revenue_report_site_groupeds 
             (
                 cr_campaign_id,
                 cr_sit_id, 
                 cr_day, 
                 cr_hour, 
                 cr_lander_searches, 
                 cr_lander_visitors, 
                 cr_publisher_revenue_amount, 
                 cr_revenue_clicks, 
                 cr_total_visitors, 
                 cr_tracked_visitors, 
                 created_at, 
                 updated_at
                 ) 
                 VALUES 
                 (
                     '" . $revenue['cr_campaign_id'] . "', 
                     '" . $revenue['cr_site_id'] . "',
                     '" . $revenue['cr_day'] . "', 
                     '" . $revenue['cr_hour'] . "', 
                     " . $revenue['cr_lander_searches'] . ", 
                     " . $revenue['cr_lander_visitors'] . ", 
                     " . $revenue['cr_publisher_revenue_amount'] . ", 
                     " . $revenue['cr_revenue_clicks'] . ", 
                     " . $revenue['cr_total_visitors'] . ", 
                     " . $revenue['cr_tracked_visitors'] . ", 
                     null, 
                     null
                     )"
            );
        }
    }
    public function seedRevenueData()
    {
        $groupedRenue = DB::connection("clickhouse")->select("
        select sum(revenue_clicks) as cr_revenue_clicks,sum(lander_searches) as cr_lander_searches,
        sum(publisher_revenue_amount) as cr_publisher_revenue_amount,sum(lander_visitors) as cr_lander_visitors,
        sum(total_visitors) as cr_total_visitors,sum(tracked_visitors) as cr_tracked_visitors,day as cr_day,hour as cr_hour,
        campaign_number as cr_campaign_id
        from 
        crossroads_revenue_reports WHERE query_date =  '2021-01-01' group by 
        cr_day,cr_hour,cr_campaign_id ");

        foreach ($groupedRenue as $revenue) {
            DB::connection('clickhouse')->statement("ALTER TABLE crossroads_revenue_report_groupeds DELETE WHERE cr_campaign_id = '" . $revenue['cr_campaign_id'] . "' AND cr_day = '" . $revenue['cr_day'] . "' AND cr_hour LIKE '" . $revenue['cr_hour'] . "'");
            DB::connection('clickhouse')->statement(
                "INSERT INTO crossroads_revenue_report_groupeds 
             (
                 cr_campaign_id, 
                 cr_day, 
                 cr_hour, 
                 cr_lander_searches, 
                 cr_lander_visitors, 
                 cr_publisher_revenue_amount, 
                 cr_revenue_clicks, 
                 cr_total_visitors, 
                 cr_tracked_visitors, 
                 created_at, 
                 updated_at
                 ) 
                 VALUES 
                 (
                     '" . $revenue['cr_campaign_id'] . "', 
                     '" . $revenue['cr_day'] . "', 
                     '" . $revenue['cr_hour'] . "', 
                     " . $revenue['cr_lander_searches'] . ", 
                     " . $revenue['cr_lander_visitors'] . ", 
                     " . $revenue['cr_publisher_revenue_amount'] . ", 
                     " . $revenue['cr_revenue_clicks'] . ", 
                     " . $revenue['cr_total_visitors'] . ", 
                     " . $revenue['cr_tracked_visitors'] . ", 
                     null, 
                     null
                     )"
            );
        }
    }
}
