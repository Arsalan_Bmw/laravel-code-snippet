<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Role;
use Ranium\SeedOnce\Traits\SeedOnce;
class UsersRolesSeeder extends Seeder
{
    use SeedOnce;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Role::truncate();
        $Roles = [
            [
                'slug' => 'superadmin',
                'name' => 'superadmin',
                'permissions' => null,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'slug' => 'admin',
                'name' => 'admin',
                'permissions' => null,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'slug' => 'user',
                'name' => 'user',
                'permissions' => null,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]
        ];
        foreach ($Roles as $role) {
            Role::create(['name' => $role['name'], 'guard_name' => 'api']);
        }
        Schema::enableForeignKeyConstraints();
    }
}
