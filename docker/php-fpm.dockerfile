FROM php:7.4-fpm-alpine

ADD ./conf/php/laravel.ini "$PHP_INI_DIR"

RUN mkdir -p /var/www/html
# Use the default production configuration

WORKDIR /var/www/html
RUN docker-php-ext-install pdo pdo_mysql