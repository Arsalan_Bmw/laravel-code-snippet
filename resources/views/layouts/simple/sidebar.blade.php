<div class="sidebar-wrapper">
   <div class="logo-wrapper">
      <a href="{{route('/')}}"><img class="img-fluid for-light" src="{{asset('assets/images/logo/logo.png')}}" alt="" /><img class="img-fluid for-dark" src="{{asset('assets/images/logo/logo_dark.png')}}" alt="" /></a>
      <div class="back-btn"><i class="fa fa-angle-left"></i></div>
      <div class="toggle-sidebar left"><i class="status_toggle middle sidebar-toggle" data-feather="chevron-left"> </i></div>
      <div class="toggle-sidebar right"><i class="status_toggle middle sidebar-toggle" data-feather="chevron-right"> </i></div>
   </div>
   <div class="logo-icon-wrapper">
   <div class="toggle-sidebar"><i data-feather="chevron-right"></i></div>
   </div>
   <nav class="sidebar-main">
      <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
      <div id="sidebar-menu">
         <ul class="sidebar-links custom-scrollbar">
            <li class="back-btn">
               <a href="{{route('/')}}"><img class="img-fluid" src="{{asset('assets/images/logo/logo-icon.png')}}" alt="" /></a>
               <div class="mobile-back text-right"><span>Back</span><i class="fa fa-angle-right pl-2" aria-hidden="true"></i></div>
            </li>
            <li class="sidebar-list">
               <a class="sidebar-link sidebar-title" href="#" ><i data-feather="box"></i><span>FB Ad Accounts</span>
               </a>
               @if(Sentinel::inRole('superadmin'))
                  <ul class="sidebar-submenu" style="display:none;">
                     <li><a href="{{route('list-account')}}" class="{{ Route::currentRouteName()=='list-account' ? 'active' : '' }}">Account List</a></li>
                     <li><a href="{{route('create-account')}}" class="{{ Route::currentRouteName()=='create-account' ? 'active' : '' }}">Create Account</a></li>
                  </ul>
               @endif
               @if(Sentinel::inRole('admin'))
                  <ul class="sidebar-submenu" style="display:none;">
                     <li><a href="{{route('admin-list-account')}}" class="{{ Route::currentRouteName()=='admin-list-account' ? 'active' : '' }}">Account List</a></li>
                     <li><a href="{{route('admin-create-account')}}" class="{{ Route::currentRouteName()=='admin-create-account' ? 'active' : '' }}">Create Account</a></li>
                  </ul>
               @endif
               @if(Sentinel::inRole('manager'))
                  <ul class="sidebar-submenu" style="display:none;">
                     <li><a href="{{route('manager-list-account')}}" class="{{ Route::currentRouteName()=='manager-list-account' ? 'active' : '' }}">Account List</a></li>
                  </ul>
               @endif
               @if(Sentinel::inRole('accountant'))
                  <ul class="sidebar-submenu" style="display:none;">
                     <li><a href="{{route('accountant-list-account')}}" class="{{ Route::currentRouteName()=='manager-list-account' ? 'active' : '' }}">Account List</a></li>
                  </ul>
               @endif
            </li>
            @if(Sentinel::inRole('superadmin'))
               <li class="sidebar-list">
                  <a class="sidebar-link sidebar-title" href="#"><i data-feather="users"></i><span class="lan-3">Users</span>
                  </a>
                  <ul class="sidebar-submenu" style="display:none;">
                     <li><a class="lan-4 {{ Route::currentRouteName()=='create-user' ? 'active' : '' }}" href="{{route('create-user')}}">Create User</a></li>
                     <li><a class="lan-4 {{ Route::currentRouteName()=='list-user' ? 'active' : '' }}" href="{{route('list-user')}}">Users</a></li>
                  </ul> 
               </li>
            @endif
         </ul>
      </div>
      <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
   </nav>
</div>
