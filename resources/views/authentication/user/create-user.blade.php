@extends('layouts.simple.master')
@section('title', 'Validation Forms')

@section('css')
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
<h3>Users</h3>
@endsection

@section('breadcrumb-items')
<li class="breadcrumb-item active">Create User</li>
@endsection

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-header">
					<h5>Create New User</h5>
				</div>
				<div class="card-body">
						@include('shared.messages')
						@if ($errors->users->first('email') != '')
						    <div class="alert alert-danger alert-error ">
						        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						        {{$errors->users->first('email')}}
						    </div>
						@endif
						{!! Form::open(['route' => ['store-user'], 'method' => 'POST','name' => 'create_user', 'class' => 'needs-validation', 'autocomplete' => 'off', 'novalidate' => '']) !!}
						<div class="row">
							<div class="col-md-4 mb-3">
								<label for="validationCustom01">First name</label>
								<input class="form-control" id="validationCustom01" type="text" placeholder="First name" name="first_name" value="{{ old('first_name') }}" required="">
								<div class="valid-feedback">Looks good!</div>
							</div>
							<div class="col-md-4 mb-3">
								<label for="validationCustom02">Last name</label>
								<input class="form-control" id="validationCustom02" type="text" placeholder="Last name" name="last_name" value="{{ old('last_name') }}"  required="">
								<div class="valid-feedback">Looks good!</div>
							</div>
							<div class="col-md-4 mb-3">
								<label for="validationCustom03">Email</label>
								<input class="form-control" id="validationCustom03" type="email" placeholder="test@gmail.com" name="email" value="{{ old('email') }}" required="">
								<div class="valid-feedback">Looks good!</div>
							</div>
							<div class="col-md-4 mb-3">
								<label for="validationCustom04">Password</label>
								<input class="form-control" id="validationCustom04" type="password" placeholder="Password" name="password" required="">
								<div class="valid-feedback">Looks good!</div>
							</div>
							<div class="col-md-4 mb-3">
								<label for="validationCustom05">Select User Type</label>
								<select class="form-control" id="validationCustom05" name="usertype">
									<option value="">Select</option>
									<option value="2">Admin</option>
									<option value="3">Media Buyer</option>
									{{-- <option value="4">accountant</option> --}}
								</select>
							</div>
						</div>
						<button class="btn btn-primary" type="submit">Submit form</button>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script src="{{asset('assets/js/form-validation-custom.js')}}"></script>
@endsection