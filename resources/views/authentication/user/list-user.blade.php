@extends('layouts.simple.master')
@section('title', 'Basic DataTables')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/vendors/datatables.css')}}">
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
<h3>Users</h3>
@endsection

@section('breadcrumb-items')
<li class="breadcrumb-item active">Users</li>
@endsection

@section('content')
<div class="container-fluid">
	<div class="row">
		<!-- Zero Configuration  Starts-->
		<div class="col-sm-12">
			<div class="card">
				<div class="card-body">
					@include('shared.messages')
					<div class="table-responsive">
						<table class="display" id="basic-1">
							<thead>
								<tr>
									<th>First Name</th>
									<th>Last Name</th>
									<th>Email</th>
									<th>Role</th>
									<th></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								@foreach($users as $user)
									<tr>
										<td>{{$user->first_name}}</td>
										<td>{{$user->last_name}}</td>
										<td>{{$user->email}}</td>
										@foreach ($user->roles as $role)
										 <td>{{($role->name == 'manager' ? 'media buyer' : $role->name)}}</td>
										@endforeach
										<td><a href="{{ route('edit-user', ['user_id' => $user->id]) }}"> <i data-feather="edit"></i></a></td>
										<td><a href="{{ route('delete-user', ['user_id' => $user->id]) }}" onclick="return confirm('Are you sure you want to delete this user?');"> <i data-feather="trash-2"></i></a></td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- Zero Configuration  Ends-->
	</div>
</div>
@endsection

@section('script')
<script src="{{asset('assets/js/datatable/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/datatable/datatables/datatable.custom.js')}}"></script>
@endsection