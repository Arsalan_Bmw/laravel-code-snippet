@extends('layouts.simple.master')
@section('title', 'Validation Forms')

@section('css')
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
<h3>Users</h3>
@endsection

@section('breadcrumb-items')
<li class="breadcrumb-item active"><a href="{{ route('list-user') }}">List Users</a></li>
<li class="breadcrumb-item active">Edit User</li>
@endsection

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header">
          <h5>Edit User</h5>
        </div>
        <div class="card-body">
            @include('shared.messages')
            @if($errors->users->first('email') != '')
                <div class="alert alert-danger alert-error ">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{$errors->users->first('email')}}
                </div>
            @endif
            {!! Form::open(['route' => ['update-user'],'method' => 'POST','name' => 'login', 'class' => 'needs-validation', 'autocomplete' => 'off', 'novalidate' => '']) !!}
            <div class="row">
                {!! Form::hidden('user_id', old('user_id', isset($editUsers->id) ? $editUsers->id : ''),  ['id' => 'user_id', 'class' => 'form-control', ]) !!}
              <div class="col-md-4 mb-3">
                <label for="validationCustom01">First name</label>
                {!! Form::text('first_name', old('first_name', isset($editUsers->first_name) ? $editUsers->first_name : ''),  ['id' => 'validationCustom01', 'class' => 'form-control', 'placeholder' => 'First Name', 'required' =>'' ]) !!}
                <div class="valid-feedback">Looks good!</div>
              </div>
              <div class="col-md-4 mb-3">
                <label for="validationCustom02">Last name</label>
                {!! Form::text('last_name', old('last_name', isset($editUsers->last_name) ? $editUsers->last_name : ''),  ['id' => 'validationCustom02', 'class' => 'form-control', 'placeholder' => 'Last Name', 'required' =>'' ]) !!}
                <div class="valid-feedback">Looks good!</div>
              </div>
              <div class="col-md-4 mb-3">
                <label for="validationCustom03">Email</label>
                {!! Form::email('email', old('email', isset($editUsers->email) ? $editUsers->email : ''),  ['id' => 'validationCustom03', 'class' => 'form-control', 'placeholder' => 'Email', 'required' =>'']) !!}
                <div class="valid-feedback">Looks good!</div>
              </div>
              <div class="col-md-4 mb-3">
                <label for="validationCustom05">Select User Type </label>
                <select class="form-control" id="validationCustom05" name="usertype">
                  <option value="">Select</option>
                  <option value="1" {{($editUsers->roles[0]['name'] == 'superadmin' ? 'selected' : '')}}>Super Admin</option>
                  <option value="2" {{($editUsers->roles[0]['name'] == 'admin' ? 'selected' : '')}}>Admin</option>
                  <option value="3" {{($editUsers->roles[0]['name'] == 'manager' ? 'selected' : '')}}>Media Buyer</option>
                  {{-- <option value="4" {{($editUsers->roles[0]['name'] == 'accountant' ? 'selected' : '')}}>accountant</option> --}}
                </select>
              </div>
            </div>
            <button class="btn btn-primary" type="submit">Submit form</button>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script src="{{asset('assets/js/form-validation-custom.js')}}"></script>
@endsection