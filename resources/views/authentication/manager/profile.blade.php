@extends('layouts.simple.master')
@section('title', 'Validation Forms')

@section('css')
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
<h3>Profile</h3>
@endsection

@section('breadcrumb-items')
<li class="breadcrumb-item active">Profile</li>
@endsection

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-sm-12">
			<div class="card">
				<div class="card-header">
					<h5>Profile</h5>
				</div>
				<div class="card-body">
					@include('shared.messages')
					@if($errors->users->first('email') != '')
					    <div class="alert alert-danger alert-error ">
					        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					        {{$errors->users->first('email')}}
					    </div>
					@endif
					{!! Form::open(['route' => ['manager-update-profile'],'method' => 'POST','name' => 'login', 'class' => 'needs-validation', 'autocomplete' => 'off', 'novalidate' => '']) !!}
					<div class="row">
						{!! Form::hidden('user_id', old('user_id', isset($user->id) ? $user->id : ''),  ['id' => 'user_id', 'class' => 'form-control', ]) !!}
						<div class="col-md-12 mb-3">
							<label for="validationCustom01">First name</label>
							{!! Form::text('first_name', old('first_name', isset($user->first_name) ? $user->first_name : ''),  ['id' => 'validationCustom01', 'class' => 'form-control', 'placeholder' => 'First Name', 'required' =>'' ]) !!}
							<div class="valid-feedback">Looks good!</div>
						</div>
						<div class="col-md-12 mb-3">
							<label for="validationCustom02">Last name</label>
							{!! Form::text('last_name', old('last_name', isset($user->last_name) ? $user->last_name : ''),  ['id' => 'validationCustom02', 'class' => 'form-control', 'placeholder' => 'Last Name', 'required' =>'' ]) !!}
							<div class="valid-feedback">Looks good!</div>
						</div>
						<div class="col-md-12 mb-3">
							<label for="validationCustom03">Email</label>
							{!! Form::email('email', old('email', isset($user->email) ? $user->email : ''),  ['id' => 'validationCustom03', 'class' => 'form-control', 'placeholder' => 'Email', 'required' =>'' ]) !!}
							<div class="valid-feedback">Looks good!</div>
						</div>
						<div class="col-md-12 mb-3">
							<label for="validationCustom05">Role</label>
							<select class="form-control" id="validationCustom05" disabled="true">
								<option>Select</option>
								<option {{(Sentinel::inRole('superadmin') ? 'selected' : '')}}>Super Admin</option>
								<option {{(Sentinel::inRole('admin') ? 'selected' : '')}}>Admin</option>
								<option {{(Sentinel::inRole('manager') ? 'selected' : '')}}>Media Buyer</option>
								{{-- <option {{(Sentinel::inRole('accountant') ? 'selected' : '')}}>Accountant</option> --}}
							</select>
						</div>
					</div>
					<button class="btn btn-primary" type="submit">Submit form</button>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script src="{{asset('assets/js/form-validation-custom.js')}}"></script>
@endsection