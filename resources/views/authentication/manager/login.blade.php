@extends('layouts.authentication.master')
@section('title', 'Login')

@section('css')
@endsection

@section('style')
@endsection


@section('content')
<div class="container-fluid">
   <div class="row">
      <div class="col-12">
         <div class="login-card">
            <div>
               <div><a class="logo" href="{{ route('index') }}"><img class="img-fluid for-light" src="{{asset('assets/images/logo/login.png')}}" alt="looginpage"><img class="img-fluid for-dark" src="{{asset('assets/images/logo/logo_dark.png')}}" alt="looginpage"></a></div>
               <div class="login-main">
                  @include('shared.messages')
                  {!! Form::open(['route' => ['manager-checklogin'], 'method' => 'POST','name' => 'login', 'class' => 'theme-form', 'autocomplete' => 'off']) !!}
                     <h4>Sign in to account</h4>
                     <p>Enter your email & password to login</p>
                     <div class="form-group">
                        <label class="col-form-label">Email Address</label>
                        {!! Form::text('email', old('email'),  ['id' => 'email', 'class' => 'form-control', 'placeholder' => 'EMAIL', '' ]) !!}
                        @if ($errors->has('email'))
                        <span class="error text-danger">
                           <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                     </div>
                     <div class="form-group">
                        <label class="col-form-label">Password</label>
                        {!! Form::password('password', ['id' => 'password', 'class' => 'form-control' , 'placeholder' => 'PASSWORD' ]) !!}
                        @if ($errors->has('password'))
                           <span class="error text-danger">
                               <strong>{{ $errors->first('password') }}</strong>
                           </span>
                        @endif
                        <!-- <div class="show-hide"><span class="show"></span></div> -->
                     </div>
                     <div class="form-group mb-0">
                        <!-- <div class="checkbox p-0">
                           <input id="checkbox1" type="checkbox">
                           <label class="text-muted" for="checkbox1">Remember password</label>
                        </div> -->
                        <!-- <a class="link" href="{{ route('forget-password') }}">Forgot password?</a> -->
                        <button class="btn btn-primary btn-block" type="submit">Sign in</button>
                     </div>
                  {!! Form::close() !!}
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection

@section('script')
@endsection