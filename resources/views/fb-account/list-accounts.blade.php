@extends('layouts.simple.master')
@section('title', 'FB Accounts')

@section('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/vendors/datatables.css')}}">
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
<h3>FB Accounts</h3>
@endsection

@section('breadcrumb-items')
<li class="breadcrumb-item active">FB Accounts</li>
@endsection

@section('content')
<div class="container-fluid">
   <div class="row">
      <!-- Zero Configuration  Starts-->
      <div class="col-sm-12">
         <div class="card">
            <div class="card-body">
               @include('shared.messages')
               <div class="table-responsive">
                  <div id="accounts">
                     <table class="display" id="accounts-table">
                        <thead>
                           <tr>
                              <th>ID</th>
                              <th>FB Ad Account ID</th>
                              <th>Build Name</th>
                              <th>Next Step</th>
                              <th>Status</th>
                              <th>Priority</th>
                              <th>Source</th>
                              <th>FB UserName</th>
                              <th>Business Manager Name</th>
                              <th>Media Buyer</th>
                              <th>Log Date</th>
                              <th></th>
                              <th></th>
                              @if(Sentinel::inRole('superadmin') || Sentinel::inRole('admin'))
                              <th></th>
                              @endif
                           </tr>
                        </thead>
                        <tbody>
                           @foreach($fbAccounts as $fbAccount)
                           <tr>
                              <td><a href="@if(Sentinel::inRole('superadmin')) {{ route('view-account', ['account_id' => $fbAccount->id]) }} @elseif(Sentinel::inRole('admin')) {{ route('admin-view-account', ['account_id' => $fbAccount->id]) }} @elseif(Sentinel::inRole('manager')) {{ route('manager-view-account', ['account_id' => $fbAccount->id]) }} @elseif(Sentinel::inRole('accountant')) {{ route('accountant-view-account', ['account_id' => $fbAccount->id]) }} @endif">{{$fbAccount->account_id}}</a></td>
                              <td><a href="@if(Sentinel::inRole('superadmin')) {{ route('view-account', ['account_id' => $fbAccount->id]) }} @elseif(Sentinel::inRole('admin')) {{ route('admin-view-account', ['account_id' => $fbAccount->id]) }} @elseif(Sentinel::inRole('manager')) {{ route('manager-view-account', ['account_id' => $fbAccount->id]) }} @elseif(Sentinel::inRole('accountant')) {{ route('accountant-view-account', ['account_id' => $fbAccount->id]) }} @endif">{{$fbAccount->ad_account_id}}</a></td>
                              <td><a href="@if(Sentinel::inRole('superadmin')) {{ route('view-account', ['account_id' => $fbAccount->id]) }} @elseif(Sentinel::inRole('admin')) {{ route('admin-view-account', ['account_id' => $fbAccount->id]) }} @elseif(Sentinel::inRole('manager')) {{ route('manager-view-account', ['account_id' => $fbAccount->id]) }} @elseif(Sentinel::inRole('accountant')) {{ route('accountant-view-account', ['account_id' => $fbAccount->id]) }} @endif">{{$fbAccount->build_name}}</a></td>
                              <td>{{$fbAccount->next_step}}</td>
                              <td>{{$fbAccount->status}}</td>
                              <td>{{$fbAccount->priority}}</td>
                              <td>{{$fbAccount->source}}</td>
                              <td>{{$fbAccount->fb_user_full_name}}</td>
                              <td>{{$fbAccount->business_manager_name}}</td>
                              <td>{{$fbAccount->media_buyer}}</td>
                              <td>{{$fbAccount->date}}</td>
                              <td><a href="@if(Sentinel::inRole('superadmin')) {{ route('view-account', ['account_id' => $fbAccount->id]) }} @elseif(Sentinel::inRole('admin')) {{ route('admin-view-account', ['account_id' => $fbAccount->id]) }} @elseif(Sentinel::inRole('manager')) {{ route('manager-view-account', ['account_id' => $fbAccount->id]) }} @elseif(Sentinel::inRole('accountant')) {{ route('accountant-view-account', ['account_id' => $fbAccount->id]) }} @endif"> <i data-feather="eye"></i></a></td>
                              <td><a href="@if(Sentinel::inRole('superadmin')){{ route('edit-account', ['account_id' => $fbAccount->id]) }} @elseif(Sentinel::inRole('admin')){{ route('admin-edit-account', ['account_id' => $fbAccount->id]) }} @elseif(Sentinel::inRole('manager')){{ route('manager-edit-account', ['account_id' => $fbAccount->id]) }} @elseif(Sentinel::inRole('accountant')){{ route('accountant-edit-account', ['account_id' => $fbAccount->id]) }} @endif"> <i data-feather="edit"></i></a></td>

                              @if(Sentinel::inRole('superadmin') || Sentinel::inRole('admin'))
                                 @if($fbAccount->archive == '0')
                                    <td><a href="@if(Sentinel::inRole('superadmin')){{ route('archive-account', ['account_id' => $fbAccount->id]) }} @elseif(Sentinel::inRole('admin')){{ route('admin-archive-account', ['account_id' => $fbAccount->id]) }} @endif" onclick="return confirm('Are you sure you want to move this Ad account to archive?');"> <i data-feather="archive"></i></a></td>
                                 @elseif($fbAccount->archive == '1')
                                    <td><a href="@if(Sentinel::inRole('superadmin')){{ route('unarchive-account', ['account_id' => $fbAccount->id]) }} @elseif(Sentinel::inRole('admin')){{ route('admin-unarchive-account', ['account_id' => $fbAccount->id]) }} @endif" onclick="return confirm('Are you sure you want to restore this Ad account from archive?');"> <i data-feather="inbox"></i></a></td>
                                 @endif
                              @endif
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Zero Configuration  Ends-->
   </div>
</div>
@endsection

@section('script')
<script src="{{asset('assets/js/datatable/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/js/datatable/datatables/datatable.custom.js')}}"></script>
<script type="text/javascript">
   $(document).ready(function() {
      $(document.body).on('click', '#show-all-accounts', function(event) {
         console.log('click');
         if($(this).is(":checked")){
             window.location.href = window.location.pathname + "?show=all";
         }
         else{
            window.location.href = window.location.pathname;
         }
      });
   });
   $(document).ready(function() {
      var table = $("#accounts-table").DataTable({
         //default is lfrtip 
          dom: 'fr<"filter-wrapper"><"toolbar">tlip',
          columnDefs: [
                          { orderable: true, targets: [0,1,2,3,4,5,6,7,8,9,10]},
                          { orderable: false, targets: '_all' }
                      ]
      });
      $('div.filter-wrapper').html('<div id="filter_col4" data-column="4"><select name="status" class="form-control column_filter" id="col4_filter" ><option value="">Status</option><option value="Active">Active</option><option value="Appealing">Appealing</option><option value="Building">Building</option><option value="Closed">Closed</option><option value="CS Issue">CS Issue</option><option value="Dismantle">Dismantle</option><option value="ID Flagged">ID Flagged</option><option value="Replaced">Replaced</option><option value="User Flagged">User Flagged</option><option value="WH Warming">WH Warming</option></select></div><div id="filter_col6" class="" data-column="6"><select name="source" class="form-control column_filter" id="col6_filter" ><option value="">Source</option><option value="DFM">DFM</option><option value="DGS">DGS</option><option value="EDR">EDR</option><option value="FBS">FBS</option><option value="JVN">JVN</option><option value="LVY">LVY</option><option value="MAX">MAX</option><option value="MNR">MNR</option><option value="RYG">RYG</option><option value="SAC">SAC</option><option value="TM8">TM8</option><option value="VBM">VBM</option></select></div><div id="filter_col9" data-column="9"><select name="media_buyer" class="form-control column_filter" id="col9_filter" ><option value="">Media Buyer</option><option value="JJ">JJ</option><option value="ER">ER</option><option value="MF">MF</option><option value="RW">RW</option><option value="SK">SK</option><option value="PC">PC</option></select></div>');

      $("div.toolbar").html('<div class="include-archive-wrapper"><label style="cursor: pointer;font-weight: bold;" for="show-all-accounts"><input class="checkbox_animated" id="show-all-accounts" type="checkbox"data-original-title="" title="" {{(request()->get('show') == 'all' ? 'checked' : '')}}>Include Archived</label></div>');
      //add margin to the right and reset clear
      $(".dataTables_length").css('clear', 'none');
      $(".dataTables_length").css('margin-right', '20px');
      

      //reset clear and padding
      $(".dataTables_info").css('clear', 'none');
      $(".dataTables_info").css('padding-top', '8px');
      $(".filter-wrapper").css({'display':'inline-block','width':'55%'});
      $("#filter_col4").css({'display':'inline-block','margin':'0 10px'});
      $("#filter_col6").css({'display':'inline-block','margin':'0 10px'});
      $("#filter_col9").css({'display':'inline-block','margin':'0 10px'});
      $("#filter_col4 select").css({'display':'inline-block'});
      $("#filter_col6 select").css({'display':'inline-block'});
      $("#filter_col9 select").css({'display':'inline-block'});

      function filterColumn ( i ) {
          $('#accounts-table').DataTable().column( i ).search(
              $('#col'+i+'_filter').val()
          ).draw();
      }

      $('select.column_filter').on( 'keyup click', function () {
         console.log($(this).parents('div').attr('data-column'));
             filterColumn( $(this).parents('div').attr('data-column') );
         } );
   });
     
</script>
@endsection