@extends('layouts.simple.master')
@section('title', 'FB Accounts')

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/date-picker.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/dropzone.css')}}">
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
<h3>Create FB Account</h3>
@endsection

@section('breadcrumb-items')
<li class="breadcrumb-item"><a href="{{ route('/') }}">FB Accounts</a></li>
<li class="breadcrumb-item active">Create Account</li>
@endsection

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="">
        <div class="">
          @include('shared.messages')
          @if(Sentinel::inRole('superadmin'))
          {!! Form::open(['route' => ['store-account'],'method' => 'POST','name' => 'create_account', 'class' => 'needs-validation', 'autocomplete' => 'off', 'novalidate' => '']) !!}
          @elseif(Sentinel::inRole('admin'))
          {!! Form::open(['route' => ['admin-store-account'],'method' => 'POST','name' => 'create_account', 'class' => 'needs-validation', 'autocomplete' => 'off', 'novalidate' => '']) !!}
          @endif
            <div class="default-according style-1" id="accordion">
              <div class="card" id="general-fields-cat">
                <div class="card-header" id="general-fields">
                  <h5 class="mb-0">
                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#generalcollapse" aria-expanded="true" aria-controls="general-fields">General Fields</button>
                  </h5>
                </div>
                <div class="collapse show" id="generalcollapse" aria-labelledby="general-fields">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="account_id">ID</label>
                          <input class="form-control" id="account_id" type="number" placeholder="ID" name="account_id" value="{{$lastRecord ? $lastRecord->id + 1: 1}}"  readonly="">
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-8">
                        <div class="form-group">
                          <label for="build_name">Build Name</label>
                          <input class="form-control" id="build_name" name="build_name" type="Text" placeholder="Build Name">
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="status">Status</label>
                          <select class="form-control" id="status" name="status" >
                            <option value="">Select</option>
                            <option value="Active">Active</option>
                            <option value="Appealing">Appealing</option>
                            <option value="Building">Building</option>
                            <option value="Closed">Closed</option>
                            <option value="CS Issue">CS Issue</option>
                            <option value="Dismantle">Dismantle</option>
                            <option value="ID Flagged">ID Flagged</option>
                            <option value="Replaced">Replaced</option>
                            <option value="User Flagged">User Flagged</option>
                            <option value="WH Warming">WH Warming</option>
                          </select>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="priority">Priority</label>
                          <select class="form-control" id="priority" name="priority" >
                            <option value="">Select</option>
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                          </select>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        <div class="form-group">
                          <label for="quick_note">Quick Note</label>
                          <textarea class="form-control" id="quick_note" name="quick_note" type="Text" maxlength="255" rows="3" placeholder="Quick Note" ></textarea>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        <div class="form-group">
                          <label for="accesspoint_notes">Access Point Notes</label>
                          <textarea class="form-control" name="accesspoint_notes" id="accesspoint_notes" rows="3" placeholder="Access Point Notes"></textarea>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="source">Source</label>
                          <select id="source" name="source" class="form-control" >
                            <option value="">Select</option>
                            <option value="DFM">DFM</option>
                            <option value="DGS">DGS</option>
                            <option value="EDR">EDR</option>
                            <option value="FBS">FBS</option>
                            <option value="JVN">JVN</option>
                            <option value="LVY">LVY</option>
                            <option value="MAX">MAX</option>
                            <option value="MNR">MNR</option>
                            <option value="RYG">RYG</option>
                            <option value="SAC">SAC</option>
                            <option value="TM8">TM8</option>
                            <option value="VBM">VBM</option>
                          </select>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="comment_management_date">Comment Management Date</label>
                          <input class="datepicker-here form-control" name="comment_management_date" id="comment_management_date" type="text" data-language="en" >
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="date_of_next_action">Date Of Next Action</label>
                          <input class="datepicker-here form-control" name="date_of_next_action" id="date_of_next_action" type="text" data-language="en" >
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                    <div class="col">
                      <label for="timezone">Timezone</label>
                      <select name="timezone" id="timezone" class="form-control">
                        <<option value="">Select</option>
                        <option value="-12:00">(GMT -12:00) Eniwetok, Kwajalein</option>
                        <option value="-11:00">(GMT -11:00) Midway Island, Samoa</option>
                        <option value="-10:00">(GMT -10:00) Hawaii</option>
                        <option value="-09:50">(GMT -9:30) Taiohae</option>
                        <option value="-09:00">(GMT -9:00) Alaska</option>
                        <option value="-08:00">(GMT -8:00) Pacific Time (US &amp; Canada)</option>
                        <option value="-07:00">(GMT -7:00) Mountain Time (US &amp; Canada)</option>
                        <option value="-06:00">(GMT -6:00) Central Time (US &amp; Canada), Mexico City</option>
                        <option value="-05:00">(GMT -5:00) Eastern Time (US &amp; Canada), Bogota, Lima</option>
                        <option value="-04:50">(GMT -4:30) Caracas</option>
                        <option value="-04:00">(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz</option>
                        <option value="-03:50">(GMT -3:30) Newfoundland</option>
                        <option value="-03:00">(GMT -3:00) Brazil, Buenos Aires, Georgetown</option>
                        <option value="-02:00">(GMT -2:00) Mid-Atlantic</option>
                        <option value="-01:00">(GMT -1:00) Azores, Cape Verde Islands</option>
                        <option value="+00:00">(GMT) Western Europe Time, London, Lisbon, Casablanca</option>
                        <option value="+01:00">(GMT +1:00) Brussels, Copenhagen, Madrid, Paris</option>
                        <option value="+02:00">(GMT +2:00) Kaliningrad, South Africa</option>
                        <option value="+03:00">(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg</option>
                        <option value="+03:50">(GMT +3:30) Tehran</option>
                        <option value="+04:00">(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi</option>
                        <option value="+04:50">(GMT +4:30) Kabul</option>
                        <option value="+05:00">(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent</option>
                        <option value="+05:50">(GMT +5:30) Bombay, Calcutta, Madras, New Delhi</option>
                        <option value="+05:75">(GMT +5:45) Kathmandu, Pokhara</option>
                        <option value="+06:00">(GMT +6:00) Almaty, Dhaka, Colombo</option>
                        <option value="+06:50">(GMT +6:30) Yangon, Mandalay</option>
                        <option value="+07:00">(GMT +7:00) Bangkok, Hanoi, Jakarta</option>
                        <option value="+08:00">(GMT +8:00) Beijing, Perth, Singapore, Hong Kong</option>
                        <option value="+08:75">(GMT +8:45) Eucla</option>
                        <option value="+09:00">(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk</option>
                        <option value="+09:50">(GMT +9:30) Adelaide, Darwin</option>
                        <option value="+10:00">(GMT +10:00) Eastern Australia, Guam, Vladivostok</option>
                        <option value="+10:50">(GMT +10:30) Lord Howe Island</option>
                        <option value="+11:00">(GMT +11:00) Magadan, Solomon Islands, New Caledonia</option>
                        <option value="+11:50">(GMT +11:30) Norfolk Island</option>
                        <option value="+12:00">(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka</option>
                        <option value="+12:75">(GMT +12:45) Chatham Islands</option>
                        <option value="+13:00">(GMT +13:00) Apia, Nukualofa</option>
                        <option value="+14:00">(GMT +14:00) Line Islands, Tokelau</option>
                      </select>
                    </div>
                  </div> 
                  </div>
                </div>
              </div>
              <div class="card" id="facebook-fields-cat">
                <div class="card-header" id="facebook-fields">
                  <h5 class="mb-0">
                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#facebookcollapse" aria-expanded="true" aria-controls="facebook-fields">Facebook Fields</button>
                  </h5>
                </div>
                <div class="collapse show" id="facebookcollapse" aria-labelledby="facebook-fields">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="fb_user_full_name">FB User Full Name</label>
                            <input type="text" name="fb_user_full_name" id="fb_user_full_name" class="form-control"  >
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="fb_user_login_info">FB User Login Info</label>
                            <input type="text" name="fb_user_login_info" id="fb_user_login_info" class="form-control" >
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="fb_user_login_info">2FA Secret</label>
                            <input type="text" name="twofa_secret" id="twofa_secret" class="form-control">
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="type_of_ad_account">Type Of Ad Account</label>
                          <select id="type_of_ad_account" name="type_of_ad_account" class="form-control" >
                            <option value="">Select</option>
                            <option value="standard">Standard</option>
                            <option value="personal">Personal</option>
                          </select>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="ad_account_name">Ad Account Name</label>
                          <input class="form-control" name="ad_account_name" id="ad_account_name" type="text"  >
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="ad_account_id">Ad Account ID</label>
                          <input class="form-control" name="ad_account_id" id="ad_account_id" type="text"  >
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        <div class="form-group">
                          <label for="active_ad_url">Active Ad URL</label>
                          <input class="form-control" name="active_ad_url" id="active_ad_url" type="text">
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="business_manager_name">Business Manager Name</label>
                          <input class="form-control" name="business_manager_name" id="business_manager_name" type="text" >
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="business_manager_id">Business Manager ID</label>
                          <input class="form-control" name="business_manager_id" id="business_manager_id" type="text"  >
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="business_verified">Business Verified</label>
                          <select id="business_verified" name="business_verified" class="form-control" >
                            <option value="">Select</option>
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                            <option value="In Review">In Review </option>
                          </select>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="starting_max_spend_of_account">Starting Max Spend Of Account</label>
                          <input class="form-control" name="starting_max_spend_of_account" id="starting_max_spend_of_account" type="number" min="0" >
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="cost_of_ad_account">Cost Of Ad Account</label>
                          <input class="form-control" name="cost_of_ad_account" id="cost_of_ad_account" type="number" min="0" >
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="page_name">Page Name</label>
                          <input class="form-control" name="page_name" id="page_name" type="text" >
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="page_url">Page URL</label>
                          <input class="form-control" name="page_url" id="page_url" type="text" >
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="page_id">Page ID</label>
                          <input class="form-control" name="page_id" id="page_id" type="text" >
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="post_scheduled">Post Scheduled</label>
                          <select id="post_scheduled" name="post_scheduled" class="form-control" >
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                          </select>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="last_day_post_scheduled">Last Day Post Scheduled</label>
                          <input class="datepicker-here form-control" name="last_day_post_scheduled" id="last_day_post_scheduled" type="text" data-language="en">
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        <label for="geo">Geo</label>
                        <select class="form-control" name="geo" id="geo">
                           <option value="">Select</option>
                           <option value="Afganistan">Afghanistan</option>
                           <option value="Albania">Albania</option>
                           <option value="Algeria">Algeria</option>
                           <option value="American Samoa">American Samoa</option>
                           <option value="Andorra">Andorra</option>
                           <option value="Angola">Angola</option>
                           <option value="Anguilla">Anguilla</option>
                           <option value="Antigua & Barbuda">Antigua & Barbuda</option>
                           <option value="Argentina">Argentina</option>
                           <option value="Armenia">Armenia</option>
                           <option value="Aruba">Aruba</option>
                           <option value="Australia">Australia</option>
                           <option value="Austria">Austria</option>
                           <option value="Azerbaijan">Azerbaijan</option>
                           <option value="Bahamas">Bahamas</option>
                           <option value="Bahrain">Bahrain</option>
                           <option value="Bangladesh">Bangladesh</option>
                           <option value="Barbados">Barbados</option>
                           <option value="Belarus">Belarus</option>
                           <option value="Belgium">Belgium</option>
                           <option value="Belize">Belize</option>
                           <option value="Benin">Benin</option>
                           <option value="Bermuda">Bermuda</option>
                           <option value="Bhutan">Bhutan</option>
                           <option value="Bolivia">Bolivia</option>
                           <option value="Bonaire">Bonaire</option>
                           <option value="Bosnia & Herzegovina">Bosnia & Herzegovina</option>
                           <option value="Botswana">Botswana</option>
                           <option value="Brazil">Brazil</option>
                           <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                           <option value="Brunei">Brunei</option>
                           <option value="Bulgaria">Bulgaria</option>
                           <option value="Burkina Faso">Burkina Faso</option>
                           <option value="Burundi">Burundi</option>
                           <option value="Cambodia">Cambodia</option>
                           <option value="Cameroon">Cameroon</option>
                           <option value="Canada">Canada</option>
                           <option value="Canary Islands">Canary Islands</option>
                           <option value="Cape Verde">Cape Verde</option>
                           <option value="Cayman Islands">Cayman Islands</option>
                           <option value="Central African Republic">Central African Republic</option>
                           <option value="Chad">Chad</option>
                           <option value="Channel Islands">Channel Islands</option>
                           <option value="Chile">Chile</option>
                           <option value="China">China</option>
                           <option value="Christmas Island">Christmas Island</option>
                           <option value="Cocos Island">Cocos Island</option>
                           <option value="Colombia">Colombia</option>
                           <option value="Comoros">Comoros</option>
                           <option value="Congo">Congo</option>
                           <option value="Cook Islands">Cook Islands</option>
                           <option value="Costa Rica">Costa Rica</option>
                           <option value="Cote DIvoire">Cote DIvoire</option>
                           <option value="Croatia">Croatia</option>
                           <option value="Cuba">Cuba</option>
                           <option value="Curaco">Curacao</option>
                           <option value="Cyprus">Cyprus</option>
                           <option value="Czech Republic">Czech Republic</option>
                           <option value="Denmark">Denmark</option>
                           <option value="Djibouti">Djibouti</option>
                           <option value="Dominica">Dominica</option>
                           <option value="Dominican Republic">Dominican Republic</option>
                           <option value="East Timor">East Timor</option>
                           <option value="Ecuador">Ecuador</option>
                           <option value="Egypt">Egypt</option>
                           <option value="El Salvador">El Salvador</option>
                           <option value="Equatorial Guinea">Equatorial Guinea</option>
                           <option value="Eritrea">Eritrea</option>
                           <option value="Estonia">Estonia</option>
                           <option value="Ethiopia">Ethiopia</option>
                           <option value="Falkland Islands">Falkland Islands</option>
                           <option value="Faroe Islands">Faroe Islands</option>
                           <option value="Fiji">Fiji</option>
                           <option value="Finland">Finland</option>
                           <option value="France">France</option>
                           <option value="French Guiana">French Guiana</option>
                           <option value="French Polynesia">French Polynesia</option>
                           <option value="French Southern Ter">French Southern Ter</option>
                           <option value="Gabon">Gabon</option>
                           <option value="Gambia">Gambia</option>
                           <option value="Georgia">Georgia</option>
                           <option value="Germany">Germany</option>
                           <option value="Ghana">Ghana</option>
                           <option value="Gibraltar">Gibraltar</option>
                           <option value="Great Britain">Great Britain</option>
                           <option value="Greece">Greece</option>
                           <option value="Greenland">Greenland</option>
                           <option value="Grenada">Grenada</option>
                           <option value="Guadeloupe">Guadeloupe</option>
                           <option value="Guam">Guam</option>
                           <option value="Guatemala">Guatemala</option>
                           <option value="Guinea">Guinea</option>
                           <option value="Guyana">Guyana</option>
                           <option value="Haiti">Haiti</option>
                           <option value="Hawaii">Hawaii</option>
                           <option value="Honduras">Honduras</option>
                           <option value="Hong Kong">Hong Kong</option>
                           <option value="Hungary">Hungary</option>
                           <option value="Iceland">Iceland</option>
                           <option value="Indonesia">Indonesia</option>
                           <option value="India">India</option>
                           <option value="Iran">Iran</option>
                           <option value="Iraq">Iraq</option>
                           <option value="Ireland">Ireland</option>
                           <option value="Isle of Man">Isle of Man</option>
                           <option value="Israel">Israel</option>
                           <option value="Italy">Italy</option>
                           <option value="Jamaica">Jamaica</option>
                           <option value="Japan">Japan</option>
                           <option value="Jordan">Jordan</option>
                           <option value="Kazakhstan">Kazakhstan</option>
                           <option value="Kenya">Kenya</option>
                           <option value="Kiribati">Kiribati</option>
                           <option value="Korea North">Korea North</option>
                           <option value="Korea Sout">Korea South</option>
                           <option value="Kuwait">Kuwait</option>
                           <option value="Kyrgyzstan">Kyrgyzstan</option>
                           <option value="Laos">Laos</option>
                           <option value="Latvia">Latvia</option>
                           <option value="Lebanon">Lebanon</option>
                           <option value="Lesotho">Lesotho</option>
                           <option value="Liberia">Liberia</option>
                           <option value="Libya">Libya</option>
                           <option value="Liechtenstein">Liechtenstein</option>
                           <option value="Lithuania">Lithuania</option>
                           <option value="Luxembourg">Luxembourg</option>
                           <option value="Macau">Macau</option>
                           <option value="Macedonia">Macedonia</option>
                           <option value="Madagascar">Madagascar</option>
                           <option value="Malaysia">Malaysia</option>
                           <option value="Malawi">Malawi</option>
                           <option value="Maldives">Maldives</option>
                           <option value="Mali">Mali</option>
                           <option value="Malta">Malta</option>
                           <option value="Marshall Islands">Marshall Islands</option>
                           <option value="Martinique">Martinique</option>
                           <option value="Mauritania">Mauritania</option>
                           <option value="Mauritius">Mauritius</option>
                           <option value="Mayotte">Mayotte</option>
                           <option value="Mexico">Mexico</option>
                           <option value="Midway Islands">Midway Islands</option>
                           <option value="Moldova">Moldova</option>
                           <option value="Monaco">Monaco</option>
                           <option value="Mongolia">Mongolia</option>
                           <option value="Montserrat">Montserrat</option>
                           <option value="Morocco">Morocco</option>
                           <option value="Mozambique">Mozambique</option>
                           <option value="Myanmar">Myanmar</option>
                           <option value="Nambia">Nambia</option>
                           <option value="Nauru">Nauru</option>
                           <option value="Nepal">Nepal</option>
                           <option value="Netherland Antilles">Netherland Antilles</option>
                           <option value="Netherlands">Netherlands (Holland, Europe)</option>
                           <option value="Nevis">Nevis</option>
                           <option value="New Caledonia">New Caledonia</option>
                           <option value="New Zealand">New Zealand</option>
                           <option value="Nicaragua">Nicaragua</option>
                           <option value="Niger">Niger</option>
                           <option value="Nigeria">Nigeria</option>
                           <option value="Niue">Niue</option>
                           <option value="Norfolk Island">Norfolk Island</option>
                           <option value="Norway">Norway</option>
                           <option value="Oman">Oman</option>
                           <option value="Pakistan">Pakistan</option>
                           <option value="Palau Island">Palau Island</option>
                           <option value="Palestine">Palestine</option>
                           <option value="Panama">Panama</option>
                           <option value="Papua New Guinea">Papua New Guinea</option>
                           <option value="Paraguay">Paraguay</option>
                           <option value="Peru">Peru</option>
                           <option value="Phillipines">Philippines</option>
                           <option value="Pitcairn Island">Pitcairn Island</option>
                           <option value="Poland">Poland</option>
                           <option value="Portugal">Portugal</option>
                           <option value="Puerto Rico">Puerto Rico</option>
                           <option value="Qatar">Qatar</option>
                           <option value="Republic of Montenegro">Republic of Montenegro</option>
                           <option value="Republic of Serbia">Republic of Serbia</option>
                           <option value="Reunion">Reunion</option>
                           <option value="Romania">Romania</option>
                           <option value="Russia">Russia</option>
                           <option value="Rwanda">Rwanda</option>
                           <option value="St Barthelemy">St Barthelemy</option>
                           <option value="St Eustatius">St Eustatius</option>
                           <option value="St Helena">St Helena</option>
                           <option value="St Kitts-Nevis">St Kitts-Nevis</option>
                           <option value="St Lucia">St Lucia</option>
                           <option value="St Maarten">St Maarten</option>
                           <option value="St Pierre & Miquelon">St Pierre & Miquelon</option>
                           <option value="St Vincent & Grenadines">St Vincent & Grenadines</option>
                           <option value="Saipan">Saipan</option>
                           <option value="Samoa">Samoa</option>
                           <option value="Samoa American">Samoa American</option>
                           <option value="San Marino">San Marino</option>
                           <option value="Sao Tome & Principe">Sao Tome & Principe</option>
                           <option value="Saudi Arabia">Saudi Arabia</option>
                           <option value="Senegal">Senegal</option>
                           <option value="Seychelles">Seychelles</option>
                           <option value="Sierra Leone">Sierra Leone</option>
                           <option value="Singapore">Singapore</option>
                           <option value="Slovakia">Slovakia</option>
                           <option value="Slovenia">Slovenia</option>
                           <option value="Solomon Islands">Solomon Islands</option>
                           <option value="Somalia">Somalia</option>
                           <option value="South Africa">South Africa</option>
                           <option value="Spain">Spain</option>
                           <option value="Sri Lanka">Sri Lanka</option>
                           <option value="Sudan">Sudan</option>
                           <option value="Suriname">Suriname</option>
                           <option value="Swaziland">Swaziland</option>
                           <option value="Sweden">Sweden</option>
                           <option value="Switzerland">Switzerland</option>
                           <option value="Syria">Syria</option>
                           <option value="Tahiti">Tahiti</option>
                           <option value="Taiwan">Taiwan</option>
                           <option value="Tajikistan">Tajikistan</option>
                           <option value="Tanzania">Tanzania</option>
                           <option value="Thailand">Thailand</option>
                           <option value="Togo">Togo</option>
                           <option value="Tokelau">Tokelau</option>
                           <option value="Tonga">Tonga</option>
                           <option value="Trinidad & Tobago">Trinidad & Tobago</option>
                           <option value="Tunisia">Tunisia</option>
                           <option value="Turkey">Turkey</option>
                           <option value="Turkmenistan">Turkmenistan</option>
                           <option value="Turks & Caicos Is">Turks & Caicos Is</option>
                           <option value="Tuvalu">Tuvalu</option>
                           <option value="Uganda">Uganda</option>
                           <option value="United Kingdom">United Kingdom</option>
                           <option value="Ukraine">Ukraine</option>
                           <option value="United Arab Erimates">United Arab Emirates</option>
                           <option value="United States of America">United States of America</option>
                           <option value="Uraguay">Uruguay</option>
                           <option value="Uzbekistan">Uzbekistan</option>
                           <option value="Vanuatu">Vanuatu</option>
                           <option value="Vatican City State">Vatican City State</option>
                           <option value="Venezuela">Venezuela</option>
                           <option value="Vietnam">Vietnam</option>
                           <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                           <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                           <option value="Wake Island">Wake Island</option>
                           <option value="Wallis & Futana Is">Wallis & Futana Is</option>
                           <option value="Yemen">Yemen</option>
                           <option value="Zaire">Zaire</option>
                           <option value="Zambia">Zambia</option>
                           <option value="Zimbabwe">Zimbabwe</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card" id="kk-other-fields-cat">
                <div class="card-header" id="kk-other-fields">
                  <h5 class="mb-0">
                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#kkandothercollapse" aria-expanded="true" aria-controls="kk-other-fields">KK and Other Fields</button>
                  </h5>
                </div>
                <div class="collapse show" id="kkandothercollapse" aria-labelledby="kk-other-fields">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="domain">Domain</label>
                          <input class="form-control" name="domain" id="domain" type="text"  >
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="kk_master_server_name">KK Master Server Name</label>
                          <input class="form-control" name="kk_master_server_name" id="kk_master_server_name" type="text" >
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="website_cleaned">Website Cleaned</label>
                          <select id="website_cleaned" name="website_cleaned" class="form-control">
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                          </select>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="cc_added">CC Added</label>
                          <select id="cc_added" name="cc_added" class="form-control" >
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                          </select>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="added_to_buyer_tool">Added To Buyer Tool</label>
                          <select id="added_to_buyer_tool" name="added_to_buyer_tool" class="form-control" >
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                          </select>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="added_to_adcost">Added To AdCost</label>
                          <select id="added_to_adcost" name="added_to_adcost" class="form-control" >
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                          </select>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="engagement_lifetime_campaign">Engagement Lifetime Campaign</label>
                          <select id="engagement_lifetime_campaign" name="engagement_lifetime_campaign" class="form-control" >
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                          </select>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="engagement_lifetime_campaign_end_date">Engagement Lifetime Campaign End Date</label>
                          <input class="datepicker-here form-control" name="engagement_lifetime_campaign_end_date" id="engagement_lifetime_campaign_end_date" type="text" data-language="en">
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="like_lifetime_campaign">Like Lifetime Campaign</label>
                          <select id="like_lifetime_campaign" name="like_lifetime_campaign" class="form-control" >
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                          </select>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="like_lifetime_campaign_end_date">Like Lifetime Campaign End Date</label>
                          <input class="datepicker-here form-control" name="like_lifetime_campaign_end_date" id="like_lifetime_campaign_end_date" type="text" data-language="en">
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="daily_likes_camp">Daily Likes Camp</label>
                          <select id="daily_likes_camp" name="daily_likes_camp" class="form-control" >
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                          </select>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="domain_verified_in_fb">Domain Verified In FB</label>
                          <select id="domain_verified_in_fb" name="domain_verified_in_fb" class="form-control" >
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                          </select>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="pixel_id">Pixel ID</label>
                          <input class="form-control" name="pixel_id" id="pixel_id" type="text"  >
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card" id="advance-fields-cat">
                <div class="card-header" id="advance-fields">
                  <h5 class="mb-0">
                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#advancecollapse" aria-expanded="true" aria-controls="advance-fields">Advance Fields</button>
                  </h5>
                </div>
                <div class="collapse show" id="advancecollapse" aria-labelledby="advance-fields">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="offer">Offer</label>
                          <select id="offer" name="offer" class="form-control"  >
                            <option value="">Select</option>
                            <option value="AINS">AINS</option>
                            <option value="CLRN">CLRN</option>
                            <option value="NTRA">NTRA</option>
                            <option value="WIFI">WIFI</option>
                          </select>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="media_buyer">Media Buyer</label>
                          <select id="media_buyer" name="media_buyer" class="form-control"  >
                            <option value="">Select</option>
                            <option value="JJ">JJ</option>
                            <option value="ER">ER</option>
                            <option value="MF">MF</option>
                            <option value="RW">RW</option>
                            <option value="SK">SK</option>
                            <option value="PC">PC</option>
                          </select>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="passed_to_media_buyer">Passed To Media Buyer</label>
                          <select id="passed_to_media_buyer" name="passed_to_media_buyer" class="form-control"  >
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                          </select>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="added_to_kk">Added To KK</label>
                          <select id="added_to_kk" name="added_to_kk" class="form-control" >
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                          </select>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="kk_campaign_status">KK Campaign Status</label>
                          <select id="kk_campaign_status" name="kk_campaign_status" class="form-control" >
                            <option value="Block All">Block All</option>
                            <option value="Active">Active</option>
                          </select>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="lpv_camp">LPV Camp</label>
                          <select id="lpv_camp" name="lpv_camp" class="form-control" >
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                          </select>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="wh_camp">WH Camp</label>
                          <select id="wh_camp" name="wh_camp" class="form-control" >
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                          </select>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="bh_camp">BH Camp</label>
                          <select id="bh_camp" name="bh_camp" class="form-control" >
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                          </select>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        <div class="form-group">
                          <label for="long_notes">Long Notes</label>
                          <textarea class="form-control" name="long_notes" id="long_notes" rows="3" placeholder="Long Notes"></textarea>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card" id="dismantle-fields-cat">
                <div class="card-header" id="dismantle-fields">
                  <h5 class="mb-0">
                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#dismantlecollapse" aria-expanded="true" aria-controls="dismantle-fields">Dismantle Fields</button>
                  </h5>
                </div>
                <div class="collapse show" id="dismantlecollapse" aria-labelledby="dismantle-fields">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="terminated_card">Terminated Card</label>
                          <select id="terminated_card" name="terminated_card" class="form-control" >
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                          </select>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="remove_from_kk">Remove From KK</label>
                          <select id="remove_from_kk" name="remove_from_kk" class="form-control" >
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                          </select>
                          <div class="valid-feedback ">Looks good!</div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
           <button class="btn-primary floating-button" type="submit"><i data-feather="save"></i></button>
          {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
<script src="{{asset('assets/js/datepicker/date-picker/datepicker.js')}}"></script>
<script src="{{asset('assets/js/datepicker/date-picker/datepicker.en.js')}}"></script>
<script src="{{asset('assets/js/datepicker/date-picker/datepicker.custom.js')}}"></script>
<script src="{{asset('assets/js/dropzone/dropzone.js')}}"></script>
<script src="{{asset('assets/js/dropzone/dropzone-script.js')}}"></script>
<script src="{{asset('assets/js/typeahead/handlebars.js')}}"></script>
<script src="{{asset('assets/js/typeahead/typeahead.bundle.js')}}"></script>
<script src="{{asset('assets/js/typeahead/typeahead.custom.js')}}"></script>
<script src="{{asset('assets/js/typeahead-search/handlebars.js')}}"></script>
<script src="{{asset('assets/js/typeahead-search/typeahead-custom.js')}}"></script>
<script src="{{asset('assets/js/form-validation-custom.js')}}"></script>
@endsection