@extends('layouts.simple.master',['title' => $editAccounts[0]->build_name])

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/vendors/dropzone.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/vendors/datatables.css')}}">
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
<h3>View FB Account</h3>
@endsection

@section('breadcrumb-items')
<li class="breadcrumb-item"><a href="{{ route('/') }}">FB Accounts</a></li>
<li class="breadcrumb-item active">View Account</li>
@endsection

@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
      <div class="">
        <div class="">
          @include('shared.messages')
          @foreach($editAccounts as $editAccount)
          @if(Sentinel::inRole('superadmin') || Sentinel::inRole('admin'))
            <div class="default-according style-1" id="accordion">
            <div class="card" id="general-fields-cat">
              <button type="button" class="btn btn-link expand-all" style="text-align: right;">Expand/Collapse All</button>
              <div class="card-header" id="general-fields">
                <h5 class="mb-0">
                  <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#generalcollapse" aria-expanded="true" aria-controls="general-fields">General Fields</button>
                </h5>
              </div>
              <div class="collapse show" id="generalcollapse" aria-labelledby="general-fields">
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="account_id">ID</label>
                        <input class="form-control" id="account_id" type="number" min="1" placeholder="ID" name="account_id" value="{{isset($editAccount->account_id) ? $editAccount->account_id : ''}}"  disabled="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-8">
                      <div class="form-group">
                        <label for="build_name">Build Name</label>
                        <input class="form-control" id="build_name" name="build_name" type="Text" value="{{isset($editAccount->build_name) ? $editAccount->build_name : ''}}"  disabled="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label for="next_step">Next Step</label>
                        <input class="form-control" id="next_step" name="next_step" type="Text"  value="{{isset($editAccount->next_step) ? $editAccount->next_step : ''}}"  readonly="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="status">Status</label>
                        <select class="form-control" id="status" name="status"  disabled="">
                          <option value="">Select</option>
                          <option value="Active" {{($editAccount->status == 'Active' ? 'selected' : '')}}>Active</option>
                          <option value="Appealing" {{($editAccount->status == 'Appealing' ? 'selected' : '')}}>Appealing</option>
                          <option value="Building" {{($editAccount->status == 'Building' ? 'selected' : '')}}>Building</option>
                          <option value="Closed" {{($editAccount->status == 'Closed' ? 'selected' : '')}}>Closed</option>
                          <option value="CS Issue" {{($editAccount->status == 'CS Issue' ? 'selected' : '')}}>CS Issue</option>
                          <option value="Dismantle" {{($editAccount->status == 'Dismantle' ? 'selected' : '')}}>Dismantle</option>
                          <option value="ID Flagged" {{($editAccount->status == 'ID Flagged' ? 'selected' : '')}}>ID Flagged</option>
                          <option value="Replaced" {{($editAccount->status == 'Replaced' ? 'selected' : '')}}>Replaced</option>
                          <option value="User Flagged" {{($editAccount->status == 'User Flagged' ? 'selected' : '')}}>User Flagged</option>
                          <option value="WH Warming" {{($editAccount->status == 'WH Warming' ? 'selected' : '')}}>WH Warming</option>
                        </select>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="priority">Priority</label>
                        <select class="form-control" id="priority" name="priority"  disabled="">
                          <option value="">Select</option>
                          <option value="0" {{($editAccount->priority == '0' ? 'selected' : '')}}>0</option>
                          <option value="1" {{($editAccount->priority == '1' ? 'selected' : '')}}>1</option>
                          <option value="2" {{($editAccount->priority == '2' ? 'selected' : '')}}>2</option>
                        </select>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label for="quick_note">Quick Note</label>
                        <textarea class="form-control" name="quick_note" id="quick_note" rows="3" placeholder="Quick Notes" disabled="" >{{isset($editAccount->quick_note) ? $editAccount->quick_note : ''}}</textarea>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label for="accesspoint_notes">Access Point Notes</label>
                        <textarea class="form-control" name="accesspoint_notes" id="accesspoint_notes" rows="3" placeholder="Access Point Notes" disabled="" >{{isset($editAccount->accesspoint_notes) ? $editAccount->accesspoint_notes : ''}}</textarea>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="source">Source</label>
                        <select id="source" name="source" class="form-control"  disabled="">
                          <option value="">Select</option>
                          <option value="DFM" {{($editAccount->source == 'DFM' ? 'selected' : '')}}>DFM</option>
                          <option value="DGS" {{($editAccount->source == 'DGS' ? 'selected' : '')}}>DGS</option>
                          <option value="EDR" {{($editAccount->source == 'EDR' ? 'selected' : '')}}>EDR</option>
                          <option value="FBS" {{($editAccount->source == 'FBS' ? 'selected' : '')}}>FBS</option>
                          <option value="JVN" {{($editAccount->source == 'JVN' ? 'selected' : '')}}>JVN</option>
                          <option value="LVY" {{($editAccount->source == 'LVY' ? 'selected' : '')}}>LVY</option>
                          <option value="MAX" {{($editAccount->source == 'MAX' ? 'selected' : '')}}>MAX</option>
                          <option value="MNR" {{($editAccount->source == 'MNR' ? 'selected' : '')}}>MNR</option>
                          <option value="RYG" {{($editAccount->source == 'RYG' ? 'selected' : '')}}>RYG</option>
                          <option value="SAC" {{($editAccount->source == 'SAC' ? 'selected' : '')}}>SAC</option>
                          <option value="TM8" {{($editAccount->source == 'TM8' ? 'selected' : '')}}>TM8</option>
                          <option value="VBM" {{($editAccount->source == 'VBM' ? 'selected' : '')}}>VBM</option>
                        </select>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="comment_management_date">Comment Management Date</label>
                        <input class="datepicker-here form-control" name="comment_management_date" id="comment_management_date" type="text" value="{{isset($editAccount->comment_management_date) ? $editAccount->comment_management_date : ''}}" data-language="en"  disabled="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="date_of_next_action">Date Of Next Action</label>
                        <input class="datepicker-here form-control" name="date_of_next_action" id="date_of_next_action" type="text" value="{{isset($editAccount->date_of_next_action) ? $editAccount->date_of_next_action : ''}}" data-language="en"  disabled="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col">
                      <label for="timezone">Timezone</label>
                      <select name="timezone" id="timezone" class="form-control" disabled="">
                        <<option value="">Select</option>
                        <option value="-12:00"{{($editAccount->timezone == '-12:00' ? 'selected' : '')}}>(GMT -12:00) Eniwetok, Kwajalein</option>
                        <option value="-11:00"{{($editAccount->timezone == '-11:00' ? 'selected' : '')}}>(GMT -11:00) Midway Island, Samoa</option>
                        <option value="-10:00"{{($editAccount->timezone == '-10:00' ? 'selected' : '')}}>(GMT -10:00) Hawaii</option>
                        <option value="-09:50"{{($editAccount->timezone == '-09:50' ? 'selected' : '')}}>(GMT -9:30) Taiohae</option>
                        <option value="-09:00"{{($editAccount->timezone == '-09:00' ? 'selected' : '')}}>(GMT -9:00) Alaska</option>
                        <option value="-08:00"{{($editAccount->timezone == '-08:00' ? 'selected' : '')}}>(GMT -8:00) Pacific Time (US &amp; Canada)</option>
                        <option value="-07:00"{{($editAccount->timezone == '-07:00' ? 'selected' : '')}}>(GMT -7:00) Mountain Time (US &amp; Canada)</option>
                        <option value="-06:00"{{($editAccount->timezone == '-06:00' ? 'selected' : '')}}>(GMT -6:00) Central Time (US &amp; Canada), Mexico City</option>
                        <option value="-05:00"{{($editAccount->timezone == '-05:00' ? 'selected' : '')}}>(GMT -5:00) Eastern Time (US &amp; Canada), Bogota, Lima</option>
                        <option value="-04:50"{{($editAccount->timezone == '-04:50' ? 'selected' : '')}}>(GMT -4:30) Caracas</option>
                        <option value="-04:00"{{($editAccount->timezone == '-04:00' ? 'selected' : '')}}>(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz</option>
                        <option value="-03:50"{{($editAccount->timezone == '-03:50' ? 'selected' : '')}}>(GMT -3:30) Newfoundland</option>
                        <option value="-03:00"{{($editAccount->timezone == '-03:00' ? 'selected' : '')}}>(GMT -3:00) Brazil, Buenos Aires, Georgetown</option>
                        <option value="-02:00"{{($editAccount->timezone == '-02:00' ? 'selected' : '')}}>(GMT -2:00) Mid-Atlantic</option>
                        <option value="-01:00"{{($editAccount->timezone == '-01:00' ? 'selected' : '')}}>(GMT -1:00) Azores, Cape Verde Islands</option>
                        <option value="+00:00"{{($editAccount->timezone == '+00:00' ? 'selected' : '')}}>(GMT) Western Europe Time, London, Lisbon, Casablanca</option>
                        <option value="+01:00"{{($editAccount->timezone == '+01:00' ? 'selected' : '')}}>(GMT +1:00) Brussels, Copenhagen, Madrid, Paris</option>
                        <option value="+02:00"{{($editAccount->timezone == '+02:00' ? 'selected' : '')}}>(GMT +2:00) Kaliningrad, South Africa</option>
                        <option value="+03:00"{{($editAccount->timezone == '+03:00' ? 'selected' : '')}}>(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg</option>
                        <option value="+03:50"{{($editAccount->timezone == '+03:50' ? 'selected' : '')}}>(GMT +3:30) Tehran</option>
                        <option value="+04:00"{{($editAccount->timezone == '+04:00' ? 'selected' : '')}}>(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi</option>
                        <option value="+04:50"{{($editAccount->timezone == '+04:50' ? 'selected' : '')}}>(GMT +4:30) Kabul</option>
                        <option value="+05:00"{{($editAccount->timezone == '+05:00' ? 'selected' : '')}}>(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent</option>
                        <option value="+05:50"{{($editAccount->timezone == '+05:50' ? 'selected' : '')}}>(GMT +5:30) Bombay, Calcutta, Madras, New Delhi</option>
                        <option value="+05:75"{{($editAccount->timezone == '+05:75' ? 'selected' : '')}}>(GMT +5:45) Kathmandu, Pokhara</option>
                        <option value="+06:00"{{($editAccount->timezone == '+06:00' ? 'selected' : '')}}>(GMT +6:00) Almaty, Dhaka, Colombo</option>
                        <option value="+06:50"{{($editAccount->timezone == '+06:50' ? 'selected' : '')}}>(GMT +6:30) Yangon, Mandalay</option>
                        <option value="+07:00"{{($editAccount->timezone == '+07:00' ? 'selected' : '')}}>(GMT +7:00) Bangkok, Hanoi, Jakarta</option>
                        <option value="+08:00"{{($editAccount->timezone == '+08:00' ? 'selected' : '')}}>(GMT +8:00) Beijing, Perth, Singapore, Hong Kong</option>
                        <option value="+08:75"{{($editAccount->timezone == '+08:75' ? 'selected' : '')}}>(GMT +8:45) Eucla</option>
                        <option value="+09:00"{{($editAccount->timezone == '+09:00' ? 'selected' : '')}}>(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk</option>
                        <option value="+09:50"{{($editAccount->timezone == '+09:50' ? 'selected' : '')}}>(GMT +9:30) Adelaide, Darwin</option>
                        <option value="+10:00"{{($editAccount->timezone == '+10:00' ? 'selected' : '')}}>(GMT +10:00) Eastern Australia, Guam, Vladivostok</option>
                        <option value="+10:50"{{($editAccount->timezone == '+10:50' ? 'selected' : '')}}>(GMT +10:30) Lord Howe Island</option>
                        <option value="+11:00"{{($editAccount->timezone == '+11:00' ? 'selected' : '')}}>(GMT +11:00) Magadan, Solomon Islands, New Caledonia</option>
                        <option value="+11:50"{{($editAccount->timezone == '+11:50' ? 'selected' : '')}}>(GMT +11:30) Norfolk Island</option>
                        <option value="+12:00"{{($editAccount->timezone == '+12:00' ? 'selected' : '')}}>(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka</option>
                        <option value="+12:75"{{($editAccount->timezone == '+12:75' ? 'selected' : '')}}>(GMT +12:45) Chatham Islands</option>
                        <option value="+13:00"{{($editAccount->timezone == '+13:00' ? 'selected' : '')}}>(GMT +13:00) Apia, Nukualofa</option>
                        <option value="+14:00"{{($editAccount->timezone == '+14:00' ? 'selected' : '')}}>(GMT +14:00) Line Islands, Tokelau</option>
                      </select>
                    </div>
                  </div>      
                </div>
              </div>
            </div>
            <div class="card" id="facebook-fields-cat">
              <div class="card-header" id="facebook-fields">
                <h5 class="mb-0">
                  <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#facebookcollapse" aria-expanded="true" aria-controls="facebook-fields">Facebook Fields</button>
                </h5>
              </div>
              <div class="collapse show" id="facebookcollapse" aria-labelledby="facebook-fields">
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="fb_user_full_name">FB User Full Name</label>
                        <input type="text" name="fb_user_full_name" id="fb_user_full_name" class="form-control" value="{{isset($editAccount->fb_user_full_name) ? $editAccount->fb_user_full_name : ''}}"  disabled="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="fb_user_login_info">FB User Login Info</label>
                        <input type="text" name="fb_user_login_info" id="fb_user_login_info" class="form-control" value="{{isset($editAccount->fb_user_login_info) ? $editAccount->fb_user_login_info : ''}}"  readonly ="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="fb_user_login_info">2FA Secret</label>
                          <input type="text" name="twofa_secret" id="twofa_secret" class="form-control" value="{{isset($editAccount->twofa_secret) ? $editAccount->twofa_secret : ''}}" readonly ="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label for="fb_campaign_name">FB Campaign Name</label>
                          <input type="text" name="fb_campaign_name" id="fb_campaign_name" class="form-control" value="{{isset($editAccount->fb_campaign_name) ? $editAccount->fb_campaign_name : ''}}" readonly="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="type_of_ad_account">Type Of Ad Account</label>
                        <select id="type_of_ad_account" name="type_of_ad_account" class="form-control"  disabled="">
                          <option value="">Select</option>
                          <option value="standard" {{($editAccount->type_of_ad_account == 'standard' ? 'selected' : '')}}>Standard</option>
                          <option value="personal" {{($editAccount->type_of_ad_account == 'personal' ? 'selected' : '')}}>Personal</option>
                        </select>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="ad_account_name">Ad Account Name</label>
                        <input class="form-control" name="ad_account_name" id="ad_account_name" type="text" value="{{isset($editAccount->ad_account_name) ? $editAccount->ad_account_name : ''}}"  disabled="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="ad_account_id">Ad Account ID</label>
                        <input class="form-control" name="ad_account_id" id="ad_account_id" type="text" value="{{isset($editAccount->ad_account_id) ? $editAccount->ad_account_id : ''}}"  disabled="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label for="active_ad_url">Active Ad URL</label>
                        <input class="form-control" name="active_ad_url" id="active_ad_url" type="text" value="{{isset($editAccount->active_ad_url) ? $editAccount->active_ad_url : ''}}" readonly="">
                         <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="business_manager_name">Business Manager Name</label>
                        <input class="form-control" name="business_manager_name" id="business_manager_name" type="text" value="{{isset($editAccount->business_manager_name) ? $editAccount->business_manager_name : ''}}"  disabled="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="business_manager_id">Business Manager ID</label>
                        <input class="form-control" name="business_manager_id" id="business_manager_id" type="text" value="{{isset($editAccount->business_manager_id) ? $editAccount->business_manager_id : ''}}"  disabled="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="business_verified">Business Verified</label>
                        <select id="business_verified" name="business_verified" class="form-control"  disabled="">
                          <option value="">Select</option>
                          <option value="Yes" {{($editAccount->business_verified == 'Yes' ? 'selected' : '')}}>Yes</option>
                          <option value="No" {{($editAccount->business_verified == 'No' ? 'selected' : '')}}>No</option>
                          <option value="In Review" {{($editAccount->business_verified == 'In Review' ? 'selected' : '')}}>In Review </option>
                        </select>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="starting_max_spend_of_account">Starting Max Spend Of Account</label>
                        <input class="form-control" name="starting_max_spend_of_account" id="starting_max_spend_of_account" type="number" min="0" value="{{isset($editAccount->starting_max_spend_of_account) ? $editAccount->starting_max_spend_of_account : ''}}"  disabled="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="cost_of_ad_account">Cost Of Ad Account</label>
                        <input class="form-control" name="cost_of_ad_account" id="cost_of_ad_account" type="number" min="0" value="{{isset($editAccount->cost_of_ad_account) ? $editAccount->cost_of_ad_account : ''}}" disabled="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="page_name">Page Name</label>
                        <input class="form-control" name="page_name" id="page_name" type="text" value="{{isset($editAccount->page_name) ? $editAccount->page_name : ''}}"  disabled="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="page_url">Page URL</label>
                        <input class="form-control" name="page_url" id="page_url" type="text" value="{{isset($editAccount->page_url) ? $editAccount->page_url : ''}}"  disabled="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="page_id">Page ID</label>
                        <input class="form-control" name="page_id" id="page_id" type="text" value="{{isset($editAccount->page_id) ? $editAccount->page_id : ''}}"  disabled="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="post_scheduled">Post Scheduled</label>
                        <select id="post_scheduled" name="post_scheduled" class="form-control"  disabled="">
                          <option value="1" {{($editAccount->post_scheduled == '1' ? 'selected' : '')}}>Yes</option>
                          <option value="0" {{($editAccount->post_scheduled == '0' ? 'selected' : '')}}>No</option>
                        </select>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="last_day_post_scheduled">Last Day Post Scheduled</label>
                        <input class="datepicker-here form-control" name="last_day_post_scheduled" id="last_day_post_scheduled" type="text" value="{{isset($editAccount->last_day_post_scheduled) ? $editAccount->last_day_post_scheduled : ''}}" data-language="en" disabled="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col">
                      <label for="geo">Geo</label>
                      <select class="form-control" name="geo" id="geo" disabled="">
                         <option>Select</option>
                         <option value="Afganistan"{{($editAccount->geo == 'Afghanistan' ? 'selected' : '')}}>Afghanistan
                         </option>
                         <option value="Albania"{{($editAccount->geo == 'Albania' ? 'selected' : '')}}>Albania
                         </option>
                         <option value="Algeria" {{($editAccount->geo == 'Algeria' ? 'selected' : '')}}>Algeria
                         </option>
                         <option value="American Samoa" {{($editAccount->geo == 'American Samoa' ? 'selected' : '')}}>American Samoa
                         </option>
                         <option value="Andorra"{{($editAccount->geo == 'Andorra' ? 'selected' : '')}}>Andorra
                         </option>
                         <option value="Angola"{{($editAccount->geo == 'Angola' ? 'selected' : '')}}>Angola
                         </option>
                         <option value="Anguilla"{{($editAccount->geo == 'Anguilla' ? 'selected' : '')}}>Anguilla
                         </option>
                         <option value="Antigua & Barbuda"{{($editAccount->geo == 'Antigua & Barbuda' ? 'selected' : '')}}>Antigua & Barbuda
                         </option>
                         <option value="Argentina"{{($editAccount->geo == 'Argentina' ? 'selected' : '')}}>Argentina
                         </option>
                         <option value="Armenia"{{($editAccount->geo == 'Armenia' ? 'selected' : '')}}>Armenia
                         </option>
                         <option value="Aruba"{{($editAccount->geo == 'Aruba' ? 'selected' : '')}}>Aruba
                         </option>
                         <option value="Australia"{{($editAccount->geo == 'Australia' ? 'selected' : '')}}>Australia
                         </option>
                         <option value="Austria"{{($editAccount->geo == 'Austria' ? 'selected' : '')}}>Austria
                         </option>
                         <option value="Azerbaijan"{{($editAccount->geo == 'Azerbaijan' ? 'selected' : '')}}>Azerbaijan
                         </option>
                         <option value="Bahamas"{{($editAccount->geo == 'Bahamas' ? 'selected' : '')}}>Bahamas
                         </option>
                         <option value="Bahrain"{{($editAccount->geo == 'Bahrain' ? 'selected' : '')}}>Bahrain
                         </option>
                         <option value="Bangladesh"{{($editAccount->geo == 'Bangladesh' ? 'selected' : '')}}>Bangladesh
                         </option>
                         <option value="Barbados"{{($editAccount->geo == 'Barbados' ? 'selected' : '')}}>Barbados
                         </option>
                         <option value="Belarus"{{($editAccount->geo == 'Belarus' ? 'selected' : '')}}>Belarus
                         </option>
                         <option value="Belgium"{{($editAccount->geo == 'Belgium' ? 'selected' : '')}}>Belgium
                         </option>
                         <option value="Belize" {{($editAccount->geo == 'Belize' ? 'selected' : '')}}>Belize
                         </option>
                         <option value="Benin" {{($editAccount->geo == 'Benin' ? 'selected' : '')}}>Benin
                         </option>
                         <option value="Bermuda" {{($editAccount->geo == 'Bermuda' ? 'selected' : '')}}>Bermuda
                         </option>
                         <option value="Bhutan" {{($editAccount->geo == 'Bhutan' ? 'selected' : '')}}>Bhutan
                         </option>
                         <option value="Bolivia" {{($editAccount->geo == 'Bolivia' ? 'selected' : '')}}>Bolivia
                         </option>
                         <option value="Bonaire" {{($editAccount->geo == 'Bonaire' ? 'selected' : '')}}>Bonaire
                         </option>
                         <option value="Bosnia & Herzegovina" {{($editAccount->geo == 'Bosnia & Herzegovina' ? 'selected' : '')}}>Bosnia & Herzegovina
                         </option>
                         <option value="Botswana" {{($editAccount->geo == 'Botswana' ? 'selected' : '')}}>Botswana
                         </option>
                         <option value="Brazil" {{($editAccount->geo == 'Brazil' ? 'selected' : '')}}>Brazil
                         </option>
                         <option value="British Indian Ocean Ter" {{($editAccount->geo == 'British Indian Ocean Ter' ? 'selected' : '')}}>British Indian Ocean Ter
                         </option>
                         <option value="Brunei" {{($editAccount->geo == 'Brunei' ? 'selected' : '')}}>Brunei
                         </option>
                         <option value="Bulgaria" {{($editAccount->geo == 'Bulgaria' ? 'selected' : '')}}>Bulgaria
                         </option>
                         <option value="Burkina Faso" {{($editAccount->geo == 'Burkina Faso' ? 'selected' : '')}}>Burkina Faso
                         </option>
                         <option value="Burundi" {{($editAccount->geo == 'Burundi' ? 'selected' : '')}}>Burundi
                         </option>
                         <option value="Cambodia" {{($editAccount->geo == 'Cambodia' ? 'selected' : '')}}>Cambodia
                         </option>
                         <option value="Cameroon" {{($editAccount->geo == 'Cameroon' ? 'selected' : '')}}>Cameroon
                         </option>
                         <option value="Canada" {{($editAccount->geo == 'Canada' ? 'selected' : '')}}>Canada
                         </option>
                         <option value="Canary Islands" {{($editAccount->geo == 'Canary Islands' ? 'selected' : '')}}>Canary Islands
                         </option>
                         <option value="Cape Verde" {{($editAccount->geo == 'Cape Verde' ? 'selected' : '')}}>Cape Verde
                         </option>
                         <option value="Cayman Islands" {{($editAccount->geo == 'Cayman Islands' ? 'selected' : '')}}>Cayman Islands
                         </option>
                         <option value="Central African Republic" {{($editAccount->geo == 'Central African Republic' ? 'selected' : '')}}>Central African Republic
                         </option>
                         <option value="Chad" {{($editAccount->geo == 'Chad' ? 'selected' : '')}}>Chad
                         </option>
                         <option value="Channel Islands" {{($editAccount->geo == 'Channel Islands' ? 'selected' : '')}}>Channel Islands
                         </option>
                         <option value="Chile" {{($editAccount->geo == 'Chile' ? 'selected' : '')}}>Chile
                         </option>
                         <option value="China" {{($editAccount->geo == 'China' ? 'selected' : '')}}>China
                         </option>
                         <option value="Christmas Island" {{($editAccount->geo == 'Christmas Island' ? 'selected' : '')}}>Christmas Island
                         </option>
                         <option value="Cocos Island" {{($editAccount->geo == 'Cocos Island' ? 'selected' : '')}}>Cocos Island
                         </option>
                         <option value="Colombia" {{($editAccount->geo == 'Colombia' ? 'selected' : '')}}>Colombia
                         </option>
                         <option value="Comoros" {{($editAccount->geo == 'Comoros' ? 'selected' : '')}}>Comoros
                         </option>
                         <option value="Congo" {{($editAccount->geo == 'Congo' ? 'selected' : '')}}>Congo
                         </option>
                         <option value="Cook Islands" {{($editAccount->geo == 'Cook Islands' ? 'selected' : '')}}>Cook Islands
                         </option>
                         <option value="Costa Rica" {{($editAccount->geo == 'Costa Rica' ? 'selected' : '')}}>Costa Rica
                         </option>
                         <option value="Cote DIvoire" {{($editAccount->geo == 'Cote DIvoire' ? 'selected' : '')}}>Cote DIvoire
                         </option>
                         <option value="Croatia" {{($editAccount->geo == 'Croatia' ? 'selected' : '')}}>Croatia
                         </option>
                         <option value="Cuba" {{($editAccount->geo == 'Cuba' ? 'selected' : '')}}>Cuba
                         </option>
                         <option value="Curaco" {{($editAccount->geo == 'Curacao' ? 'selected' : '')}}>Curacao
                         </option>
                         <option value="Cyprus" {{($editAccount->geo == 'Cyprus' ? 'selected' : '')}}>Cyprus
                         </option>
                         <option value="Czech Republic" {{($editAccount->geo == 'Czech Republic' ? 'selected' : '')}}>Czech Republic
                         </option>
                         <option value="Denmark" {{($editAccount->geo == 'Denmark' ? 'selected' : '')}}>Denmark
                         </option>
                         <option value="Djibouti" {{($editAccount->geo == 'Djibouti' ? 'selected' : '')}}>Djibouti
                         </option>
                         <option value="Dominica" {{($editAccount->geo == 'Dominica' ? 'selected' : '')}}>Dominica
                         </option>
                         <option value="Dominican Republic" {{($editAccount->geo == 'Dominican Republic' ? 'selected' : '')}}>Dominican Republic
                         </option>
                         <option value="East Timor" {{($editAccount->geo == 'East Timor' ? 'selected' : '')}}>East Timor
                         </option>
                         <option value="Ecuador" {{($editAccount->geo == 'Ecuador' ? 'selected' : '')}}>Ecuador
                         </option>
                         <option value="Egypt" {{($editAccount->geo == 'Egypt' ? 'selected' : '')}}>Egypt
                         </option>
                         <option value="El Salvador" {{($editAccount->geo == 'El Salvador' ? 'selected' : '')}}>El Salvador
                         </option>
                         <option value="Equatorial Guinea" {{($editAccount->geo == 'Equatorial Guinea' ? 'selected' : '')}}>Equatorial Guinea
                         </option>
                         <option value="Eritrea" {{($editAccount->geo == 'Eritrea' ? 'selected' : '')}}>Eritrea
                         </option>
                         <option value="Estonia" {{($editAccount->geo == 'Estonia' ? 'selected' : '')}}>Estonia
                         </option>
                         <option value="Ethiopia" {{($editAccount->geo == 'Ethiopia' ? 'selected' : '')}}>Ethiopia
                         </option>
                         <option value="Falkland Islands" {{($editAccount->geo == 'Falkland Islands' ? 'selected' : '')}}>Falkland Islands
                         </option>
                         <option value="Faroe Islands" {{($editAccount->geo == 'Faroe Islands' ? 'selected' : '')}}>Faroe Islands
                         </option>
                         <option value="Fiji" {{($editAccount->geo == 'Fiji' ? 'selected' : '')}}>Fiji
                         </option>
                         <option value="Finland" {{($editAccount->geo == 'Finland' ? 'selected' : '')}}>Finland
                         </option>
                         <option value="France" {{($editAccount->geo == 'France' ? 'selected' : '')}}>France
                         </option>
                         <option value="French Guiana" {{($editAccount->geo == 'French Guiana' ? 'selected' : '')}}>French Guiana
                         </option>
                         <option value="French Polynesia" {{($editAccount->geo == 'French Polynesia' ? 'selected' : '')}}>French Polynesia
                         </option>
                         <option value="French Southern Ter" {{($editAccount->geo == 'French Southern Ter' ? 'selected' : '')}}>French Southern Ter
                         </option>
                         <option value="Gabon" {{($editAccount->geo == 'Gabon' ? 'selected' : '')}}>Gabon
                         </option>
                         <option value="Gambia" {{($editAccount->geo == 'Gambia' ? 'selected' : '')}}>Gambia
                         </option>
                         <option value="Georgia" {{($editAccount->geo == 'Georgia' ? 'selected' : '')}}>Georgia
                         </option>
                         <option value="Germany" {{($editAccount->geo == 'Germany' ? 'selected' : '')}}>Germany
                         </option>
                         <option value="Ghana" {{($editAccount->geo == 'Ghana' ? 'selected' : '')}}>Ghana
                         </option>
                         <option value="Gibraltar" {{($editAccount->geo == 'Gibraltar' ? 'selected' : '')}}>Gibraltar
                         </option>
                         <option value="Great Britain" {{($editAccount->geo == 'Great Britain' ? 'selected' : '')}}>Great Britain
                         </option>
                         <option value="Greece" {{($editAccount->geo == 'Greece' ? 'selected' : '')}}>Greece
                         </option>
                         <option value="Greenland" {{($editAccount->geo == 'Greenland' ? 'selected' : '')}}>Greenland
                         </option>
                         <option value="Grenada" {{($editAccount->geo == 'Grenada' ? 'selected' : '')}}>Grenada
                         </option>
                         <option value="Guadeloupe" {{($editAccount->geo == 'Guadeloupe' ? 'selected' : '')}}>Guadeloupe
                         </option>
                         <option value="Guam" {{($editAccount->geo == 'Guam' ? 'selected' : '')}}>Guam
                         </option>
                         <option value="Guatemala" {{($editAccount->geo == 'Guatemala' ? 'selected' : '')}}>Guatemala
                         </option>
                         <option value="Guinea" {{($editAccount->geo == 'Guinea' ? 'selected' : '')}}>Guinea
                         </option>
                         <option value="Guyana" {{($editAccount->geo == 'Guyana' ? 'selected' : '')}}>Guyana
                         </option>
                         <option value="Haiti" {{($editAccount->geo == 'Haiti' ? 'selected' : '')}}>Haiti
                         </option>
                         <option value="Hawaii" {{($editAccount->geo == 'Hawaii' ? 'selected' : '')}}>Hawaii
                         </option>
                         <option value="Honduras" {{($editAccount->geo == 'Honduras' ? 'selected' : '')}}>Honduras
                         </option>
                         <option value="Hong Kong" {{($editAccount->geo == 'Hong Kong' ? 'selected' : '')}}>Hong Kong
                         </option>
                         <option value="Hungary" {{($editAccount->geo == 'Hungary' ? 'selected' : '')}}>Hungary
                         </option>
                         <option value="Iceland" {{($editAccount->geo == 'Iceland' ? 'selected' : '')}}>Iceland
                         </option>
                         <option value="Indonesia" {{($editAccount->geo == 'Indonesia' ? 'selected' : '')}}>Indonesia
                         </option>
                         <option value="India" {{($editAccount->geo == 'India' ? 'selected' : '')}}>India
                         </option>
                         <option value="Iran" {{($editAccount->geo == 'Iran' ? 'selected' : '')}}>Iran
                         </option>
                         <option value="Iraq" {{($editAccount->geo == 'Iraq' ? 'selected' : '')}}>Iraq
                         </option>
                         <option value="Ireland" {{($editAccount->geo == 'Ireland' ? 'selected' : '')}}>Ireland
                         </option>
                         <option value="Isle of Man" {{($editAccount->geo == 'Isle of Man' ? 'selected' : '')}}>Isle of Man
                         </option>
                         <option value="Israel" {{($editAccount->geo == 'Israel' ? 'selected' : '')}}>Israel
                         </option>
                         <option value="Italy" {{($editAccount->geo == 'Italy' ? 'selected' : '')}}>Italy
                         </option>
                         <option value="Jamaica" {{($editAccount->geo == 'Jamaica' ? 'selected' : '')}}>Jamaica
                         </option>
                         <option value="Japan" {{($editAccount->geo == 'Japan' ? 'selected' : '')}}>Japan
                         </option>
                         <option value="Jordan" {{($editAccount->geo == 'Jordan' ? 'selected' : '')}}>Jordan
                         </option>
                         <option value="Kazakhstan" {{($editAccount->geo == 'Kazakhstan' ? 'selected' : '')}}>Kazakhstan
                         </option>
                         <option value="Kenya" {{($editAccount->geo == 'Kenya' ? 'selected' : '')}}>Kenya
                         </option>
                         <option value="Kiribati" {{($editAccount->geo == 'Kiribati' ? 'selected' : '')}}>Kiribati
                         </option>
                         <option value="Korea North" {{($editAccount->geo == 'Korea North' ? 'selected' : '')}}>Korea North
                         </option>
                         <option value="Korea Sout" {{($editAccount->geo == 'Korea South' ? 'selected' : '')}}>Korea South
                         </option>
                         <option value="Kuwait" {{($editAccount->geo == 'Kuwait' ? 'selected' : '')}}>Kuwait
                         </option>
                         <option value="Kyrgyzstan" {{($editAccount->geo == 'Kyrgyzstan' ? 'selected' : '')}}>Kyrgyzstan
                         </option>
                         <option value="Laos" {{($editAccount->geo == 'Laos' ? 'selected' : '')}}>Laos
                         </option>
                         <option value="Latvia" {{($editAccount->geo == 'Latvia' ? 'selected' : '')}}>Latvia
                         </option>
                         <option value="Lebanon" {{($editAccount->geo == 'Lebanon' ? 'selected' : '')}}>Lebanon
                         </option>
                         <option value="Lesotho" {{($editAccount->geo == 'Lesotho' ? 'selected' : '')}}>Lesotho
                         </option>
                         <option value="Liberia" {{($editAccount->geo == 'Liberia' ? 'selected' : '')}}>Liberia
                         </option>
                         <option value="Libya" {{($editAccount->geo == 'Libya' ? 'selected' : '')}}>Libya
                         </option>
                         <option value="Liechtenstein" {{($editAccount->geo == 'Liechtenstein' ? 'selected' : '')}}>Liechtenstein
                         </option>
                         <option value="Lithuania" {{($editAccount->geo == 'Lithuania' ? 'selected' : '')}}>Lithuania
                         </option>
                         <option value="Luxembourg" {{($editAccount->geo == 'Luxembourg' ? 'selected' : '')}}>Luxembourg
                         </option>
                         <option value="Macau" {{($editAccount->geo == 'Macau' ? 'selected' : '')}}>Macau
                         </option>
                         <option value="Macedonia" {{($editAccount->geo == 'Macedonia' ? 'selected' : '')}}>Macedonia
                         </option>
                         <option value="Madagascar" {{($editAccount->geo == 'Madagascar' ? 'selected' : '')}}>Madagascar
                         </option>
                         <option value="Malaysia" {{($editAccount->geo == 'Malaysia' ? 'selected' : '')}}>Malaysia
                         </option>
                         <option value="Malawi" {{($editAccount->geo == 'Malawi' ? 'selected' : '')}}>Malawi
                         </option>
                         <option value="Maldives" {{($editAccount->geo == 'Maldives' ? 'selected' : '')}}>Maldives
                         </option>
                         <option value="Mali" {{($editAccount->geo == 'Mali' ? 'selected' : '')}}>Mali
                         </option>
                         <option value="Malta" {{($editAccount->geo == 'Malta' ? 'selected' : '')}}>Malta
                         </option>
                         <option value="Marshall Islands" {{($editAccount->geo == 'Marshall Islands' ? 'selected' : '')}}>Marshall Islands
                         </option>
                         <option value="Martinique" {{($editAccount->geo == 'Martinique' ? 'selected' : '')}}>Martinique
                         </option>
                         <option value="Mauritania" {{($editAccount->geo == 'Mauritania' ? 'selected' : '')}}>Mauritania
                         </option>
                         <option value="Mauritius" {{($editAccount->geo == 'Mauritius' ? 'selected' : '')}}>Mauritius
                         </option>
                         <option value="Mayotte" {{($editAccount->geo == 'Mayotte' ? 'selected' : '')}}>Mayotte
                         </option>
                         <option value="Mexico" {{($editAccount->geo == 'Mexico' ? 'selected' : '')}}>Mexico
                         </option>
                         <option value="Midway Islands" {{($editAccount->geo == 'Midway Islands' ? 'selected' : '')}}>Midway Islands
                         </option>
                         <option value="Moldova" {{($editAccount->geo == 'Moldova' ? 'selected' : '')}}>Moldova
                         </option>
                         <option value="Monaco" {{($editAccount->geo == 'Monaco' ? 'selected' : '')}}>Monaco
                         </option>
                         <option value="Mongolia" {{($editAccount->geo == 'Mongolia' ? 'selected' : '')}}>Mongolia
                         </option>
                         <option value="Montserrat" {{($editAccount->geo == 'Montserrat' ? 'selected' : '')}}>Montserrat
                         </option>
                         <option value="Morocco" {{($editAccount->geo == 'Morocco' ? 'selected' : '')}}>Morocco
                         </option>
                         <option value="Mozambique" {{($editAccount->geo == 'Mozambique' ? 'selected' : '')}}>Mozambique
                         </option>
                         <option value="Myanmar" {{($editAccount->geo == 'Myanmar' ? 'selected' : '')}}>Myanmar
                         </option>
                         <option value="Nambia" {{($editAccount->geo == 'Nambia' ? 'selected' : '')}}>Nambia
                         </option>
                         <option value="Nauru" {{($editAccount->geo == 'Nauru' ? 'selected' : '')}}>Nauru
                         </option>
                         <option value="Nepal" {{($editAccount->geo == 'Nepal' ? 'selected' : '')}}>Nepal
                         </option>
                         <option value="Netherland Antilles" {{($editAccount->geo == 'Netherland Antilles' ? 'selected' : '')}}>Netherland Antilles
                         </option>
                         <option value="Netherlands" {{($editAccount->geo == 'Netherlands (Holland, Europe)' ? 'selected' : '')}}>Netherlands (Holland, Europe)
                         </option>
                         <option value="Nevis" {{($editAccount->geo == 'Nevis' ? 'selected' : '')}}>Nevis
                         </option>
                         <option value="New Caledonia" {{($editAccount->geo == 'New Caledonia' ? 'selected' : '')}}>New Caledonia
                         </option>
                         <option value="New Zealand" {{($editAccount->geo == 'New Zealand' ? 'selected' : '')}}>New Zealand
                         </option>
                         <option value="Nicaragua" {{($editAccount->geo == 'Nicaragua' ? 'selected' : '')}}>Nicaragua
                         </option>
                         <option value="Niger" {{($editAccount->geo == 'Niger' ? 'selected' : '')}}>Niger
                         </option>
                         <option value="Nigeria" {{($editAccount->geo == 'Nigeria' ? 'selected' : '')}}>Nigeria
                         </option>
                         <option value="Niue" {{($editAccount->geo == 'Niue' ? 'selected' : '')}}>Niue
                         </option>
                         <option value="Norfolk Island" {{($editAccount->geo == 'Norfolk Island' ? 'selected' : '')}}>Norfolk Island
                         </option>
                         <option value="Norway" {{($editAccount->geo == 'Norway' ? 'selected' : '')}}>Norway
                         </option>
                         <option value="Oman" {{($editAccount->geo == 'Oman' ? 'selected' : '')}}>Oman
                         </option>
                         <option value="Pakistan" {{($editAccount->geo == 'Pakistan' ? 'selected' : '')}}>Pakistan
                         </option>
                         <option value="Palau Island" {{($editAccount->geo == 'Palau Island' ? 'selected' : '')}}>Palau Island
                         </option>
                         <option value="Palestine" {{($editAccount->geo == 'Palestine' ? 'selected' : '')}}>Palestine
                         </option>
                         <option value="Panama" {{($editAccount->geo == 'Panama' ? 'selected' : '')}}>Panama
                         </option>
                         <option value="Papua New Guinea" {{($editAccount->geo == 'Papua New Guinea' ? 'selected' : '')}}>Papua New Guinea
                         </option>
                         <option value="Paraguay" {{($editAccount->geo == 'Paraguay' ? 'selected' : '')}}>Paraguay
                         </option>
                         <option value="Peru" {{($editAccount->geo == 'Peru' ? 'selected' : '')}}>Peru
                         </option>
                         <option value="Phillipines" {{($editAccount->geo == 'Philippines' ? 'selected' : '')}}>Philippines
                         </option>
                         <option value="Pitcairn Island" {{($editAccount->geo == 'Pitcairn Island' ? 'selected' : '')}}>Pitcairn Island
                         </option>
                         <option value="Poland" {{($editAccount->geo == 'Poland' ? 'selected' : '')}}>Poland
                         </option>
                         <option value="Portugal" {{($editAccount->geo == 'Portugal' ? 'selected' : '')}}>Portugal
                         </option>
                         <option value="Puerto Rico" {{($editAccount->geo == 'Puerto Rico' ? 'selected' : '')}}>Puerto Rico
                         </option>
                         <option value="Qatar" {{($editAccount->geo == 'Qatar' ? 'selected' : '')}}>Qatar
                         </option>
                         <option value="Republic of Montenegro" {{($editAccount->geo == 'Republic of Montenegro' ? 'selected' : '')}}>Republic of Montenegro
                         </option>
                         <option value="Republic of Serbia" {{($editAccount->geo == 'Republic of Serbia' ? 'selected' : '')}}>Republic of Serbia
                         </option>
                         <option value="Reunion" {{($editAccount->geo == 'Reunion' ? 'selected' : '')}}>Reunion
                         </option>
                         <option value="Romania" {{($editAccount->geo == 'Romania' ? 'selected' : '')}}>Romania
                         </option>
                         <option value="Russia" {{($editAccount->geo == 'Russia' ? 'selected' : '')}}>Russia
                         </option>
                         <option value="Rwanda" {{($editAccount->geo == 'Rwanda' ? 'selected' : '')}}>Rwanda
                         </option>
                         <option value="St Barthelemy" {{($editAccount->geo == 'St Barthelemy' ? 'selected' : '')}}>St Barthelemy
                         </option>
                         <option value="St Eustatius" {{($editAccount->geo == 'St Eustatius' ? 'selected' : '')}}>St Eustatius
                         </option>
                         <option value="St Helena" {{($editAccount->geo == 'St Helena' ? 'selected' : '')}}>St Helena
                         </option>
                         <option value="St Kitts-Nevis" {{($editAccount->geo == 'St Kitts-Nevis' ? 'selected' : '')}}>St Kitts-Nevis
                         </option>
                         <option value="St Lucia" {{($editAccount->geo == 'St Lucia' ? 'selected' : '')}}>St Lucia
                         </option>
                         <option value="St Maarten" {{($editAccount->geo == 'St Maarten' ? 'selected' : '')}}>St Maarten
                         </option>
                         <option value="St Pierre & Miquelon" {{($editAccount->geo == 'St Pierre & Miquelon' ? 'selected' : '')}}>St Pierre & Miquelon
                         </option>
                         <option value="St Vincent & Grenadines" {{($editAccount->geo == 'St Vincent & Grenadines' ? 'selected' : '')}}>St Vincent & Grenadines
                         </option>
                         <option value="Saipan" {{($editAccount->geo == 'Saipan' ? 'selected' : '')}}>Saipan
                         </option>
                         <option value="Samoa" {{($editAccount->geo == 'Samoa' ? 'selected' : '')}}>Samoa
                         </option>
                         <option value="Samoa American" {{($editAccount->geo == 'Samoa American' ? 'selected' : '')}}>Samoa American
                         </option>
                         <option value="San Marino" {{($editAccount->geo == 'San Marino' ? 'selected' : '')}}>San Marino
                         </option>
                         <option value="Sao Tome & Principe" {{($editAccount->geo == 'Sao Tome & Principe' ? 'selected' : '')}}>Sao Tome & Principe
                         </option>
                         <option value="Saudi Arabia" {{($editAccount->geo == 'Saudi Arabia' ? 'selected' : '')}}>Saudi Arabia
                         </option>
                         <option value="Senegal" {{($editAccount->geo == 'Senegal' ? 'selected' : '')}}>Senegal
                         </option>
                         <option value="Seychelles" {{($editAccount->geo == 'Seychelles' ? 'selected' : '')}}>Seychelles
                         </option>
                         <option value="Sierra Leone" {{($editAccount->geo == 'Sierra Leone' ? 'selected' : '')}}>Sierra Leone
                         </option>
                         <option value="Singapore" {{($editAccount->geo == 'Singapore' ? 'selected' : '')}}>Singapore
                         </option>
                         <option value="Slovakia" {{($editAccount->geo == 'Slovakia' ? 'selected' : '')}}>Slovakia
                         </option>
                         <option value="Slovenia" {{($editAccount->geo == 'Slovenia' ? 'selected' : '')}}>Slovenia
                         </option>
                         <option value="Solomon Islands" {{($editAccount->geo == 'Solomon Islands' ? 'selected' : '')}}>Solomon Islands
                         </option>
                         <option value="Somalia" {{($editAccount->geo == 'Somalia' ? 'selected' : '')}}>Somalia
                         </option>
                         <option value="South Africa" {{($editAccount->geo == 'South Africa' ? 'selected' : '')}}>South Africa
                         </option>
                         <option value="Spain" {{($editAccount->geo == 'Spain' ? 'selected' : '')}}>Spain
                         </option>
                         <option value="Sri Lanka" {{($editAccount->geo == 'Sri Lanka' ? 'selected' : '')}}>Sri Lanka
                         </option>
                         <option value="Sudan" {{($editAccount->geo == 'Sudan' ? 'selected' : '')}}>Sudan
                         </option>
                         <option value="Suriname" {{($editAccount->geo == 'Suriname' ? 'selected' : '')}}>Suriname
                         </option>
                         <option value="Swaziland" {{($editAccount->geo == 'Swaziland' ? 'selected' : '')}}>Swaziland
                         </option>
                         <option value="Sweden" {{($editAccount->geo == 'Sweden' ? 'selected' : '')}}>Sweden
                         </option>
                         <option value="Switzerland" {{($editAccount->geo == 'Switzerland' ? 'selected' : '')}}>Switzerland
                         </option>
                         <option value="Syria" {{($editAccount->geo == 'Syria' ? 'selected' : '')}}>Syria
                         </option>
                         <option value="Tahiti" {{($editAccount->geo == 'Tahiti' ? 'selected' : '')}}>Tahiti
                         </option>
                         <option value="Taiwan" {{($editAccount->geo == 'Taiwan' ? 'selected' : '')}}>Taiwan
                         </option>
                         <option value="Tajikistan" {{($editAccount->geo == 'Tajikistan' ? 'selected' : '')}}>Tajikistan
                         </option>
                         <option value="Tanzania" {{($editAccount->geo == 'Tanzania' ? 'selected' : '')}}>Tanzania
                         </option>
                         <option value="Thailand" {{($editAccount->geo == 'Thailand' ? 'selected' : '')}}>Thailand
                         </option>
                         <option value="Togo" {{($editAccount->geo == 'Togo' ? 'selected' : '')}}>Togo
                         </option>
                         <option value="Tokelau" {{($editAccount->geo == 'Tokelau' ? 'selected' : '')}}>Tokelau
                         </option>
                         <option value="Tonga" {{($editAccount->geo == 'Tonga' ? 'selected' : '')}}>Tonga
                         </option>
                         <option value="Trinidad & Tobago" {{($editAccount->geo == 'Trinidad & Tobago' ? 'selected' : '')}}>Trinidad & Tobago
                         </option>
                         <option value="Tunisia" {{($editAccount->geo == 'Tunisia' ? 'selected' : '')}}>Tunisia
                         </option>
                         <option value="Turkey" {{($editAccount->geo == 'Turkey' ? 'selected' : '')}}>Turkey
                         </option>
                         <option value="Turkmenistan" {{($editAccount->geo == 'Turkmenistan' ? 'selected' : '')}}>Turkmenistan
                         </option>
                         <option value="Turks & Caicos Is" {{($editAccount->geo == 'Turks & Caicos Is' ? 'selected' : '')}}>Turks & Caicos Is
                         </option>
                         <option value="Tuvalu" {{($editAccount->geo == 'Tuvalu' ? 'selected' : '')}}>Tuvalu
                         </option>
                         <option value="Uganda" {{($editAccount->geo == 'Uganda' ? 'selected' : '')}}>Uganda
                         </option>
                         <option value="United Kingdom" {{($editAccount->geo == 'United Kingdom' ? 'selected' : '')}}>United Kingdom
                         </option>
                         <option value="Ukraine" {{($editAccount->geo == 'Ukraine' ? 'selected' : '')}}>Ukraine
                         </option>
                         <option value="United Arab Erimates" {{($editAccount->geo == 'United Arab Emirates' ? 'selected' : '')}}>United Arab Emirates
                         </option>
                         <option value="United States of America" {{($editAccount->geo == 'United States of America' ? 'selected' : '')}}>United States of America
                         </option>
                         <option value="Uraguay" {{($editAccount->geo == 'Uruguay' ? 'selected' : '')}}>Uruguay
                         </option>
                         <option value="Uzbekistan" {{($editAccount->geo == 'Uzbekistan' ? 'selected' : '')}}>Uzbekistan
                         </option>
                         <option value="Vanuatu" {{($editAccount->geo == 'Vanuatu' ? 'selected' : '')}}>Vanuatu
                         </option>
                         <option value="Vatican City State" {{($editAccount->geo == 'Vatican City State' ? 'selected' : '')}}>Vatican City State
                         </option>
                         <option value="Venezuela" {{($editAccount->geo == 'Venezuela' ? 'selected' : '')}}>Venezuela
                         </option>
                         <option value="Vietnam" {{($editAccount->geo == 'Vietnam' ? 'selected' : '')}}>Vietnam
                         </option>
                         <option value="Virgin Islands (Brit)" {{($editAccount->geo == 'Virgin Islands (Brit)' ? 'selected' : '')}}>Virgin Islands (Brit)
                         </option>
                         <option value="Virgin Islands (USA)" {{($editAccount->geo == 'Virgin Islands (USA)' ? 'selected' : '')}}>Virgin Islands (USA)
                         </option>
                         <option value="Wake Island" {{($editAccount->geo == 'Wake Island' ? 'selected' : '')}}>Wake Island
                         </option>
                         <option value="Wallis & Futana Is" {{($editAccount->geo == 'Wallis & Futana Is' ? 'selected' : '')}}>Wallis & Futana Is
                         </option>
                         <option value="Yemen" {{($editAccount->geo == 'Yemen' ? 'selected' : '')}}>Yemen
                         </option>
                         <option value="Zaire" {{($editAccount->geo == 'Zaire' ? 'selected' : '')}}>Zaire
                         </option>
                         <option value="Zambia" {{($editAccount->geo == 'Zambia' ? 'selected' : '')}}>Zambia
                         </option>
                         <option value="Zimbabwe" {{($editAccount->geo == 'Zimbabwe' ? 'selected' : '')}}>Zimbabwe
                         </option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card" id="kk-other-fields-cat">
              <div class="card-header" id="kk-other-fields">
                <h5 class="mb-0">
                  <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#kkandothercollapse" aria-expanded="true" aria-controls="kk-other-fields">KK and Other Fields</button>
                </h5>
              </div>
              <div class="collapse show" id="kkandothercollapse" aria-labelledby="kk-other-fields">
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="domain">Domain</label>
                        <input class="form-control" name="domain" id="domain" type="text" value="{{isset($editAccount->domain) ? $editAccount->domain : ''}}"  disabled="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="kk_master_server_name">KK Master Server Name</label>
                        <input class="form-control" name="kk_master_server_name" id="kk_master_server_name" type="text" value="{{isset($editAccount->kk_master_server_name) ? $editAccount->kk_master_server_name : ''}}"  disabled="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="website_cleaned">Website Cleaned</label>
                        <select id="website_cleaned" name="website_cleaned" class="form-control" disabled="">
                          <option value="1" {{($editAccount->website_cleaned == '1' ? 'selected' : '')}}>Yes</option>
                          <option value="0" {{($editAccount->website_cleaned == '0' ? 'selected' : '')}}>No</option>
                        </select>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="cc_added">CC Added</label>
                        <select id="cc_added" name="cc_added" class="form-control"  disabled="">
                          <option value="1" {{($editAccount->cc_added == '1' ? 'selected' : '')}}>Yes</option>
                          <option value="0" {{($editAccount->cc_added == '0' ? 'selected' : '')}}>No</option>
                        </select>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="added_to_buyer_tool">Added To Buyer Tool</label>
                        <select id="added_to_buyer_tool" name="added_to_buyer_tool" class="form-control"  disabled="">
                          <option value="1" {{($editAccount->added_to_buyer_tool == '1' ? 'selected' : '')}}>Yes</option>
                          <option value="0" {{($editAccount->added_to_buyer_tool == '0' ? 'selected' : '')}}>No</option>
                        </select>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="added_to_adcost">Added To AdCost</label>
                        <select id="added_to_adcost" name="added_to_adcost" class="form-control"  disabled="">
                          <option value="1" {{($editAccount->added_to_adcost == '1' ? 'selected' : '')}}>Yes</option>
                          <option value="0" {{($editAccount->added_to_adcost == '0' ? 'selected' : '')}}>No</option>
                        </select>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="engagement_lifetime_campaign">Engagement Lifetime Campaign</label>
                        <select id="engagement_lifetime_campaign" name="engagement_lifetime_campaign" class="form-control"  disabled="">
                          <option value="1" {{($editAccount->engagement_lifetime_campaign == '1' ? 'selected' : '')}}>Yes</option>
                          <option value="0" {{($editAccount->engagement_lifetime_campaign == '0' ? 'selected' : '')}}>No</option>
                        </select>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="engagement_lifetime_campaign_end_date">Engagement Lifetime Campaign End Date</label>
                        <input class="datepicker-here form-control" name="engagement_lifetime_campaign_end_date" id="engagement_lifetime_campaign_end_date" type="text" value="{{isset($editAccount->engagement_lifetime_campaign_end_date) ? $editAccount->engagement_lifetime_campaign_end_date : ''}}" data-language="en" disabled="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="like_lifetime_campaign">Like Lifetime Campaign</label>
                        <select id="like_lifetime_campaign" name="like_lifetime_campaign" class="form-control"  disabled="">
                          <option value="1" {{($editAccount->like_lifetime_campaign == '1' ? 'selected' : '')}}>Yes</option>
                          <option value="0" {{($editAccount->like_lifetime_campaign == '0' ? 'selected' : '')}}>No</option>
                        </select>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="like_lifetime_campaign_end_date">Like Lifetime Campaign End Date</label>
                        <input class="datepicker-here form-control" name="like_lifetime_campaign_end_date" id="like_lifetime_campaign_end_date" type="text" value="{{isset($editAccount->like_lifetime_campaign_end_date) ? $editAccount->like_lifetime_campaign_end_date : ''}}" data-language="en" disabled="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="daily_likes_camp">Daily Likes Camp</label>
                        <select id="daily_likes_camp" name="daily_likes_camp" class="form-control"  disabled="">
                          <option value="1" {{($editAccount->daily_likes_camp == '1' ? 'selected' : '')}}>Yes</option>
                          <option value="0" {{($editAccount->daily_likes_camp == '0' ? 'selected' : '')}}>No</option>
                        </select>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="domain_verified_in_fb">Domain Verified In FB</label>
                        <select id="domain_verified_in_fb" name="domain_verified_in_fb" class="form-control"  disabled="">
                          <option value="1" {{($editAccount->domain_verified_in_fb == '1' ? 'selected' : '')}}>Yes</option>
                          <option value="0" {{($editAccount->domain_verified_in_fb == '0' ? 'selected' : '')}}>No</option>
                        </select>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="pixel_id">Pixel ID</label>
                        <input class="form-control" name="pixel_id" id="pixel_id" type="text" value="{{isset($editAccount->pixel_id) ? $editAccount->pixel_id : ''}}"  disabled="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card" id="advance-fields-cat">
              <div class="card-header" id="advance-fields">
                <h5 class="mb-0">
                  <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#advancecollapse" aria-expanded="true" aria-controls="advance-fields">Advance Fields</button>
                </h5>
              </div>
              <div class="collapse show" id="advancecollapse" aria-labelledby="advance-fields">
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="offer">Offer</label>
                        <select id="offer" name="offer" class="form-control"  disabled="">
                          <option value="">Select</option>
                          <option value="AINS" {{($editAccount->offer == 'AINS' ? 'selected' : '')}}>AINS</option>
                          <option value="CLRN" {{($editAccount->offer == 'CLRN' ? 'selected' : '')}}>CLRN</option>
                          <option value="NTRA" {{($editAccount->offer == 'NTRA' ? 'selected' : '')}}>NTRA</option>
                          <option value="WIFI" {{($editAccount->offer == 'WIFI' ? 'selected' : '')}}>WIFI</option>
                        </select>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="media_buyer">Media Buyer</label>
                        <select id="media_buyer" name="media_buyer" class="form-control"  disabled="">
                          <option value="">Select</option>
                          <option value="JJ" {{($editAccount->media_buyer == 'JJ' ? 'selected' : '')}}>JJ</option>
                          <option value="ER" {{($editAccount->media_buyer == 'ER' ? 'selected' : '')}}>ER</option>
                          <option value="MF" {{($editAccount->media_buyer == 'MF' ? 'selected' : '')}}>MF</option>
                          <option value="RW" {{($editAccount->media_buyer == 'RW' ? 'selected' : '')}}>RW</option>
                          <option value="SK" {{($editAccount->media_buyer == 'SK' ? 'selected' : '')}}>SK</option>
                          <option value="PC" {{($editAccount->media_buyer == 'PC' ? 'selected' : '')}}>PC</option>
                        </select>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="passed_to_media_buyer">Passed To Media Buyer</label>
                        <select id="passed_to_media_buyer" name="passed_to_media_buyer" class="form-control"  disabled="">
                          <option value="1" {{($editAccount->passed_to_media_buyer == '1' ? 'selected' : '')}}>Yes</option>
                          <option value="0" {{($editAccount->passed_to_media_buyer == '0' ? 'selected' : '')}}>No</option>
                        </select>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="added_to_kk">Added To KK</label>
                        <select id="added_to_kk" name="added_to_kk" class="form-control"  disabled="">
                          <option value="1" {{($editAccount->added_to_kk == '1' ? 'selected' : '')}}>Yes</option>
                          <option value="0" {{($editAccount->added_to_kk == '0' ? 'selected' : '')}}>No</option>
                        </select>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="kk_campaign_status">KK Campaign Status</label>
                        <select id="kk_campaign_status" name="kk_campaign_status" class="form-control"  disabled="">
                          <option value="Block All" {{($editAccount->kk_campaign_status == 'Block All' ? 'selected' : '')}}>Block All</option>
                          <option value="Active" {{($editAccount->kk_campaign_status == 'Active' ? 'selected' : '')}}>Active</option>
                        </select>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label for="kk_account_name">KK Account Name</label>
                        <input class="form-control" id="kk_account_name" name="kk_account_name" type="Text" value="{{isset($editAccount->kk_account_name) ? $editAccount->kk_account_name : ''}}"  disabled="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label for="kk_campaign_title">KK Campaign Title</label> 
                        <input class="form-control" id="kk_campaign_title" name="kk_campaign_title" type="Text" value="{{isset($editAccount->kk_campaign_title) ? $editAccount->kk_campaign_title : ''}}"  readonly="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label for="kk_campaign_url">KK Campaign URL</label>
                        <input class="form-control" id="kk_campaign_url" name="kk_campaign_url" type="Text"  value="{{isset($editAccount->kk_campaign_url) ? $editAccount->kk_campaign_url : ''}}"  readonly="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label for="url_tracking_string">URL Tracking String</label>
                        <input class="form-control" id="url_tracking_string" name="url_tracking_string" type="Text" value="{{isset($editAccount->url_tracking_string) ? $editAccount->url_tracking_string : ''}}"  readonly="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="lpv_camp">LPV Camp</label>
                        <select id="lpv_camp" name="lpv_camp" class="form-control"  disabled="">
                          <option value="1" {{($editAccount->lpv_camp == '1' ? 'selected' : '')}}>Yes</option>
                          <option value="0" {{($editAccount->lpv_camp == '0' ? 'selected' : '')}}>No</option>
                        </select>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="wh_camp">WH Camp</label>
                        <select id="wh_camp" name="wh_camp" class="form-control"  disabled="">
                          <option value="1" {{($editAccount->wh_camp == '1' ? 'selected' : '')}}>Yes</option>
                          <option value="0" {{($editAccount->wh_camp == '0' ? 'selected' : '')}}>No</option>
                        </select>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="bh_camp">BH Camp</label>
                        <select id="bh_camp" name="bh_camp" class="form-control"  disabled="">
                          <option value="1" {{($editAccount->bh_camp == '1' ? 'selected' : '')}}>Yes</option>
                          <option value="0" {{($editAccount->bh_camp == '0' ? 'selected' : '')}}>No</option>
                        </select>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label for="long_notes">Long Notes</label>
                        <textarea class="form-control" name="long_notes" id="long_notes" rows="3" placeholder="Long Notes" disabled="">{{isset($editAccount->long_notes) ? $editAccount->long_notes : ''}}</textarea>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card" id="dismantle-fields-cat">
              <div class="card-header" id="dismantle-fields">
                <h5 class="mb-0">
                  <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#dismantlecollapse" aria-expanded="true" aria-controls="dismantle-fields">Dismantle Fields</button>
                </h5>
              </div>
              <div class="collapse show" id="dismantlecollapse" aria-labelledby="dismantle-fields">
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="terminated_card">Terminated Card</label>
                        <select id="terminated_card" name="terminated_card" class="form-control"  disabled="">
                          <option value="1" {{($editAccount->terminated_card == '1' ? 'selected' : '')}}>Yes</option>
                          <option value="0" {{($editAccount->terminated_card == '0' ? 'selected' : '')}}>No</option>
                        </select>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="remove_from_kk">Remove From KK</label>
                        <select id="remove_from_kk" name="remove_from_kk" class="form-control"  disabled="">
                          <option value="1" {{($editAccount->remove_from_kk == '1' ? 'selected' : '')}}>Yes</option>
                          <option value="0" {{($editAccount->remove_from_kk == '0' ? 'selected' : '')}}>No</option>
                        </select>
                        <div class="valid-feedback ">Looks good!</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </div>
          @elseif(Sentinel::inRole('manager'))
            <div class="default-according style-1" id="accordion">
              <div class="card" id="general-fields-cat">
                <button type="button" class="btn btn-link expand-all" style="text-align: right;">Expand/Collapse All</button>
                <div class="card-header" id="general-fields">
                  <h5 class="mb-0">
                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#generalcollapse" aria-expanded="true" aria-controls="general-fields">General Fields</button>
                  </h5>
                </div>
                <div class="collapse show" id="generalcollapse" aria-labelledby="general-fields">
                  <div class="card-body">
                    <div class="row">
                      <div class="col">
                        <div class="form-group">
                          <label for="accesspoint_notes">Access Point Notes</label>
                          <textarea class="form-control" name="accesspoint_notes" id="accesspoint_notes" rows="3" placeholder="Access Point Notes" disabled="" >{{isset($editAccount->accesspoint_notes) ? $editAccount->accesspoint_notes : ''}}</textarea>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="source">Source</label>
                          <select id="source" name="source" class="form-control"  disabled="">
                            <option value="">Select</option>
                            <option value="DFM" {{($editAccount->source == 'DFM' ? 'selected' : '')}}>DFM</option>
                            <option value="DGS" {{($editAccount->source == 'DGS' ? 'selected' : '')}}>DGS</option>
                            <option value="EDR" {{($editAccount->source == 'EDR' ? 'selected' : '')}}>EDR</option>
                            <option value="FBS" {{($editAccount->source == 'FBS' ? 'selected' : '')}}>FBS</option>
                            <option value="JVN" {{($editAccount->source == 'JVN' ? 'selected' : '')}}>JVN</option>
                            <option value="LVY" {{($editAccount->source == 'LVY' ? 'selected' : '')}}>LVY</option>
                            <option value="MAX" {{($editAccount->source == 'MAX' ? 'selected' : '')}}>MAX</option>
                            <option value="MNR" {{($editAccount->source == 'MNR' ? 'selected' : '')}}>MNR</option>
                            <option value="RYG" {{($editAccount->source == 'RYG' ? 'selected' : '')}}>RYG</option>
                            <option value="SAC" {{($editAccount->source == 'SAC' ? 'selected' : '')}}>SAC</option>
                            <option value="VBM" {{($editAccount->source == 'VBM' ? 'selected' : '')}}>VBM</option>
                          </select>
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-8">
                        <label for="timezone">Timezone</label>
                        <select name="timezone" id="timezone" class="form-control" disabled="">
                          <<option value="">Select</option>
                          <option value="-12:00"{{($editAccount->timezone == '-12:00' ? 'selected' : '')}}>(GMT -12:00) Eniwetok, Kwajalein</option>
                          <option value="-11:00"{{($editAccount->timezone == '-11:00' ? 'selected' : '')}}>(GMT -11:00) Midway Island, Samoa</option>
                          <option value="-10:00"{{($editAccount->timezone == '-10:00' ? 'selected' : '')}}>(GMT -10:00) Hawaii</option>
                          <option value="-09:50"{{($editAccount->timezone == '-09:50' ? 'selected' : '')}}>(GMT -9:30) Taiohae</option>
                          <option value="-09:00"{{($editAccount->timezone == '-09:00' ? 'selected' : '')}}>(GMT -9:00) Alaska</option>
                          <option value="-08:00"{{($editAccount->timezone == '-08:00' ? 'selected' : '')}}>(GMT -8:00) Pacific Time (US &amp; Canada)</option>
                          <option value="-07:00"{{($editAccount->timezone == '-07:00' ? 'selected' : '')}}>(GMT -7:00) Mountain Time (US &amp; Canada)</option>
                          <option value="-06:00"{{($editAccount->timezone == '-06:00' ? 'selected' : '')}}>(GMT -6:00) Central Time (US &amp; Canada), Mexico City</option>
                          <option value="-05:00"{{($editAccount->timezone == '-05:00' ? 'selected' : '')}}>(GMT -5:00) Eastern Time (US &amp; Canada), Bogota, Lima</option>
                          <option value="-04:50"{{($editAccount->timezone == '-04:50' ? 'selected' : '')}}>(GMT -4:30) Caracas</option>
                          <option value="-04:00"{{($editAccount->timezone == '-04:00' ? 'selected' : '')}}>(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz</option>
                          <option value="-03:50"{{($editAccount->timezone == '-03:50' ? 'selected' : '')}}>(GMT -3:30) Newfoundland</option>
                          <option value="-03:00"{{($editAccount->timezone == '-03:00' ? 'selected' : '')}}>(GMT -3:00) Brazil, Buenos Aires, Georgetown</option>
                          <option value="-02:00"{{($editAccount->timezone == '-02:00' ? 'selected' : '')}}>(GMT -2:00) Mid-Atlantic</option>
                          <option value="-01:00"{{($editAccount->timezone == '-01:00' ? 'selected' : '')}}>(GMT -1:00) Azores, Cape Verde Islands</option>
                          <option value="+00:00"{{($editAccount->timezone == '+00:00' ? 'selected' : '')}}>(GMT) Western Europe Time, London, Lisbon, Casablanca</option>
                          <option value="+01:00"{{($editAccount->timezone == '+01:00' ? 'selected' : '')}}>(GMT +1:00) Brussels, Copenhagen, Madrid, Paris</option>
                          <option value="+02:00"{{($editAccount->timezone == '+02:00' ? 'selected' : '')}}>(GMT +2:00) Kaliningrad, South Africa</option>
                          <option value="+03:00"{{($editAccount->timezone == '+03:00' ? 'selected' : '')}}>(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg</option>
                          <option value="+03:50"{{($editAccount->timezone == '+03:50' ? 'selected' : '')}}>(GMT +3:30) Tehran</option>
                          <option value="+04:00"{{($editAccount->timezone == '+04:00' ? 'selected' : '')}}>(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi</option>
                          <option value="+04:50"{{($editAccount->timezone == '+04:50' ? 'selected' : '')}}>(GMT +4:30) Kabul</option>
                          <option value="+05:00"{{($editAccount->timezone == '+05:00' ? 'selected' : '')}}>(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent</option>
                          <option value="+05:50"{{($editAccount->timezone == '+05:50' ? 'selected' : '')}}>(GMT +5:30) Bombay, Calcutta, Madras, New Delhi</option>
                          <option value="+05:75"{{($editAccount->timezone == '+05:75' ? 'selected' : '')}}>(GMT +5:45) Kathmandu, Pokhara</option>
                          <option value="+06:00"{{($editAccount->timezone == '+06:00' ? 'selected' : '')}}>(GMT +6:00) Almaty, Dhaka, Colombo</option>
                          <option value="+06:50"{{($editAccount->timezone == '+06:50' ? 'selected' : '')}}>(GMT +6:30) Yangon, Mandalay</option>
                          <option value="+07:00"{{($editAccount->timezone == '+07:00' ? 'selected' : '')}}>(GMT +7:00) Bangkok, Hanoi, Jakarta</option>
                          <option value="+08:00"{{($editAccount->timezone == '+08:00' ? 'selected' : '')}}>(GMT +8:00) Beijing, Perth, Singapore, Hong Kong</option>
                          <option value="+08:75"{{($editAccount->timezone == '+08:75' ? 'selected' : '')}}>(GMT +8:45) Eucla</option>
                          <option value="+09:00"{{($editAccount->timezone == '+09:00' ? 'selected' : '')}}>(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk</option>
                          <option value="+09:50"{{($editAccount->timezone == '+09:50' ? 'selected' : '')}}>(GMT +9:30) Adelaide, Darwin</option>
                          <option value="+10:00"{{($editAccount->timezone == '+10:00' ? 'selected' : '')}}>(GMT +10:00) Eastern Australia, Guam, Vladivostok</option>
                          <option value="+10:50"{{($editAccount->timezone == '+10:50' ? 'selected' : '')}}>(GMT +10:30) Lord Howe Island</option>
                          <option value="+11:00"{{($editAccount->timezone == '+11:00' ? 'selected' : '')}}>(GMT +11:00) Magadan, Solomon Islands, New Caledonia</option>
                          <option value="+11:50"{{($editAccount->timezone == '+11:50' ? 'selected' : '')}}>(GMT +11:30) Norfolk Island</option>
                          <option value="+12:00"{{($editAccount->timezone == '+12:00' ? 'selected' : '')}}>(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka</option>
                          <option value="+12:75"{{($editAccount->timezone == '+12:75' ? 'selected' : '')}}>(GMT +12:45) Chatham Islands</option>
                          <option value="+13:00"{{($editAccount->timezone == '+13:00' ? 'selected' : '')}}>(GMT +13:00) Apia, Nukualofa</option>
                          <option value="+14:00"{{($editAccount->timezone == '+14:00' ? 'selected' : '')}}>(GMT +14:00) Line Islands, Tokelau</option>
                        </select>
                      </div>
                    </div>      
                  </div>
                </div>
              </div>
              <div class="card" id="facebook-fields-cat">
                <div class="card-header" id="facebook-fields">
                  <h5 class="mb-0">
                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#facebookcollapse" aria-expanded="true" aria-controls="facebook-fields">Facebook Fields</button>
                  </h5>
                </div>
                <div class="collapse show" id="facebookcollapse" aria-labelledby="facebook-fields">
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="fb_user_full_name">FB User Full Name</label>
                          <input type="text" name="fb_user_full_name" id="fb_user_full_name" class="form-control" value="{{isset($editAccount->fb_user_full_name) ? $editAccount->fb_user_full_name : ''}}"  disabled="">
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-8">
                        <div class="form-group">
                          <label for="fb_campaign_name">FB Campaign Name</label>
                          <input type="text" name="fb_campaign_name" id="fb_campaign_name" class="form-control" value="{{isset($editAccount->fb_campaign_name) ? $editAccount->fb_campaign_name : ''}}" readonly="">
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="ad_account_name">Ad Account Name</label>
                          <input class="form-control" name="ad_account_name" id="ad_account_name" type="text" value="{{isset($editAccount->ad_account_name) ? $editAccount->ad_account_name : ''}}"  disabled="">
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="ad_account_id">Ad Account ID</label>
                          <input class="form-control" name="ad_account_id" id="ad_account_id" type="text" value="{{isset($editAccount->ad_account_id) ? $editAccount->ad_account_id : ''}}"  disabled="">
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="active_ad_url">Active Ad URL</label>
                          <input class="form-control" name="active_ad_url" id="active_ad_url" type="text" value="{{isset($editAccount->active_ad_url) ? $editAccount->active_ad_url : ''}}" readonly="">
                           <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="business_manager_name">Business Manager Name</label>
                          <input class="form-control" name="business_manager_name" id="business_manager_name" type="text" value="{{isset($editAccount->business_manager_name) ? $editAccount->business_manager_name : ''}}"  disabled="">
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="business_manager_id">Business Manager ID</label>
                          <input class="form-control" name="business_manager_id" id="business_manager_id" type="text" value="{{isset($editAccount->business_manager_id) ? $editAccount->business_manager_id : ''}}"  disabled="">
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                       <div class="form-group">
                         <label for="page_name">Page Name</label>
                         <input class="form-control" name="page_name" id="page_name" type="text" value="{{isset($editAccount->page_name) ? $editAccount->page_name : ''}}"  disabled="">
                         <div class="valid-feedback">Looks good!</div>
                       </div>
                     </div>
                    </div>
                    <div class="row">
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="page_url">Page URL</label>
                          <input class="form-control" name="page_url" id="page_url" type="text" value="{{isset($editAccount->page_url) ? $editAccount->page_url : ''}}"  disabled="">
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <div class="form-group">
                          <label for="page_id">Page ID</label>
                          <input class="form-control" name="page_id" id="page_id" type="text" value="{{isset($editAccount->page_id) ? $editAccount->page_id : ''}}"  disabled="">
                          <div class="valid-feedback">Looks good!</div>
                        </div>
                      </div>
                      <div class="col-sm-4">
                        <label for="geo">Geo</label>
                        <select class="form-control" name="geo" id="geo" disabled="">
                          <option>Select</option>
                          <option value="Afganistan"{{($editAccount->geo == 'Afghanistan' ? 'selected' : '')}}>Afghanistan
                          </option>
                          <option value="Albania"{{($editAccount->geo == 'Albania' ? 'selected' : '')}}>Albania
                          </option>
                          <option value="Algeria" {{($editAccount->geo == 'Algeria' ? 'selected' : '')}}>Algeria
                          </option>
                          <option value="American Samoa" {{($editAccount->geo == 'American Samoa' ? 'selected' : '')}}>American Samoa
                          </option>
                          <option value="Andorra"{{($editAccount->geo == 'Andorra' ? 'selected' : '')}}>Andorra
                          </option>
                          <option value="Angola"{{($editAccount->geo == 'Angola' ? 'selected' : '')}}>Angola
                          </option>
                          <option value="Anguilla"{{($editAccount->geo == 'Anguilla' ? 'selected' : '')}}>Anguilla
                          </option>
                          <option value="Antigua & Barbuda"{{($editAccount->geo == 'Antigua & Barbuda' ? 'selected' : '')}}>Antigua & Barbuda
                          </option>
                          <option value="Argentina"{{($editAccount->geo == 'Argentina' ? 'selected' : '')}}>Argentina
                          </option>
                          <option value="Armenia"{{($editAccount->geo == 'Armenia' ? 'selected' : '')}}>Armenia
                          </option>
                          <option value="Aruba"{{($editAccount->geo == 'Aruba' ? 'selected' : '')}}>Aruba
                          </option>
                          <option value="Australia"{{($editAccount->geo == 'Australia' ? 'selected' : '')}}>Australia
                          </option>
                          <option value="Austria"{{($editAccount->geo == 'Austria' ? 'selected' : '')}}>Austria
                          </option>
                          <option value="Azerbaijan"{{($editAccount->geo == 'Azerbaijan' ? 'selected' : '')}}>Azerbaijan
                          </option>
                          <option value="Bahamas"{{($editAccount->geo == 'Bahamas' ? 'selected' : '')}}>Bahamas
                          </option>
                          <option value="Bahrain"{{($editAccount->geo == 'Bahrain' ? 'selected' : '')}}>Bahrain
                          </option>
                          <option value="Bangladesh"{{($editAccount->geo == 'Bangladesh' ? 'selected' : '')}}>Bangladesh
                          </option>
                          <option value="Barbados"{{($editAccount->geo == 'Barbados' ? 'selected' : '')}}>Barbados
                          </option>
                          <option value="Belarus"{{($editAccount->geo == 'Belarus' ? 'selected' : '')}}>Belarus
                          </option>
                          <option value="Belgium"{{($editAccount->geo == 'Belgium' ? 'selected' : '')}}>Belgium
                          </option>
                          <option value="Belize" {{($editAccount->geo == 'Belize' ? 'selected' : '')}}>Belize
                          </option>
                          <option value="Benin" {{($editAccount->geo == 'Benin' ? 'selected' : '')}}>Benin
                          </option>
                          <option value="Bermuda" {{($editAccount->geo == 'Bermuda' ? 'selected' : '')}}>Bermuda
                          </option>
                          <option value="Bhutan" {{($editAccount->geo == 'Bhutan' ? 'selected' : '')}}>Bhutan
                          </option>
                          <option value="Bolivia" {{($editAccount->geo == 'Bolivia' ? 'selected' : '')}}>Bolivia
                          </option>
                          <option value="Bonaire" {{($editAccount->geo == 'Bonaire' ? 'selected' : '')}}>Bonaire
                          </option>
                          <option value="Bosnia & Herzegovina" {{($editAccount->geo == 'Bosnia & Herzegovina' ? 'selected' : '')}}>Bosnia & Herzegovina
                          </option>
                          <option value="Botswana" {{($editAccount->geo == 'Botswana' ? 'selected' : '')}}>Botswana
                          </option>
                          <option value="Brazil" {{($editAccount->geo == 'Brazil' ? 'selected' : '')}}>Brazil
                          </option>
                          <option value="British Indian Ocean Ter" {{($editAccount->geo == 'British Indian Ocean Ter' ? 'selected' : '')}}>British Indian Ocean Ter
                          </option>
                          <option value="Brunei" {{($editAccount->geo == 'Brunei' ? 'selected' : '')}}>Brunei
                          </option>
                          <option value="Bulgaria" {{($editAccount->geo == 'Bulgaria' ? 'selected' : '')}}>Bulgaria
                          </option>
                          <option value="Burkina Faso" {{($editAccount->geo == 'Burkina Faso' ? 'selected' : '')}}>Burkina Faso
                          </option>
                          <option value="Burundi" {{($editAccount->geo == 'Burundi' ? 'selected' : '')}}>Burundi
                          </option>
                          <option value="Cambodia" {{($editAccount->geo == 'Cambodia' ? 'selected' : '')}}>Cambodia
                          </option>
                          <option value="Cameroon" {{($editAccount->geo == 'Cameroon' ? 'selected' : '')}}>Cameroon
                          </option>
                          <option value="Canada" {{($editAccount->geo == 'Canada' ? 'selected' : '')}}>Canada
                          </option>
                          <option value="Canary Islands" {{($editAccount->geo == 'Canary Islands' ? 'selected' : '')}}>Canary Islands
                          </option>
                          <option value="Cape Verde" {{($editAccount->geo == 'Cape Verde' ? 'selected' : '')}}>Cape Verde
                          </option>
                          <option value="Cayman Islands" {{($editAccount->geo == 'Cayman Islands' ? 'selected' : '')}}>Cayman Islands
                          </option>
                          <option value="Central African Republic" {{($editAccount->geo == 'Central African Republic' ? 'selected' : '')}}>Central African Republic
                          </option>
                          <option value="Chad" {{($editAccount->geo == 'Chad' ? 'selected' : '')}}>Chad
                          </option>
                          <option value="Channel Islands" {{($editAccount->geo == 'Channel Islands' ? 'selected' : '')}}>Channel Islands
                          </option>
                          <option value="Chile" {{($editAccount->geo == 'Chile' ? 'selected' : '')}}>Chile
                          </option>
                          <option value="China" {{($editAccount->geo == 'China' ? 'selected' : '')}}>China
                          </option>
                          <option value="Christmas Island" {{($editAccount->geo == 'Christmas Island' ? 'selected' : '')}}>Christmas Island
                          </option>
                          <option value="Cocos Island" {{($editAccount->geo == 'Cocos Island' ? 'selected' : '')}}>Cocos Island
                          </option>
                          <option value="Colombia" {{($editAccount->geo == 'Colombia' ? 'selected' : '')}}>Colombia
                          </option>
                          <option value="Comoros" {{($editAccount->geo == 'Comoros' ? 'selected' : '')}}>Comoros
                          </option>
                          <option value="Congo" {{($editAccount->geo == 'Congo' ? 'selected' : '')}}>Congo
                          </option>
                          <option value="Cook Islands" {{($editAccount->geo == 'Cook Islands' ? 'selected' : '')}}>Cook Islands
                          </option>
                          <option value="Costa Rica" {{($editAccount->geo == 'Costa Rica' ? 'selected' : '')}}>Costa Rica
                          </option>
                          <option value="Cote DIvoire" {{($editAccount->geo == 'Cote DIvoire' ? 'selected' : '')}}>Cote DIvoire
                          </option>
                          <option value="Croatia" {{($editAccount->geo == 'Croatia' ? 'selected' : '')}}>Croatia
                          </option>
                          <option value="Cuba" {{($editAccount->geo == 'Cuba' ? 'selected' : '')}}>Cuba
                          </option>
                          <option value="Curaco" {{($editAccount->geo == 'Curacao' ? 'selected' : '')}}>Curacao
                          </option>
                          <option value="Cyprus" {{($editAccount->geo == 'Cyprus' ? 'selected' : '')}}>Cyprus
                          </option>
                          <option value="Czech Republic" {{($editAccount->geo == 'Czech Republic' ? 'selected' : '')}}>Czech Republic
                          </option>
                          <option value="Denmark" {{($editAccount->geo == 'Denmark' ? 'selected' : '')}}>Denmark
                          </option>
                          <option value="Djibouti" {{($editAccount->geo == 'Djibouti' ? 'selected' : '')}}>Djibouti
                          </option>
                          <option value="Dominica" {{($editAccount->geo == 'Dominica' ? 'selected' : '')}}>Dominica
                          </option>
                          <option value="Dominican Republic" {{($editAccount->geo == 'Dominican Republic' ? 'selected' : '')}}>Dominican Republic
                          </option>
                          <option value="East Timor" {{($editAccount->geo == 'East Timor' ? 'selected' : '')}}>East Timor
                          </option>
                          <option value="Ecuador" {{($editAccount->geo == 'Ecuador' ? 'selected' : '')}}>Ecuador
                          </option>
                          <option value="Egypt" {{($editAccount->geo == 'Egypt' ? 'selected' : '')}}>Egypt
                          </option>
                          <option value="El Salvador" {{($editAccount->geo == 'El Salvador' ? 'selected' : '')}}>El Salvador
                          </option>
                          <option value="Equatorial Guinea" {{($editAccount->geo == 'Equatorial Guinea' ? 'selected' : '')}}>Equatorial Guinea
                          </option>
                          <option value="Eritrea" {{($editAccount->geo == 'Eritrea' ? 'selected' : '')}}>Eritrea
                          </option>
                          <option value="Estonia" {{($editAccount->geo == 'Estonia' ? 'selected' : '')}}>Estonia
                          </option>
                          <option value="Ethiopia" {{($editAccount->geo == 'Ethiopia' ? 'selected' : '')}}>Ethiopia
                          </option>
                          <option value="Falkland Islands" {{($editAccount->geo == 'Falkland Islands' ? 'selected' : '')}}>Falkland Islands
                          </option>
                          <option value="Faroe Islands" {{($editAccount->geo == 'Faroe Islands' ? 'selected' : '')}}>Faroe Islands
                          </option>
                          <option value="Fiji" {{($editAccount->geo == 'Fiji' ? 'selected' : '')}}>Fiji
                          </option>
                          <option value="Finland" {{($editAccount->geo == 'Finland' ? 'selected' : '')}}>Finland
                          </option>
                          <option value="France" {{($editAccount->geo == 'France' ? 'selected' : '')}}>France
                          </option>
                          <option value="French Guiana" {{($editAccount->geo == 'French Guiana' ? 'selected' : '')}}>French Guiana
                          </option>
                          <option value="French Polynesia" {{($editAccount->geo == 'French Polynesia' ? 'selected' : '')}}>French Polynesia
                          </option>
                          <option value="French Southern Ter" {{($editAccount->geo == 'French Southern Ter' ? 'selected' : '')}}>French Southern Ter
                          </option>
                          <option value="Gabon" {{($editAccount->geo == 'Gabon' ? 'selected' : '')}}>Gabon
                          </option>
                          <option value="Gambia" {{($editAccount->geo == 'Gambia' ? 'selected' : '')}}>Gambia
                          </option>
                          <option value="Georgia" {{($editAccount->geo == 'Georgia' ? 'selected' : '')}}>Georgia
                          </option>
                          <option value="Germany" {{($editAccount->geo == 'Germany' ? 'selected' : '')}}>Germany
                          </option>
                          <option value="Ghana" {{($editAccount->geo == 'Ghana' ? 'selected' : '')}}>Ghana
                          </option>
                          <option value="Gibraltar" {{($editAccount->geo == 'Gibraltar' ? 'selected' : '')}}>Gibraltar
                          </option>
                          <option value="Great Britain" {{($editAccount->geo == 'Great Britain' ? 'selected' : '')}}>Great Britain
                          </option>
                          <option value="Greece" {{($editAccount->geo == 'Greece' ? 'selected' : '')}}>Greece
                          </option>
                          <option value="Greenland" {{($editAccount->geo == 'Greenland' ? 'selected' : '')}}>Greenland
                          </option>
                          <option value="Grenada" {{($editAccount->geo == 'Grenada' ? 'selected' : '')}}>Grenada
                          </option>
                          <option value="Guadeloupe" {{($editAccount->geo == 'Guadeloupe' ? 'selected' : '')}}>Guadeloupe
                          </option>
                          <option value="Guam" {{($editAccount->geo == 'Guam' ? 'selected' : '')}}>Guam
                          </option>
                          <option value="Guatemala" {{($editAccount->geo == 'Guatemala' ? 'selected' : '')}}>Guatemala
                          </option>
                          <option value="Guinea" {{($editAccount->geo == 'Guinea' ? 'selected' : '')}}>Guinea
                          </option>
                          <option value="Guyana" {{($editAccount->geo == 'Guyana' ? 'selected' : '')}}>Guyana
                          </option>
                          <option value="Haiti" {{($editAccount->geo == 'Haiti' ? 'selected' : '')}}>Haiti
                          </option>
                          <option value="Hawaii" {{($editAccount->geo == 'Hawaii' ? 'selected' : '')}}>Hawaii
                          </option>
                          <option value="Honduras" {{($editAccount->geo == 'Honduras' ? 'selected' : '')}}>Honduras
                          </option>
                          <option value="Hong Kong" {{($editAccount->geo == 'Hong Kong' ? 'selected' : '')}}>Hong Kong
                          </option>
                          <option value="Hungary" {{($editAccount->geo == 'Hungary' ? 'selected' : '')}}>Hungary
                          </option>
                          <option value="Iceland" {{($editAccount->geo == 'Iceland' ? 'selected' : '')}}>Iceland
                          </option>
                          <option value="Indonesia" {{($editAccount->geo == 'Indonesia' ? 'selected' : '')}}>Indonesia
                          </option>
                          <option value="India" {{($editAccount->geo == 'India' ? 'selected' : '')}}>India
                          </option>
                          <option value="Iran" {{($editAccount->geo == 'Iran' ? 'selected' : '')}}>Iran
                          </option>
                          <option value="Iraq" {{($editAccount->geo == 'Iraq' ? 'selected' : '')}}>Iraq
                          </option>
                          <option value="Ireland" {{($editAccount->geo == 'Ireland' ? 'selected' : '')}}>Ireland
                          </option>
                          <option value="Isle of Man" {{($editAccount->geo == 'Isle of Man' ? 'selected' : '')}}>Isle of Man
                          </option>
                          <option value="Israel" {{($editAccount->geo == 'Israel' ? 'selected' : '')}}>Israel
                          </option>
                          <option value="Italy" {{($editAccount->geo == 'Italy' ? 'selected' : '')}}>Italy
                          </option>
                          <option value="Jamaica" {{($editAccount->geo == 'Jamaica' ? 'selected' : '')}}>Jamaica
                          </option>
                          <option value="Japan" {{($editAccount->geo == 'Japan' ? 'selected' : '')}}>Japan
                          </option>
                          <option value="Jordan" {{($editAccount->geo == 'Jordan' ? 'selected' : '')}}>Jordan
                          </option>
                          <option value="Kazakhstan" {{($editAccount->geo == 'Kazakhstan' ? 'selected' : '')}}>Kazakhstan
                          </option>
                          <option value="Kenya" {{($editAccount->geo == 'Kenya' ? 'selected' : '')}}>Kenya
                          </option>
                          <option value="Kiribati" {{($editAccount->geo == 'Kiribati' ? 'selected' : '')}}>Kiribati
                          </option>
                          <option value="Korea North" {{($editAccount->geo == 'Korea North' ? 'selected' : '')}}>Korea North
                          </option>
                          <option value="Korea Sout" {{($editAccount->geo == 'Korea South' ? 'selected' : '')}}>Korea South
                          </option>
                          <option value="Kuwait" {{($editAccount->geo == 'Kuwait' ? 'selected' : '')}}>Kuwait
                          </option>
                          <option value="Kyrgyzstan" {{($editAccount->geo == 'Kyrgyzstan' ? 'selected' : '')}}>Kyrgyzstan
                          </option>
                          <option value="Laos" {{($editAccount->geo == 'Laos' ? 'selected' : '')}}>Laos
                          </option>
                          <option value="Latvia" {{($editAccount->geo == 'Latvia' ? 'selected' : '')}}>Latvia
                          </option>
                          <option value="Lebanon" {{($editAccount->geo == 'Lebanon' ? 'selected' : '')}}>Lebanon
                          </option>
                          <option value="Lesotho" {{($editAccount->geo == 'Lesotho' ? 'selected' : '')}}>Lesotho
                          </option>
                          <option value="Liberia" {{($editAccount->geo == 'Liberia' ? 'selected' : '')}}>Liberia
                          </option>
                          <option value="Libya" {{($editAccount->geo == 'Libya' ? 'selected' : '')}}>Libya
                          </option>
                          <option value="Liechtenstein" {{($editAccount->geo == 'Liechtenstein' ? 'selected' : '')}}>Liechtenstein
                          </option>
                          <option value="Lithuania" {{($editAccount->geo == 'Lithuania' ? 'selected' : '')}}>Lithuania
                          </option>
                          <option value="Luxembourg" {{($editAccount->geo == 'Luxembourg' ? 'selected' : '')}}>Luxembourg
                          </option>
                          <option value="Macau" {{($editAccount->geo == 'Macau' ? 'selected' : '')}}>Macau
                          </option>
                          <option value="Macedonia" {{($editAccount->geo == 'Macedonia' ? 'selected' : '')}}>Macedonia
                          </option>
                          <option value="Madagascar" {{($editAccount->geo == 'Madagascar' ? 'selected' : '')}}>Madagascar
                          </option>
                          <option value="Malaysia" {{($editAccount->geo == 'Malaysia' ? 'selected' : '')}}>Malaysia
                          </option>
                          <option value="Malawi" {{($editAccount->geo == 'Malawi' ? 'selected' : '')}}>Malawi
                          </option>
                          <option value="Maldives" {{($editAccount->geo == 'Maldives' ? 'selected' : '')}}>Maldives
                          </option>
                          <option value="Mali" {{($editAccount->geo == 'Mali' ? 'selected' : '')}}>Mali
                          </option>
                          <option value="Malta" {{($editAccount->geo == 'Malta' ? 'selected' : '')}}>Malta
                          </option>
                          <option value="Marshall Islands" {{($editAccount->geo == 'Marshall Islands' ? 'selected' : '')}}>Marshall Islands
                          </option>
                          <option value="Martinique" {{($editAccount->geo == 'Martinique' ? 'selected' : '')}}>Martinique
                          </option>
                          <option value="Mauritania" {{($editAccount->geo == 'Mauritania' ? 'selected' : '')}}>Mauritania
                          </option>
                          <option value="Mauritius" {{($editAccount->geo == 'Mauritius' ? 'selected' : '')}}>Mauritius
                          </option>
                          <option value="Mayotte" {{($editAccount->geo == 'Mayotte' ? 'selected' : '')}}>Mayotte
                          </option>
                          <option value="Mexico" {{($editAccount->geo == 'Mexico' ? 'selected' : '')}}>Mexico
                          </option>
                          <option value="Midway Islands" {{($editAccount->geo == 'Midway Islands' ? 'selected' : '')}}>Midway Islands
                          </option>
                          <option value="Moldova" {{($editAccount->geo == 'Moldova' ? 'selected' : '')}}>Moldova
                          </option>
                          <option value="Monaco" {{($editAccount->geo == 'Monaco' ? 'selected' : '')}}>Monaco
                          </option>
                          <option value="Mongolia" {{($editAccount->geo == 'Mongolia' ? 'selected' : '')}}>Mongolia
                          </option>
                          <option value="Montserrat" {{($editAccount->geo == 'Montserrat' ? 'selected' : '')}}>Montserrat
                          </option>
                          <option value="Morocco" {{($editAccount->geo == 'Morocco' ? 'selected' : '')}}>Morocco
                          </option>
                          <option value="Mozambique" {{($editAccount->geo == 'Mozambique' ? 'selected' : '')}}>Mozambique
                          </option>
                          <option value="Myanmar" {{($editAccount->geo == 'Myanmar' ? 'selected' : '')}}>Myanmar
                          </option>
                          <option value="Nambia" {{($editAccount->geo == 'Nambia' ? 'selected' : '')}}>Nambia
                          </option>
                          <option value="Nauru" {{($editAccount->geo == 'Nauru' ? 'selected' : '')}}>Nauru
                          </option>
                          <option value="Nepal" {{($editAccount->geo == 'Nepal' ? 'selected' : '')}}>Nepal
                          </option>
                          <option value="Netherland Antilles" {{($editAccount->geo == 'Netherland Antilles' ? 'selected' : '')}}>Netherland Antilles
                          </option>
                          <option value="Netherlands" {{($editAccount->geo == 'Netherlands (Holland, Europe)' ? 'selected' : '')}}>Netherlands (Holland, Europe)
                          </option>
                          <option value="Nevis" {{($editAccount->geo == 'Nevis' ? 'selected' : '')}}>Nevis
                          </option>
                          <option value="New Caledonia" {{($editAccount->geo == 'New Caledonia' ? 'selected' : '')}}>New Caledonia
                          </option>
                          <option value="New Zealand" {{($editAccount->geo == 'New Zealand' ? 'selected' : '')}}>New Zealand
                          </option>
                          <option value="Nicaragua" {{($editAccount->geo == 'Nicaragua' ? 'selected' : '')}}>Nicaragua
                          </option>
                          <option value="Niger" {{($editAccount->geo == 'Niger' ? 'selected' : '')}}>Niger
                          </option>
                          <option value="Nigeria" {{($editAccount->geo == 'Nigeria' ? 'selected' : '')}}>Nigeria
                          </option>
                          <option value="Niue" {{($editAccount->geo == 'Niue' ? 'selected' : '')}}>Niue
                          </option>
                          <option value="Norfolk Island" {{($editAccount->geo == 'Norfolk Island' ? 'selected' : '')}}>Norfolk Island
                          </option>
                          <option value="Norway" {{($editAccount->geo == 'Norway' ? 'selected' : '')}}>Norway
                          </option>
                          <option value="Oman" {{($editAccount->geo == 'Oman' ? 'selected' : '')}}>Oman
                          </option>
                          <option value="Pakistan" {{($editAccount->geo == 'Pakistan' ? 'selected' : '')}}>Pakistan
                          </option>
                          <option value="Palau Island" {{($editAccount->geo == 'Palau Island' ? 'selected' : '')}}>Palau Island
                          </option>
                          <option value="Palestine" {{($editAccount->geo == 'Palestine' ? 'selected' : '')}}>Palestine
                          </option>
                          <option value="Panama" {{($editAccount->geo == 'Panama' ? 'selected' : '')}}>Panama
                          </option>
                          <option value="Papua New Guinea" {{($editAccount->geo == 'Papua New Guinea' ? 'selected' : '')}}>Papua New Guinea
                          </option>
                          <option value="Paraguay" {{($editAccount->geo == 'Paraguay' ? 'selected' : '')}}>Paraguay
                          </option>
                          <option value="Peru" {{($editAccount->geo == 'Peru' ? 'selected' : '')}}>Peru
                          </option>
                          <option value="Phillipines" {{($editAccount->geo == 'Philippines' ? 'selected' : '')}}>Philippines
                          </option>
                          <option value="Pitcairn Island" {{($editAccount->geo == 'Pitcairn Island' ? 'selected' : '')}}>Pitcairn Island
                          </option>
                          <option value="Poland" {{($editAccount->geo == 'Poland' ? 'selected' : '')}}>Poland
                          </option>
                          <option value="Portugal" {{($editAccount->geo == 'Portugal' ? 'selected' : '')}}>Portugal
                          </option>
                          <option value="Puerto Rico" {{($editAccount->geo == 'Puerto Rico' ? 'selected' : '')}}>Puerto Rico
                          </option>
                          <option value="Qatar" {{($editAccount->geo == 'Qatar' ? 'selected' : '')}}>Qatar
                          </option>
                          <option value="Republic of Montenegro" {{($editAccount->geo == 'Republic of Montenegro' ? 'selected' : '')}}>Republic of Montenegro
                          </option>
                          <option value="Republic of Serbia" {{($editAccount->geo == 'Republic of Serbia' ? 'selected' : '')}}>Republic of Serbia
                          </option>
                          <option value="Reunion" {{($editAccount->geo == 'Reunion' ? 'selected' : '')}}>Reunion
                          </option>
                          <option value="Romania" {{($editAccount->geo == 'Romania' ? 'selected' : '')}}>Romania
                          </option>
                          <option value="Russia" {{($editAccount->geo == 'Russia' ? 'selected' : '')}}>Russia
                          </option>
                          <option value="Rwanda" {{($editAccount->geo == 'Rwanda' ? 'selected' : '')}}>Rwanda
                          </option>
                          <option value="St Barthelemy" {{($editAccount->geo == 'St Barthelemy' ? 'selected' : '')}}>St Barthelemy
                          </option>
                          <option value="St Eustatius" {{($editAccount->geo == 'St Eustatius' ? 'selected' : '')}}>St Eustatius
                          </option>
                          <option value="St Helena" {{($editAccount->geo == 'St Helena' ? 'selected' : '')}}>St Helena
                          </option>
                          <option value="St Kitts-Nevis" {{($editAccount->geo == 'St Kitts-Nevis' ? 'selected' : '')}}>St Kitts-Nevis
                          </option>
                          <option value="St Lucia" {{($editAccount->geo == 'St Lucia' ? 'selected' : '')}}>St Lucia
                          </option>
                          <option value="St Maarten" {{($editAccount->geo == 'St Maarten' ? 'selected' : '')}}>St Maarten
                          </option>
                          <option value="St Pierre & Miquelon" {{($editAccount->geo == 'St Pierre & Miquelon' ? 'selected' : '')}}>St Pierre & Miquelon
                          </option>
                          <option value="St Vincent & Grenadines" {{($editAccount->geo == 'St Vincent & Grenadines' ? 'selected' : '')}}>St Vincent & Grenadines
                          </option>
                          <option value="Saipan" {{($editAccount->geo == 'Saipan' ? 'selected' : '')}}>Saipan
                          </option>
                          <option value="Samoa" {{($editAccount->geo == 'Samoa' ? 'selected' : '')}}>Samoa
                          </option>
                          <option value="Samoa American" {{($editAccount->geo == 'Samoa American' ? 'selected' : '')}}>Samoa American
                          </option>
                          <option value="San Marino" {{($editAccount->geo == 'San Marino' ? 'selected' : '')}}>San Marino
                          </option>
                          <option value="Sao Tome & Principe" {{($editAccount->geo == 'Sao Tome & Principe' ? 'selected' : '')}}>Sao Tome & Principe
                          </option>
                          <option value="Saudi Arabia" {{($editAccount->geo == 'Saudi Arabia' ? 'selected' : '')}}>Saudi Arabia
                          </option>
                          <option value="Senegal" {{($editAccount->geo == 'Senegal' ? 'selected' : '')}}>Senegal
                          </option>
                          <option value="Seychelles" {{($editAccount->geo == 'Seychelles' ? 'selected' : '')}}>Seychelles
                          </option>
                          <option value="Sierra Leone" {{($editAccount->geo == 'Sierra Leone' ? 'selected' : '')}}>Sierra Leone
                          </option>
                          <option value="Singapore" {{($editAccount->geo == 'Singapore' ? 'selected' : '')}}>Singapore
                          </option>
                          <option value="Slovakia" {{($editAccount->geo == 'Slovakia' ? 'selected' : '')}}>Slovakia
                          </option>
                          <option value="Slovenia" {{($editAccount->geo == 'Slovenia' ? 'selected' : '')}}>Slovenia
                          </option>
                          <option value="Solomon Islands" {{($editAccount->geo == 'Solomon Islands' ? 'selected' : '')}}>Solomon Islands
                          </option>
                          <option value="Somalia" {{($editAccount->geo == 'Somalia' ? 'selected' : '')}}>Somalia
                          </option>
                          <option value="South Africa" {{($editAccount->geo == 'South Africa' ? 'selected' : '')}}>South Africa
                          </option>
                          <option value="Spain" {{($editAccount->geo == 'Spain' ? 'selected' : '')}}>Spain
                          </option>
                          <option value="Sri Lanka" {{($editAccount->geo == 'Sri Lanka' ? 'selected' : '')}}>Sri Lanka
                          </option>
                          <option value="Sudan" {{($editAccount->geo == 'Sudan' ? 'selected' : '')}}>Sudan
                          </option>
                          <option value="Suriname" {{($editAccount->geo == 'Suriname' ? 'selected' : '')}}>Suriname
                          </option>
                          <option value="Swaziland" {{($editAccount->geo == 'Swaziland' ? 'selected' : '')}}>Swaziland
                          </option>
                          <option value="Sweden" {{($editAccount->geo == 'Sweden' ? 'selected' : '')}}>Sweden
                          </option>
                          <option value="Switzerland" {{($editAccount->geo == 'Switzerland' ? 'selected' : '')}}>Switzerland
                          </option>
                          <option value="Syria" {{($editAccount->geo == 'Syria' ? 'selected' : '')}}>Syria
                          </option>
                          <option value="Tahiti" {{($editAccount->geo == 'Tahiti' ? 'selected' : '')}}>Tahiti
                          </option>
                          <option value="Taiwan" {{($editAccount->geo == 'Taiwan' ? 'selected' : '')}}>Taiwan
                          </option>
                          <option value="Tajikistan" {{($editAccount->geo == 'Tajikistan' ? 'selected' : '')}}>Tajikistan
                          </option>
                          <option value="Tanzania" {{($editAccount->geo == 'Tanzania' ? 'selected' : '')}}>Tanzania
                          </option>
                          <option value="Thailand" {{($editAccount->geo == 'Thailand' ? 'selected' : '')}}>Thailand
                          </option>
                          <option value="Togo" {{($editAccount->geo == 'Togo' ? 'selected' : '')}}>Togo
                          </option>
                          <option value="Tokelau" {{($editAccount->geo == 'Tokelau' ? 'selected' : '')}}>Tokelau
                          </option>
                          <option value="Tonga" {{($editAccount->geo == 'Tonga' ? 'selected' : '')}}>Tonga
                          </option>
                          <option value="Trinidad & Tobago" {{($editAccount->geo == 'Trinidad & Tobago' ? 'selected' : '')}}>Trinidad & Tobago
                          </option>
                          <option value="Tunisia" {{($editAccount->geo == 'Tunisia' ? 'selected' : '')}}>Tunisia
                          </option>
                          <option value="Turkey" {{($editAccount->geo == 'Turkey' ? 'selected' : '')}}>Turkey
                          </option>
                          <option value="Turkmenistan" {{($editAccount->geo == 'Turkmenistan' ? 'selected' : '')}}>Turkmenistan
                          </option>
                          <option value="Turks & Caicos Is" {{($editAccount->geo == 'Turks & Caicos Is' ? 'selected' : '')}}>Turks & Caicos Is
                          </option>
                          <option value="Tuvalu" {{($editAccount->geo == 'Tuvalu' ? 'selected' : '')}}>Tuvalu
                          </option>
                          <option value="Uganda" {{($editAccount->geo == 'Uganda' ? 'selected' : '')}}>Uganda
                          </option>
                          <option value="United Kingdom" {{($editAccount->geo == 'United Kingdom' ? 'selected' : '')}}>United Kingdom
                          </option>
                          <option value="Ukraine" {{($editAccount->geo == 'Ukraine' ? 'selected' : '')}}>Ukraine
                          </option>
                          <option value="United Arab Erimates" {{($editAccount->geo == 'United Arab Emirates' ? 'selected' : '')}}>United Arab Emirates
                          </option>
                          <option value="United States of America" {{($editAccount->geo == 'United States of America' ? 'selected' : '')}}>United States of America
                          </option>
                          <option value="Uraguay" {{($editAccount->geo == 'Uruguay' ? 'selected' : '')}}>Uruguay
                          </option>
                          <option value="Uzbekistan" {{($editAccount->geo == 'Uzbekistan' ? 'selected' : '')}}>Uzbekistan
                          </option>
                          <option value="Vanuatu" {{($editAccount->geo == 'Vanuatu' ? 'selected' : '')}}>Vanuatu
                          </option>
                          <option value="Vatican City State" {{($editAccount->geo == 'Vatican City State' ? 'selected' : '')}}>Vatican City State
                          </option>
                          <option value="Venezuela" {{($editAccount->geo == 'Venezuela' ? 'selected' : '')}}>Venezuela
                          </option>
                          <option value="Vietnam" {{($editAccount->geo == 'Vietnam' ? 'selected' : '')}}>Vietnam
                          </option>
                          <option value="Virgin Islands (Brit)" {{($editAccount->geo == 'Virgin Islands (Brit)' ? 'selected' : '')}}>Virgin Islands (Brit)
                          </option>
                          <option value="Virgin Islands (USA)" {{($editAccount->geo == 'Virgin Islands (USA)' ? 'selected' : '')}}>Virgin Islands (USA)
                          </option>
                          <option value="Wake Island" {{($editAccount->geo == 'Wake Island' ? 'selected' : '')}}>Wake Island
                          </option>
                          <option value="Wallis & Futana Is" {{($editAccount->geo == 'Wallis & Futana Is' ? 'selected' : '')}}>Wallis & Futana Is
                          </option>
                          <option value="Yemen" {{($editAccount->geo == 'Yemen' ? 'selected' : '')}}>Yemen
                          </option>
                          <option value="Zaire" {{($editAccount->geo == 'Zaire' ? 'selected' : '')}}>Zaire
                          </option>
                          <option value="Zambia" {{($editAccount->geo == 'Zambia' ? 'selected' : '')}}>Zambia
                          </option>
                          <option value="Zimbabwe" {{($editAccount->geo == 'Zimbabwe' ? 'selected' : '')}}>Zimbabwe
                          </option>
                       </select>
                      </div>
                    </div>
                    </div>
                  </div>
                </div>
            <div class="card" id="kk-other-fields-cat">
              <div class="card-header" id="kk-other-fields">
                <h5 class="mb-0">
                  <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#kkandothercollapse" aria-expanded="true" aria-controls="kk-other-fields">KK and Other Fields</button>
                </h5>
              </div>
              <div class="collapse show" id="kkandothercollapse" aria-labelledby="kk-other-fields">
                <div class="card-body">
                  <div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label for="pixel_id">Pixel ID</label>
                        <input class="form-control" name="pixel_id" id="pixel_id" type="text" value="{{isset($editAccount->pixel_id) ? $editAccount->pixel_id : ''}}"  disabled="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card" id="advance-fields-cat">
              <div class="card-header" id="advance-fields">
                <h5 class="mb-0">
                  <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#advancecollapse" aria-expanded="true" aria-controls="advance-fields">Advance Fields</button>
                </h5>
              </div>
              <div class="collapse show" id="advancecollapse" aria-labelledby="advance-fields">
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="offer">Offer</label>
                        <select id="offer" name="offer" class="form-control"  disabled="">
                          <option value="">Select</option>
                          <option value="AINS" {{($editAccount->offer == 'AINS' ? 'selected' : '')}}>AINS</option>
                          <option value="NTRA" {{($editAccount->offer == 'NTRA' ? 'selected' : '')}}>NTRA</option>
                          <option value="WIFI" {{($editAccount->offer == 'WIFI' ? 'selected' : '')}}>WIFI</option>
                        </select>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="media_buyer">Media Buyer</label>
                        <select id="media_buyer" name="media_buyer" class="form-control"  disabled="">
                          <option value="">Select</option>
                          <option value="JJ" {{($editAccount->media_buyer == 'JJ' ? 'selected' : '')}}>JJ</option>
                          <option value="ER" {{($editAccount->media_buyer == 'ER' ? 'selected' : '')}}>ER</option>
                          <option value="MF" {{($editAccount->media_buyer == 'MF' ? 'selected' : '')}}>MF</option>
                          <option value="RW" {{($editAccount->media_buyer == 'RW' ? 'selected' : '')}}>RW</option>
                          <option value="SK" {{($editAccount->media_buyer == 'SK' ? 'selected' : '')}}>SK</option>
                          <option value="PC" {{($editAccount->media_buyer == 'PC' ? 'selected' : '')}}>PC</option>
                        </select>
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label for="kk_campaign_title">KK Campaign Title</label> 
                        <input class="form-control" id="kk_campaign_title" name="kk_campaign_title" type="Text" value="{{isset($editAccount->kk_campaign_title) ? $editAccount->kk_campaign_title : ''}}"  readonly="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label for="kk_campaign_url">KK Campaign URL</label>
                        <input class="form-control" id="kk_campaign_url" name="kk_campaign_url" type="Text"  value="{{isset($editAccount->kk_campaign_url) ? $editAccount->kk_campaign_url : ''}}"  readonly="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label for="url_tracking_string">URL Tracking String</label>
                        <input class="form-control" id="url_tracking_string" name="url_tracking_string" type="Text" value="{{isset($editAccount->url_tracking_string) ? $editAccount->url_tracking_string : ''}}"  readonly="">
                        <div class="valid-feedback">Looks good!</div>
                      </div>
                    </div>
                  </div>
                  </div>
                </div>
              </div>
            </div>
          @endif

          <a class="btn-primary floating-button" href="@if(Sentinel::inRole('superadmin')) {{route('edit-account', ['account_id' => $editAccount->id])}} @elseif(Sentinel::inRole('admin')) {{route('admin-edit-account', ['account_id' => $editAccount->id])}} @elseif(Sentinel::inRole('manager')) {{route('manager-edit-account', ['account_id' => $editAccount->id])}} @elseif(Sentinel::inRole('accountant')) {{route('accountant-edit-account', ['account_id' => $editAccount->id])}} @endif"> <i data-feather="edit"></i></a>
          @endforeach
          </div>  
        </div>
      </div>
      <div class="col-sm-12">
        <div class="card">
          <div class="card-header" style="padding: 20px 0.75rem 0.75rem 3.25rem;">
            <h3 style="display: inline-block;">Logs</h3>
            <button class="btn btn-primary logButton" style="float: right;" type="button" data-toggle="modal" data-target="#accountLogModal">Create New Log Entery</button>
            <div class="modal fade" id="accountLogModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel2">New Log Entery</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                     </div>
                     @if(Sentinel::inRole('superadmin'))
                    {!! Form::open(['route' => ['save-account-log'],'method' => 'POST','name' => 'create_account', 'class' => 'needs-validation', 'autocomplete' => 'off', 'novalidate' => '']) !!}
                    @elseif(Sentinel::inRole('admin'))
                    {!! Form::open(['route' => ['admin-save-account-log'],'method' => 'POST','name' => 'create_account', 'class' => 'needs-validation', 'autocomplete' => 'off', 'novalidate' => '']) !!}
                    @elseif(Sentinel::inRole('manager'))
                    {!! Form::open(['route' => ['manager-save-account-log'],'method' => 'POST','name' => 'create_account', 'class' => 'needs-validation', 'autocomplete' => 'off', 'novalidate' => '']) !!}
                    @endif
                      <input type="hidden" name="account_id" value="{{$editAccounts[0]->id}}">
                       <div class="modal-body">
                             <div class="form-group input-group date" id="logdate" data-target-input="nearest">
                              <input class="form-control datetimepicker-input digits" id="logdate" type="text" name="date" data-target="#logdate" data-toggle="datetimepicker">
                             </div>
                             <div class="form-group">
                                <label class="col-form-label" for="message-text">Log Message:</label>
                                <textarea class="form-control" name="log_messages" id="message-text" ></textarea>
                             </div>
                       </div>
                       <div class="modal-footer">
                          <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                          <button class="btn btn-primary" type="submit">Enter Log</button>
                       </div>
                     {!! Form::close() !!}
                  </div>
               </div>
            </div>
          </div>
          <div class="card-body">
            <div class="table-responsive">
               <div id="accounts">
                  <table class="display" id="logTable">
                     <thead>
                        <tr>
                           <th>User</th>
                           <th>Date</th>
                           <th>Message</th>
                        </tr>
                     </thead>
                     <tbody>
                      @foreach($accountLogs as $accountLog)
                        <tr>
                          <td>{{$accountLog->first_name}} {{$accountLog->last_name}}</td>
                          <td>{{$accountLog->date}}</td>
                          <td>{{$accountLog->log_messages}}</td>
                        </tr>
                      @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
          </div>
        </div> 
      </div>
    </div>
  </div>
  @endsection

  @section('script')
  <script src="{{asset('assets/js/datatable/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('assets/js/form-validation-custom.js')}}"></script>
  <script src="{{asset('assets/js/datepicker/date-time-picker/moment.min.js')}}"></script>
<script src="{{asset('assets/js/datepicker/date-time-picker/tempusdominus-bootstrap-4.min.js')}}"></script>
  <script>
    $(document).ready(function() {
        var table = $('#logTable').DataTable( {
            rowReorder: false,
            "order": [],
            columnDefs: [
                { orderable: true, targets: [0,1]},
                { orderable: false, targets: '_all' }
            ]
        } );
    } );
    $(document.body).on('click', '.logButton', function(event) {
      console.log('click');
      $('#logdate').datetimepicker({
          format: 'YYYY-MM-DD HH:mm:ss',
          useCurrent: true,
          defaultDate: new Date()
      });
    });
    $('#accountLogModal').on('shown.bs.modal', function () {
      $('#message-text').focus()
    })
    $('#accountLogModal').on('hidden.bs.modal', function () {  
      $("#logdate").data('datetimepicker').clear();
      $("#logdate").data('datetimepicker').destroy();
    })
  </script>
  <script type="text/javascript">
    $(document).ready(function() {
      $(".expand-all").click(function(){
        if ($(this).data("closedAll")) {
          $(".collapse").collapse("show");
        }
        else {
          $(".collapse").collapse("hide");
        }
        // save last state
        $(this).data("closedAll",!$(this).data("closedAll")); 
      });
      // init with all closed
      $(".expand-all").data("closedAll",false);
    });
  </script>
@endsection