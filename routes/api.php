<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'App\Http\Controllers\API\V1', 'prefix' => 'v1'], function () {
    Route::post('login', 'AuthController@login');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('verify', 'AuthController@verify');
        Route::get('logout', 'AuthController@logout');

        Route::apiResource('fb-accounts', 'FBAccountsController')->middleware('HasPermission:view fbAccount');
        Route::get('/get-dropdown', 'FBAccountsController@getDropdowns')->name('fb-accounts.get-dropdown');

        Route::get('users/media-buyers', 'UserController@mediaBuyers');

        Route::group(['middleware' => ['role:superadmin','HasPermission:view user']], function () {
            Route::apiResource('users', 'UserController');
            Route::put('users/update-status/{id}', 'UserController@updateUserStatus');

        });
        Route::apiResource('logs', 'FBAccountLogController')->only(['show', 'store']);
        Route::group(['prefix' => 'reports'], function () {
            Route::apiResource('fb-accounts', 'FbAccountSpendDataController')->only(['show'])->middleware('HasPermission:view fbAccount');
            Route::get('taboola-campaign', 'TaboolaCampaignController@index')->middleware('HasPermission:view taboola');
            Route::get('revenue', 'TaboolaCampaignController@getRevenueReport')->middleware('HasPermission:view taboola');
        });
       
        Route::get('verticals/all', 'VerticalController@all');
        Route::apiResource('verticals', 'VerticalController')->middleware('HasPermission:view vertical');
       
        Route::get('source/all', 'SourceController@all');
        Route::apiResource('source', 'SourceController')->middleware('HasPermission:view source');
        Route::apiResource('metadata', 'MetaConfigrationController')->only('index');
     
    });
    Route::group(['prefix' => 'webhook'], function () {
        Route::apiResource('fb-reports', 'DownloadAdcostController')->only(['store']);
    });
});
